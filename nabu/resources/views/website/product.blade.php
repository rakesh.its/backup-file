@include('website.header')
<style>
.top_nav{
	background-color:blue;
}
</style>
<section class="portfolio"  style="background-color:#f9f9f9;margin-top:70px;">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-7 col-md-10">
				<div class="section-heading color-white">
					<h2 class="title" style="color:blue;">
						Our Products
					</h2>
				</div>
			</div>
		</div>
		<div class="row">
		@if(count($toReturn['Products'])>0)
			@foreach($toReturn['Products'] as $key_p => $value_p)
			<div class="col-lg-4 col-md-6">
				<div class="single-portfolio">
					<img src="{{url('public/upload/product_image')}}/{{$value_p['product_photo']}}" alt="">
					<div class="content-wrapper">
						<div class="content">
							<h4 class="title">
								{{$value_p['']}}
							</h4>
							<div class="links">
								<a href="{{url('public/upload/product_image')}}/{{$value_p['product_photo']}}" class="link img-popup">
									<i class="far fa-eye"></i>
								</a>
								<a href="{{url('/Products/details')}}/{{Crypt::encrypt($value_p->id)}}" class="link">
									<i class="fas fa-link"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		@else
		<div>
			<h2>Product Not Available</h2>
		</div>
		@endif
		
			
			
		</div>
	</div>
</section>


@include('website.footer')
