@include('website.header')
<section class="hero-area" style="background: url({{url('public/WebsiteAsset/images/banner001.jpg')}});">
	<div id="site-banner" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img src="{{url('public/WebsiteAsset/images/banner1.jpg')}}" alt="...">
				<div class="carousel-caption banner-carousel-caption">
					<h1>Comming Soon</h1>
				</div>
			</div>
			<div class="carousel-item">
				<img src="{{url('public/WebsiteAsset/images/banner2.jpg')}}" alt="...">
				<div class="carousel-caption banner-carousel-caption">
					<h1>Comming Soon</h1>
				</div>
			</div>
		</div>
		<a class="carousel-control-prev banner-slider-arrows" href="#site-banner" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next banner-slider-arrows" href="#site-banner" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</section>




@include('website.footer')