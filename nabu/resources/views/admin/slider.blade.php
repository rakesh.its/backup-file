@include('admin.include.header')
<link href="{{url('public/AdminAssest/vendors/summernote/dist/summernote.css')}}" rel="stylesheet" />
@include('admin.include.sidebar')

<!-- START PAGE CONTENT-->

    <div class="ibox" style="margin-top:10px">
        <div class="ibox-head">
            <div class="ibox-title">List Slider</div>
            <button class="btn btn-outline-primary pull-right" onclick="add_slider()">Add Slider <i class="fa fa-plus"></i> </button>
        </div>
       
        <div class="ibox-body">
            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Message</th>
                        <th> Photo</th>
                        <th>Date Created</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                @foreach ($toReturn['slider'] as $data)
                    <tr>
                        <td>{{$data->silder_message}}</td>
                        <td><img src="{{url('public/upload/slider')}}/{{$data->slider_photo}}" width="50px" height="50px"></td>

                        <td class="text-right">{{date('d M Y',strtotime($data->created_at))}}</td>
                        @if($data->Status == 1)
                        <td class="text-center">
                            <p class="mb-0">
                                <span class="badge badge-success">Active</span>
                            </p>
                        </td>
                        @else
                        <td class="text-center">
                            <p class="mb-0">
                                <span class="badge badge-danger">Inactive</span>
                            </p>
                        </td>
                        @endif
                        <td style="width: 15%;" class="action">
                                <a href="javascript:void()" class="on-default view-row" onclick="edit_slider({{$data->id}})" data-placement="top" title="Active "><i class="fa fa-edit" aria-hidden="true"></i></a>
                                <a href="{{url('admin/slider/statusUpdate/')}}/{{$data->id}}" class="on-default view-row" data-placement="top" title="Active "><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    @endforeach
               
            </table>
        </div>
    </div>

    <div class="modal fade" id="slider_action" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{url('admin/slider/save')}}" method="post" enctype="multipart/form-data">
                @csrf()
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Slider : <span id="title_view">Add</span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Slider Message:<font color="red">*</font></label>
                            <textarea name="silder_message" id="silder_message" class="form-control" required ></textarea>
                            <input type="hidden" required class="form-control" name="slider_id" id="slider_id">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Slider Image:<font color="red">*</font></label>
                            <input type="file" class="form-control"  accept="image/gif, image/jpg, image/jpeg, image/png" name="slider_photo" id="inputimage" required>
                            <input  type="hidden" name="edit_image" id="edit_image" >
                            <div id="edit_image_body"></div>
                        </div>
                        
                        <div class="form-group">
                            <p class="control-label"><b>Is Active</b>
                                <font color="red">*</font>
                            </p>
                            <div class="radio radio-info form-check-inline">
                                <input type="radio" value="1" name="status" checked="">
                                <label for="inlineRadio1"> Active </label>
                            </div>
                            <div class="radio radio-info form-check-inline">
                                <input type="radio" value="0" name="status">
                                <label for="inlineRadio1"> Inactive </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <center><button type="submit" class="btn btn-primary">Submit</button></center>
                    </div>
                </div>
            </form>
        </div>
    </div>           
@include('admin.include.footer') 
   
<script src="{{url('public/AdminAssest/vendors/summernote/dist/summernote.min.js')}}" type="text/javascript"></script>
<script>
     function add_slider() {
        $('#page').val('');
        $("#title_view").html("Add");

        $('#slider_action').modal('show');
    }

    function edit_slider(id) {
        $("#title_view").html("Edit");
        $("#edit_image_body").html("");
        var to_append="";
       
        $("#product_category_id").val(id);
        $.ajax({
            url: "{{url('admin/slider/fetch')}}" + "/" + id,
            method: "GET",
            contentType: 'application/json',
            dataType: "json",
            success: function (data) {
                to_append += `<div class="images-delete-block" style="margin-right:5px; display:inline-block; position:relative; padding:3px;border:1px solid #c4c4c4;border-radious:3px;">
                                <img src="{{url('public/upload/slider/`+ data.slider_photo + `')}}" style="height:90px; min-height:90px; min-width:80px;">
                            </div>`;
                console.log(data);
               
                $("#slider_id").val(data.id);

                $("#silder_message").val(data.silder_message);
                $("#edit_image_body").append(to_append);

                var val = data.Status;
                if (val == 1) {
                    $('input[name=status][value=' + val + ']').prop('checked', true);
                } else {
                    $('input[name=status][value=' + val + ']').prop('checked', true);
                }
            }
        });
        $('#slider_action').modal('show');

    }
</script>
