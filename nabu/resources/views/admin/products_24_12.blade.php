@include('admin.include.header')
<link href="{{url('public/AdminAssest/vendors/summernote/dist/summernote.css')}}" rel="stylesheet" />
@include('admin.include.sidebar')

<!-- START product CONTENT-->

    <div class="ibox" style="margin-top: 10px">
        <div class="ibox-head">
            <div class="ibox-title">List  Product</div>
            <button class="btn btn-outline-primary pull-right" onclick="addProducts()">Add Product<i class="fa fa-plus"></i> </button>
        </div>
       
        <div class="ibox-body">
            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Price	</th>
                        <th>Start Price</th>
                        <th> Photo</th>
                        <th>Specification	</th>
                        <th>Date Created</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
               
                <tbody>
                    @foreach($products as $key_products => $value_products)
                    <tr>
                        <td>{{$value_products->product_name}}</td>
                        <td>{{$value_products->product_price}}</td>
                        <td>{{$value_products->product_start_price}}</td>
                        <td><img src="{{url('public/upload/product_image')}}/{{$value_products->product_photo}}" width="50px" height="50px"></td>
                        <td>{{$value_products->product_specs}}</td>
                        <td style="width:11%">{{date('d/M/Y',strtotime($value_products->created_at))}}</td>
                        @if($value_products->status == 1)
                        <td>
                            <p class="mb-0">
                                <span class="badge badge-success">Active</span>
                            </p>
                        </td>
                        @else
                        <td>
                            <p class="mb-0">
                                <span class="badge badge-danger">Inactive</span>
                            </p>
                        </td>
                        @endif
                        <td style="width: 15%;" class="action">
                                <a href="javascript:void()" class="on-default view-row" onclick="editProducts({{$value_products->id}})" data-placement="top" title="Active "><i class="fa fa-edit" aria-hidden="true"></i></a>
                                <a href="{{url('admin/products/statusUpdate/')}}/{{$value_products->id}}" class="on-default view-row" data-placement="top" title="Active "><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    @endforeach
                   
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="products_action" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{url('admin/products/save')}}" method="post" enctype="multipart/form-data" id="product_frm">
                @csrf()
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Products :<span id="title_view">Add</span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Name:</label>
                            <input type="text" required class="form-control" name="product_name" id="product_name">
                            <input type="hidden" required class="form-control" name="products_id" id="products_id">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Product Specification:</label>
                            <input type="text" required class="form-control" name="product_specs" id="product_specs">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Product Price:</label>
                            <input type="number"  class="form-control" name="product_price" id="product_price">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Product Start Price:</label>
                            <input type="number"  class="form-control" name="product_start_price" id="product_start_price">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Product Photo:</label>
                            <input type="file"  class="form-control" name="product_photo" id="product_photo">
                            <div id="edit_photo_section"></div>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Product Pdf:</label>
                            <input type="file"  class="form-control" name="product_pdf" id="product_pdf">
                        </div>
                        
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Product Description:</label>
                            <textarea name="product_description"  class="form-control" id="product_description"></textarea>
                        </div>
                        
                        <div class="form-group">
                            <p class="control-label"><b>Is Active</b>
                                <font color="red">*</font>
                            </p>
                            <div class="radio radio-info form-check-inline">
                                <input type="radio" value="1" name="status" checked="">
                                <label for="inlineRadio1"> Active </label>
                            </div>
                            <div class="radio radio-info form-check-inline">
                                <input type="radio" value="0" name="status">
                                <label for="inlineRadio1"> Inactive </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <center><button type="submit" class="btn btn-primary">Submit</button></center>
                    </div>
                </div>
            </form>
        </div>
    </div>           
@include('admin.include.footer') 
   
<script src="{{url('public/AdminAssest/vendors/summernote/dist/summernote.min.js')}}" type="text/javascript"></script>
<script>
     function addProducts() {
        $('#product').val('');
        $("#title_view").html("Add");
        $("#product_frm").trigger('reset');
        $('#products_action').modal('show');
    }

    function editProducts(id) {
        $("#title_view").html("Edit");
        var  to_append="";
       
        $("#products_id").val(id);
        $.ajax({
            url: "{{url('admin/products/fetch')}}" + "/" + id,
            method: "GET",
            contentType: 'application/json',
            dataType: "json",
            success: function (data) {

                console.log(data);
                $("#product_name").val(data.product_name);
                $("#product_description	").val(data.product_description	);
                $("#product_specs").val(data.product_specs);
                $("#product_price").val(data.product_price);
                $("#product_start_price").val(data.product_start_price);
             
                var val = data.status;
                if (val == 1) {
                    $('input[name=status][value=' + val + ']').prop('checked', true);
                } else {
                    $('input[name=status][value=' + val + ']').prop('checked', true);
                }
                to_append += `<div class="images-delete-block" style="margin-right:5px; display:inline-block; position:relative; padding:3px;border:1px solid #c4c4c4;border-radious:3px;">
                                <img src="{{url('public/upload/product_pdf/`+ data.product_photo + `')}}" style="height:90px; min-height:90px; min-width:80px;">
                            </div>`;
                var val = data.status;
                if (val == 1) {
                    $('input[name=is_active][value=' + val + ']').prop('checked', true);
                } else {
                    $('input[name=is_active][value=' + val + ']').prop('checked', true);
                }
                $('#edit_photo_section').css('display', 'block');
                $('#edit_photo_section').append(to_append);
            }
        });
        $('#products_action').modal('show');

    }
</script>
