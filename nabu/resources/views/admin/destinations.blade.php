@include('admin.include.header')
<link href="{{url('public/AdminAssest/vendors/summernote/dist/summernote.css')}}" rel="stylesheet" />
@include('admin.include.sidebar')

<!-- START destinations CONTENT-->

    <div class="ibox" style="margin-top:10px">
        <div class="ibox-head">
            <div class="ibox-title">List  destinations</div>
            <button class="btn btn-outline-primary pull-right" onclick="adddestinations()">Add destinations<i class="fa fa-plus"></i> </button>
        </div>
       
        <div class="ibox-body">
            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Title</th>
                        <th>Position</th>
                        <th>Date Created</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
               
                <tbody>
                    @foreach($destinations as $key_destinations => $value_destinations)
                    <tr>
                        <td>{{$value_destinations->destinations_name}}</td>
                        <td>{{$value_destinations->destinations_title}}</td>
                        <td>{{$value_destinations->destinations_position}}</td>
                        <td>{{date('d/M/Y',strtotime($value_destinations->created_at))}}</td>
                        @if($value_destinations->status == 1)
                        <td>
                            <p class="mb-0">
                                <span class="badge badge-success">Active</span>
                            </p>
                        </td>
                        @else
                        <td>
                            <p class="mb-0">
                                <span class="badge badge-danger">Inactive</span>
                            </p>
                        </td>
                        @endif
                        <td style="width: 15%;" class="action">
                                <a href="javascript:void()" class="on-default view-row" onclick="editdestinations({{$value_destinations->id}})" data-placement="top" title="Active "><i class="fa fa-edit" aria-hidden="true"></i></a>
                                <a href="{{url('admin/destinations/statusUpdate/')}}/{{$value_destinations->id}}" class="on-default view-row" data-placement="top" title="Active "><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    @endforeach
                   
                    
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="destinations_action" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{url('admin/destinations/save')}}" method="post" enctype="multipart/form-data">
                @csrf()
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">destinations :<span id="title_view">Add</span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Name:</label>
                            <input type="text" required class="form-control" name="destinations_name" id="destinations_name">
                            <input type="hidden" required class="form-control" name="destinations_id" id="destinations_id">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Title:</label>
                            <input type="text" required class="form-control" name="destinations_title" id="destinations_title">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">destinations Position:</label>
                            <select class="form-control" name="destinations_position" id="destinations_position">
                                <option>Header</option>
                                <option>Footer</option>
                            </select>
                            
                        </div>
                        
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">destinations Contain:</label>
                            <textarea name="destinations_contain" data-provide="markdown" data-iconlibrary="fa" rows="10">h1 header ============ Paragraphs are separated by a blank line. 2nd paragraph. *Italic*, **bold**, and `monospace`. Itemized lists look like: * this one * that one * the other one</textarea>
                        </div>
                        
                        <div class="form-group">
                            <p class="control-label"><b>Is Active</b>
                                <font color="red">*</font>
                            </p>
                            <div class="radio radio-info form-check-inline">
                                <input type="radio" value="1" name="status" checked="">
                                <label for="inlineRadio1"> Active </label>
                            </div>
                            <div class="radio radio-info form-check-inline">
                                <input type="radio" value="0" name="status">
                                <label for="inlineRadio1"> Inactive </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <center><button type="submit" class="btn btn-primary">Submit</button></center>
                    </div>
                </div>
            </form>
        </div>
    </div>           
@include('admin.include.footer') 
   
<script src="{{url('public/AdminAssest/vendors/summernote/dist/summernote.min.js')}}" type="text/javascript"></script>
<script>
     function adddestinations() {
        $('#destinations').val('');
        $("#title_view").html("Add");

        $('#destinations_action').modal('show');
    }

    function editdestinations(id) {
        $("#title_view").html("Edit");
       
        $("#destinations_id").val(id);
        $.ajax({
            url: "{{url('admin/destinations/fetch')}}" + "/" + id,
            method: "GET",
            contentType: 'application/json',
            dataType: "json",
            success: function (data) {

                console.log(data);
                $("#destinations_title").val(data.destinations_title);
                $("#destinations_name").val(data.destinations_name);
                $("#destinations_contain").val(data.destinations_contain);
                $("#destinations_position").val(data.destinations_position);
             
                var val = data.status;
                if (val == 1) {
                    $('input[name=status][value=' + val + ']').prop('checked', true);
                } else {
                    $('input[name=status][value=' + val + ']').prop('checked', true);
                }
            }
        });
        $('#destinations_action').modal('show');

    }
</script>
