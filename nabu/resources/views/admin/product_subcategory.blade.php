@include('admin.include.header')
<link href="{{url('public/AdminAssest/vendors/summernote/dist/summernote.css')}}" rel="stylesheet" />
@include('admin.include.sidebar')

<!-- START PAGE CONTENT-->

    <div class="ibox" style="margin-top:10px">
        <div class="ibox-head">
            <div class="ibox-title">List  Product Sub Category</div>
            <button class="btn btn-outline-primary pull-right" onclick="addproduct_subcategory()">Add Product Subcategory <i class="fa fa-plus"></i> </button>
        </div>
       
        <div class="ibox-body">
            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Description</th>
                        <th>Date Created</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
               
                <tbody>
                @foreach ($toReturn['product_subcategory'] as $data)
                    <tr>
                        <td>{{$data->subcategory_title}}</td>
                        <td>{{$data->category_titile}}</td>
                        
                        <td>{{$data->subcategory_description}}</td>
                        <td class="text-right">{{date('d M Y',strtotime($data->created_at))}}</td>
                        @if($data->status == 1)
                        <td class="text-center">
                            <p class="mb-0">
                                <span class="badge badge-success">Active</span>
                            </p>
                        </td>
                        @else
                        <td class="text-center">
                            <p class="mb-0">
                                <span class="badge badge-danger">Inactive</span>
                            </p>
                        </td>
                        @endif
                        <td style="width: 15%;" class="action">
                                <a href="javascript:void()" class="on-default view-row" onclick="editproduct_subcategory({{$data->id}})" data-placement="top" title="Active "><i class="fa fa-edit" aria-hidden="true"></i></a>
                                <a href="{{url('admin/product_subcategory/statusUpdate/')}}/{{$data->id}}" class="on-default view-row" data-placement="top" title="Active "><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    @endforeach
                   
                    
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="product_subcategory_action" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{url('admin/product_subcategory/save')}}" method="post" enctype="multipart/form-data">
                @csrf()
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Sub Category : <span id="title_view">Add</span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label"> Title:<span style="color:red;">*</span></label>
                            <input type="text" required class="form-control" name="subcategory_title" id="subcategory_title">
                            <input type="hidden" required class="form-control" name="product_subcategory_id" id="product_subcategory_id">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Select Category<span style="color:red;">*</span></label>
                            <select class="form-control" name="category_id" id="category_id" required>
                                <option value="">-Select-</option>
                                @foreach($toReturn['category'] as $category)
                                <option value="{{$category['id']}}">{{$category['category_titile']}}</option>
                                @endforeach
                            </select>
                            <h6 id="state_val"></h6>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label"> Description:</label>
                            <textarea name="description" id="description" class="form-control"></textarea>
                        </div>
                        
                        <div class="form-group">
                            <p class="control-label"><b>Is Active</b>
                                <font color="red">*</font>
                            </p>
                            <div class="radio radio-info form-check-inline">
                                <input type="radio" value="1" name="status" checked="">
                                <label for="inlineRadio1"> Active </label>
                            </div>
                            <div class="radio radio-info form-check-inline">
                                <input type="radio" value="0" name="status">
                                <label for="inlineRadio1"> Inactive </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <center><button type="submit" class="btn btn-primary">Submit</button></center>
                    </div>
                </div>
            </form>
        </div>
    </div>           
@include('admin.include.footer') 
   
<script src="{{url('public/AdminAssest/vendors/summernote/dist/summernote.min.js')}}" type="text/javascript"></script>
<script>
     function addproduct_subcategory() {
        $('#page').val('');
        $("#title_view").html("Add");

        $('#product_subcategory_action').modal('show');
    }

    function editproduct_subcategory(id) {
        $("#title_view").html("Edit");
       
        $("#product_subcategory_id").val(id);
        $.ajax({
            url: "{{url('admin/product_subcategory/fetch')}}" + "/" + id,
            method: "GET",
            contentType: 'application/json',
            dataType: "json",
            success: function (data) {

                console.log(data);
                $("#product_subcategory_title").val(data.product_subcategory_title);
                $("#subcategory_title").val(data.subcategory_title);
                $("#description	").val(data.subcategory_description	);
                $("#product_subcategory_position").val(data.product_subcategory_position);
                $("#category_id").val(data.category_id);
             
                var val = data.status;
                if (val == 1) {
                    $('input[name=status][value=' + val + ']').prop('checked', true);
                } else {
                    $('input[name=status][value=' + val + ']').prop('checked', true);
                }
            }
        });
        $('#product_subcategory_action').modal('show');

    }
</script>
