@include('admin.include.header')
<link href="{{url('public/AdminAssest/vendors/summernote/dist/summernote.css')}}" rel="stylesheet" />
@include('admin.include.sidebar')

<!-- START PAGE CONTENT-->

    <div class="ibox" style="margin-top:10px">
        <div class="ibox-head">
            <div class="ibox-title">List Contact </div>
        </div>
       
        <div class="ibox-body">
            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Product Name</th>
                        <th>Email</th>
                        <th>Subject</th>
                        <th>Phone No</th>
                        <th>Address</th>
                        <th>Comment</th>
                        <th>Date Created</th>
                    </tr>
                </thead>
               
                <tbody>
                @foreach ($toReturn['contacts'] as $data)
                    <tr>
                        <td>{{$data->name}}</td>
                        @if($data->Product_id!="")
                        @php
						@$products=DB::table('products')->where('id',$data->Product_id)->value('product_name') ?? "";
					    @endphp
                        <td>{{$products}}</td>
                        @else
                        <td> </td>
                        @endif
                        <td>{{$data->email}}</td>
                        <td>{{$data->subject}}</td>
                        <td>{{$data->phone_no}}</td>
                        <td>{{$data->address}}</td>
                        <td>{{$data->comments}}</td>
                        <td class="text-right">{{date('d M Y',strtotime($data->created_at))}}</td>
                    </tr>
                @endforeach
                   
                    
                </tbody>
            </table>
        </div>
    </div>
        
@include('admin.include.footer') 
   
