<nav class="page-sidebar" id="sidebar">
            <div id="sidebar-collapse">
                <div class="admin-block d-flex">
                    <div>
                        <img src="{{url('public/AdminAssest/img/admin-avatar.png')}}" width="45px" />
                    </div>
                    <div class="admin-info">
                        <div class="font-strong">{{Auth::user()->name}}</div><small>Administrator</small></div>
                </div>
                <ul class="side-menu metismenu">
                    <li>
                        <a class="active" href="{{url('dashboard')}}"><i class="sidebar-item-icon fa fa-th-large"></i>
                            <span class="nav-label">Dashboard</span>
                        </a>
                    </li>
                    <li class="heading">FEATURES</li>
                    <li>
                        <a href="{{url('admin/comming_soon')}}"><i class="sidebar-item-icon fa fa-edit"></i>
                            <span class="nav-label">Order</span></a>
                    </li>
                    <li>
                        <a href="javascript:;"><i class="sidebar-item-icon fa fa-edit"></i>
                            <span class="nav-label">Products</span><i class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-2-level collapse">
                            <li>
                                <a href="{{url('admin/products')}}">Product</a>
                            </li>
                            <li>
                                <a href="{{url('admin/product_category')}}">Categories </a>
                            </li>
                            <!-- <li>
                                <a href="{{url('admin/product_subcategory')}}">Sub categories</a>
                            </li> -->
                           
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;"><i class="sidebar-item-icon fa fa-bookmark"></i>
                            <span class="nav-label">CMS</span><i class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-2-level collapse">
                            <li>
                                <a href="{{url('admin/pages')}}">Pages </a>
                            </li>
                            <li>
                                <a href="{{url('admin/comming_soon')}}">Header</a>
                            </li>
                            <li>
                                <a href="{{url('admin/comming_soon')}}">Footers</a>
                            </li>
                        </ul>
                    </li>
                   
                    <li>
                        <a href="{{url('admin/contact')}}"><i class="sidebar-item-icon fa fa-edit"></i>
                            <span class="nav-label">Leads/Contact</span></a>
                    </li>
                    <li>
                        <a href="{{url('admin/comming_soon')}}"><i class="sidebar-item-icon fa fa-edit"></i>
                            <span class="nav-label">Blog</span></a>
                    </li>
                    
                    <li>
                        <a href="{{url('admin/category')}}"><i class="sidebar-item-icon fa fa-edit"></i>
                            <span class="nav-label">Category</span></a>
                    </li>
                    <li>
                        <a href="{{url('admin/testimonial')}}"><i class="sidebar-item-icon fa fa-edit"></i>
                            <span class="nav-label">Testimonial</span></a>
                    </li>
                    <li>
                        <a href="{{url('admin/slider')}}"><i class="sidebar-item-icon fa fa-edit"></i>
                            <span class="nav-label">Slider</span></a>
                    </li>
                    <li>
                        <a href="javascript:;"><i class="sidebar-item-icon fa fa-edit"></i>
                            <span class="nav-label">Settings</span><i class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-2-level collapse">
                            <li>
                                <a href="{{url('admin/user')}}">User</a>
                            </li>                           
                        </ul>
                    </li>
                    
                </ul>
            </div>
        </nav>
        <div class="content-wrapper">