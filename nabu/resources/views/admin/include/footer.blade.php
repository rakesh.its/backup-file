
<!-- END PAGE CONTENT-->
<footer class="page-footer">
                <div class="font-13"> {{date('Y')}}© <b>Nabu</b> - All rights reserved.</div>
            </footer>
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
   
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
    <!-- CORE PLUGINS-->
    <script src="{{url('public/AdminAssest/vendors/jquery/dist/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{url('public/AdminAssest/vendors/popper.js/dist/umd/popper.min.js')}}" type="text/javascript"></script>
    <script src="{{url('public/AdminAssest/vendors/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{url('public/AdminAssest/vendors/metisMenu/dist/metisMenu.min.js')}}" type="text/javascript"></script>
    <script src="{{url('public/AdminAssest/vendors/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <!-- PAGE LEVEL PLUGINS-->
    <script src="{{url('public/AdminAssest/vendors/chart.js/dist/Chart.min.js')}}" type="text/javascript"></script>
    <script src="{{url('public/AdminAssest/vendors/jvectormap/jquery-jvectormap-2.0.3.min.js')}}" type="text/javascript"></script>
    <script src="{{url('public/AdminAssest/vendors/jvectormap/jquery-jvectormap-world-mill-en.js')}}" type="text/javascript"></script>
    <script src="{{url('public/AdminAssest/vendors/jvectormap/jquery-jvectormap-us-aea-en.js')}}" type="text/javascript"></script>
    <!-- CORE SCRIPTS-->
    <script src="{{url('public/AdminAssest/js/app.min.js')}}" type="text/javascript"></script>
    <!-- PAGE LEVEL SCRIPTS-->
    <script src="{{url('public/AdminAssest/js/scripts/dashboard_1_demo.js')}}" type="text/javascript"></script>
</body>

</html>