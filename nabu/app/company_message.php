<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class company_message extends Model
{
    public $timestamps = false;
    protected $table = 'company_message';
}
