<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product_subcategory extends Model
{
    public $timestamps = false;
    protected $table = 'product_subcategory';
}
