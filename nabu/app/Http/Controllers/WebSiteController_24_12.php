<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\pages;
use App\products;
use App\category;
use App\testimonial;
use App\destinations;
use App\slider;
use Crypt;
use Session;
use Hash;
use DB;
 
class WebSiteController extends Controller
{
    public function GetHomePageDate()
    {

        $toReturn=[];
        $Category_Data=category::where('status',1)->orderBy('id','DESC')->limit(6)->get();
        $Testimonial_Data=testimonial::where('status',1)->orderBy('id','DESC')->limit(6)->get();
        $Products_Data=products::where('status',1)->orderBy('id','DESC')->limit(6)->get();
        $slider_Data=slider::where('status',1)->orderBy('id','DESC')->limit(5)->get();
        $Destinations_Data=destinations::where('status',1)->orderBy('id','DESC')->limit(3)->get();
        $toReturn['Category_Data']=$Category_Data;
        $toReturn['Testimonial_Data']=$Testimonial_Data;
        $toReturn['Products_Data']=$Products_Data;
        $toReturn['slider_Data']=$slider_Data;
        $toReturn['Destinations_Data']=$Destinations_Data;
        return view('website.home')->with('toReturn',$toReturn);
    }
}
