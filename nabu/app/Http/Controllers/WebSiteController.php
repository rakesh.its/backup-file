<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\pages;
use App\products;
use App\contacts;
use App\category;
use App\testimonial;
use App\destinations;
use App\product_category;
use App\product_subcategory;
use App\slider;
use Crypt;
use Session;
use Hash;
use DB;
 
class WebSiteController extends Controller
{
    public function GetHomePageDate()
    {
        $toReturn=[];
        $Category_Data=category::where('status',1)->orderBy('id','DESC')->limit(6)->get();
        $Testimonial_Data=testimonial::where('status',1)->orderBy('id','DESC')->limit(6)->get();
        $slider_Data=slider::where('status',1)->orderBy('id','DESC')->limit(5)->get();
        $Destinations_Data=destinations::where('status',1)->orderBy('id','DESC')->limit(3)->get();
        $Products_Data=products::where('status',1)->orderBy('id','DESC')->limit(13)->get();
        $product_subcategory_Data=product_subcategory::where('status',1)->orderBy('id','DESC')->limit(13)->get();
        $product_category_Data=product_category::where('status',1)->orderBy('id','DESC')->limit(3)->get();
        $GetProductDetailsCategoryBy=[];
        foreach($product_category_Data as $key=> $value)
        {
            $GetProductDetailsCategoryBy[$key]=products::where('product_category_id',$value->id)->where('status',1)->get() ?? [];
        }
        $toReturn['Category_Data']=$Category_Data;
        $toReturn['Testimonial_Data']=$Testimonial_Data;
        $toReturn['Products_Data']=$Products_Data;
        $toReturn['slider_Data']=$slider_Data;
        $toReturn['Destinations_Data']=$Destinations_Data;
        $toReturn['product_category_Data']=$product_category_Data;
        $toReturn['product_subcategory_Data']=$product_subcategory_Data;
        $toReturn['GetProductDetailsCategoryBy']=$GetProductDetailsCategoryBy;
        //echo "<pre>";
        //print_r($GetProductDetailsCategoryBy);
        // return "test";
        $tag = $this->LoadNavProducts();  // Calling function
        $toReturn['tag']=$tag;  // Adding to toReturn array
        
        return view('website.home')->with('toReturn',$toReturn);
    }

    public function contact()
    {

        $tag = $this->LoadNavProducts();  // Calling function
        $toReturn['tag']=$tag;  // Adding to toReturn array

        return view('website.contact')->with('toReturn',$toReturn);
    }
    
    public function website_comming_soon()
    {

        $tag = $this->LoadNavProducts();  // Calling function
        $toReturn['tag']=$tag;  // Adding to toReturn array

        return view('website.comming_soon')->with('toReturn',$toReturn);
    }

    public function contactsave(Request $request)
    {
        // return $request;

        $contact_details = new contacts();
        $contact_details->name = $request->name;
        $contact_details->email = $request->email;
        $contact_details->subject = $request->subject;
        $contact_details->phone_no = $request->phone_no;
        $contact_details->address = $request->address;
        $contact_details->comments = $request->comments;
        $contact_details->Product_id = $request->Product_id ?? "";
        $contact_details->save();
    
        return redirect('/');
      
    }

    # Function for Product list in nav
    public function LoadNavProducts(){
        # Start For Nav Product Listing
        $product_category_Data=product_category::where('status',1)->orderBy('id','DESC')->get(); // Get Data For Catagory

        $tag = [];  // arrray default value
        

        foreach($product_category_Data as $key => $value){  // Foreach For  Product Catagory

                $pro_ducts = [];  // array default value
                $key_1 = 0; // Count Value

                $Products = products::where('product_category_id',$value->id)->where('status',1)->orderBy('id','DESC')->get();  // get data for products
                
                foreach($Products as $item){ // Foreach For Products
                    # Array for Product Listing
                    $pro_ducts[$key_1++] = collect([
                        "id" =>  $item->id,
                        "product_name" =>  $item->product_name,
                        "product_category_id" =>  $item->product_category_id,
                        "product_subcategory_id" =>  $item->product_subcategory_id,
                        "product_description" =>  $item->product_description,
                        "product_specs" =>  $item->product_specs,
                        "product_photo" =>  $item->product_photo,
                        "product_pdf" =>  $item->product_pdf,
                        "product_price" =>  $item->product_price,
                        "product_start_price" =>  $item->product_start_price,
                        "status" =>  $item->status,
                        "created_at" =>  $item->created_at,
                    ]);
                }
                
            # Array For Category
            $tag[$key] = collect([
                "id" =>  $value->id,
                "category_titile" =>  $value->category_titile,
                "description" =>  $value->description,
                "category_image" =>  $value->category_image,
                "status" =>  $value->status,
                "created_at" =>  $value->created_at,
                "products" =>  $pro_ducts,
            ]);
        }

        return $tag;  // Returning data
        # end 
    }  
    public function ProductAll()
    {
        $tag = $this->LoadNavProducts();  // Calling function
        $toReturn['tag']=$tag;  // Adding to toReturn array
        $toReturn['Products']=products::where('status',1)->orderBy('id','DESC')->get();
        return view('website.product')->with('toReturn',$toReturn);
    }
    public function ProductDetails($ProductId)
    {
        $tag = $this->LoadNavProducts();  // Calling function
        $toReturn['tag']=$tag;  // Adding to toReturn array
        $toReturn['ProductDetails']=products::where('status',1)->where('id',Crypt::decrypt($ProductId))->orderBy('id','DESC')->first();
        return view('website.product_details')->with('toReturn',$toReturn);
    }
    public function viewPages($page_name)
    {
        $tag = $this->LoadNavProducts();  // Calling function
        $toReturn['tag']=$tag;  // Adding to toReturn array
        $pages_details=pages::where('page_name',$page_name)->where('status',1)->first();
        $toReturn['pages_details']=$pages_details;
        return view('website.staticPages')->with('toReturn',$toReturn);
    }
}
