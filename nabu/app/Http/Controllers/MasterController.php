<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\pages;
use App\User;
use App\products;
use App\category;
use App\slider;
use App\testimonial;
use App\contacts;
use App\destinations;
use App\product_category;
use App\product_subcategory;
use Crypt;
use Session;
use Hash;
use DB;

class MasterController extends Controller
{
    function list_pages()
    {
        $pages = pages::get();
        return view('admin.pages')->with('pages', $pages);
    }
    // Save pages Details
    public function save_pages(Request $request)
    {
        // return $request;
        if ($request->pages_id != "") {
            $validator =  $request->validate([
                'page_name' => 'required',
                'page_title' => 'required',
                'status' => 'required',
            ]);
            $pages = pages::find($request->pages_id);
            $message = "Page Updated";
        } else {
            $validator =  $request->validate([
                'page_name' => 'required',
                'page_title' => 'required',
                'status' => 'required',
            ]);
            $pages = new pages();
            $message = "Page Added";
        }
        $pages->page_name = $request->page_name;
        $pages->page_title = $request->page_title;
        $pages->page_position = $request->page_position;
        $pages->page_contain = $request->page_contain;
        $pages->page_slug = $request->page_title;
        $pages->status = $request->status;
        $pages->save();
        Session::put('success', $message);
        return redirect('admin/pages');
    }
    // Fetch pages Details
    public function fetchpages($pages_id)
    {
        $pages = pages::where('id', $pages_id)->first();
        return response()->json($pages);
    }
    // Update Status For Active and Deactive
    public function updateStatuspages($pages_id)
    {
        $pages = pages::where('id', $pages_id)->first();
        if ($pages->status == 1) {
            $pages->status = 0;
            $message = "Page Deactived";
        } else {
            $pages->status = 1;
            $message = "Page Actived";
        }
        $pages->save();
        Session::put('success', $message);
        return redirect('admin/pages');
    }
    function list_products()
    {
        $toReturn['category']=product_category::where('status', 1)->get();
        $toReturn['products'] = products::leftjoin('product_category', 'product_category.id', '=', 'products.product_category_id')
        ->select(
            'products.id as id',
            'products.product_name as product_name',
            'products.product_category_id as city_product_category_id',
            'products.product_description as product_description',
            'products.product_specs as product_specs',
            'products.product_photo as product_photo',
            'products.product_price as product_price',
            'products.created_at as created_at',
            'products.product_start_price as product_start_price',
            'products.status as status',
            'product_category.category_titile as category_titile'
        )
        ->orderBy('id','DESC')->get();
        return view('admin.products')->with('toReturn', $toReturn);
    }
    // Save products Details
    public function save_products(Request $request)
    {
        // return $request;
        if ($request->products_id != "") {
            // $validator =  $request->validate([
            //     'product_name' => 'required',
            //     'product_photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            //     'product_pdf' => 'required|max:2048',
            //     'status' => 'required',
            // ]);
            $products = products::find($request->products_id);
            $message = "Product Updated";
        } else {
            $validator =  $request->validate([
                'product_name' => 'required',
                'product_photo' => 'required',
                'product_category_id' => 'required',
                'status' => 'required',
            ]);
            $products = new products();
            $message = "Product Added";
        }
        $product_photo_name='';
        $product_pdf_name='';
        if ($request->product_photo != "") {
            $product_photo_name = $request->product_photo->getClientOriginalName();
            request()->product_photo->move('public/upload/product_image', $product_photo_name);
        }
        if ($request->product_pdf != "") {
            $product_pdf_name = $request->product_pdf->getClientOriginalName();
            request()->product_pdf->move('public/upload/product_pdf', $product_pdf_name);
        }
        $products->product_name = $request->product_name;
        $products->product_category_id = $request->product_category_id;
        $products->product_description = $request->product_description;
        $products->product_specs = $request->product_specs;
        if($product_photo_name!="")
        {
            $products->product_photo = $product_photo_name;
        }
        if($product_pdf_name)
        {
            $products->product_pdf = $product_pdf_name;
        }
        $products->product_price = $request->product_price ?? 0;
        $products->product_start_price = $request->product_start_price;
        $products->status = $request->status;
        $products->save();
        Session::put('success', $message);
        return redirect('admin/products');
    }
    // Fetch products Details
    public function fetchproducts($products_id)
    {
        $products = products::where('id', $products_id)->first();
        return response()->json($products);
    }
    // Update Status For Active and Deactive
    public function updateStatusproducts($products_id)
    {
        $products = products::where('id', $products_id)->first();
        if ($products->status == 1) {
            $products->status = 0;
            $message = "Product Deactived";
        } else {
            $products->status = 1;
            $message = "Product Actived";
        }
        $products->save();
        Session::put('success', $message);
        return redirect('admin/products');
    }
   
    function list_destinations()
    {
        $destinations = destinations::get();
        return view('admin.destinations')->with('destinations', $destinations);
    }
    // Save destinations Details
    public function save_destinations(Request $request)
    {
        // return $request;
        if ($request->destinations_id != "") {
            $validator =  $request->validate([
                'heading' => 'required',
                'description' => 'required',
                'photos' => 'required',
                'status' => 'required',
            ]);
            $destinations = destinations::find($request->destinations_id);
            $message = " Destinations Updated";
        } else {
            $validator =  $request->validate([
                'heading' => 'required',
                'description' => 'required',
                'status' => 'required',
            ]);
            $destinations = new destinations();
            $message = " Destinations Added";
        }
        $destinations_photo_name='';
        if ($request->photos != "") {
            $photos_name = $request->photos->getClientOriginalName();
            request()->photos->move('public/upload/destinations', $photos_name);
        }
        
        $destinations->heading = $request->heading;
        $destinations->description = $request->description;
        if($destinations_photo_name!="")
        {
            $destinations->photos = $destinations_photo_name;
        }
       
        $destinations->status = $request->status;
        $destinations->save();
        Session::put('success', $message);
        return redirect('admin/destinations');
    }
    // Fetch destinations Details
    public function fetchdestinations($destinations_id)
    {
        $destinations = destinations::where('id', $destinations_id)->first();
        return response()->json($destinations);
    }
    // Update Status For Active and Deactive
    public function updateStatusdestinations($destinations_id)
    {
        $destinations = destinations::where('id', $destinations_id)->first();
        if ($destinations->status == 1) {
            $destinations->status = 0;
            $message = "Destinations Deactived";
        } else {
            $destinations->status = 1;
            $message = "Destinations Actived";
        }
        $destinations->save();
        Session::put('success', $message);
        return redirect('admin/destinations');
    }
    function list_testimonial()
    {
        $testimonial = testimonial::orderBy('id','DESC')->get();
        return view('admin.testimonial')->with('testimonial', $testimonial);
    }
    // Save testimonial Details
    public function save_testimonial(Request $request)
    {
        // return $request;
        if ($request->testimonial_id != "") {
            
            $testimonial = testimonial::find($request->testimonial_id);
            $message = " testimonial Updated";
        } else {
           
            $testimonial = new testimonial();
            $message = " testimonial Added";
        }
        $customer_photo_name='';
        if ($request->customer_photo != "") {
            $customer_photo_name = $request->customer_photo->getClientOriginalName();
            request()->customer_photo->move('public/upload/testimonial', $customer_photo_name);
        }
        
        $testimonial->customer_name	 = $request->customer_name	;
        $testimonial->customer_message	 = $request->customer_message	;
        if($customer_photo_name!="")
        {
            $testimonial->customer_photo = $customer_photo_name;
        }
        $testimonial->status = $request->status;
        $testimonial->Address = $request->Address ?? "";
        $testimonial->save();
        Session::put('success', $message);
        return redirect('admin/testimonial');
    }
    // Fetch testimonial Details
    public function fetchtestimonial($testimonial_id)
    {
        $testimonial = testimonial::where('id', $testimonial_id)->first();
        return response()->json($testimonial);
    }
    // Update Status For Active and Deactive
    public function updateStatustestimonial($testimonial_id)
    {
        $testimonial = testimonial::where('id', $testimonial_id)->first();
        if ($testimonial->status == 1) {
            $testimonial->status = 0;
            $message = "Testimonial Deactived";
        } else {
            $testimonial->status = 1;
            $message = "Testimonial Actived";
        }
        $testimonial->save();
        Session::put('success', $message);
        return redirect('admin/testimonial');
    }

    

    function list_category()
    {
        $category = category::orderBy('id','DESC')->get();
        return view('admin.category')->with('category', $category);
    }
   
    // Save category Details
    public function save_category(Request $request)
    {
        // return $request;
        if ($request->category_id != "") {
            $validator =  $request->validate([
                'heading' => 'required',
                'description' => 'required',
                'status' => 'required',
            ]);
            $category = category::find($request->category_id);
            $message = "Category Updated";
        } else {
            $validator =  $request->validate([
                'heading' => 'required',
                'description' => 'required',
                'status' => 'required',
            ]);
            $category = new category();
            $message = "Category Added";
        }
        $customer_Photo_name='';
        $category->heading	 = $request->heading	;
        $category->description = $request->description?? ""	;
        $category->status = $request->status;
        $category->save();
        Session::put('success', $message);
        return redirect('admin/category');
    }
    // Fetch category Details
    public function fetchcategory($category_id)
    {
        $category = category::where('id', $category_id)->first();
        return response()->json($category);
    }
    // Update Status For Active and Deactive
    public function updateStatuscategory($category_id)
    {
        $category = category::where('id', $category_id)->first();
        if ($category->status == 1) {
            $category->status = 0;
            $message = "Category Deactived";
        } else {
            $category->status = 1;
            $message = "Category Actived";
        }
        $category->save();
        Session::put('success', $message);
        return redirect('admin/category');
    }

    // slider
    function list_slider()
    {
        // return view('admin.slider');
        $toReturn['slider'] = slider::orderBy('id','DESC')->get();
        return view('admin.slider')->with('toReturn', $toReturn);
    }

     // slider save_slider Details
     public function save_slider(Request $request)
     {
        //  return $request;
         if ($request->slider_id != "") {
             
             $slider_data = slider::find($request->slider_id);
             $message = "Slider Updated";
         } else {
            
             $slider_data = new slider();
             $message = "Slider Added";
         }
         $slider_image_name='';
         if ($request->slider_photo != "") {
             $slider_image_name = $request->slider_photo->getClientOriginalName();
             request()->slider_photo->move('public/upload/slider', $slider_image_name);
         }
         
         
         $slider_data->silder_message = $request->silder_message;
         if($slider_image_name!="")
         {
             $slider_data->slider_photo = $slider_image_name;
         }
         $slider_data->status = $request->status;
         $slider_data->save();
         Session::put('success', $message);
         return redirect('admin/slider');
     }

    // Fetch slider Details
    public function fetch_slider($id)
    {
        $product_category = slider::where('id', $id)->first();
        return response()->json($product_category);
    }

     // Update Status For Active and Deactive
     public function updateStatusSlider($id)
     {
         $slider = slider::where('id', $id)->first();
         if ($slider->Status == 1) {
             $slider->Status = 0;
             $message = "Product Category Deactived";
         } else {
             $slider->Status = 1;
             $message = "Product Category Actived";
         }
         $slider->save();
         Session::put('success', $message);
         return redirect('admin/slider');
     }


    // Product Category
    function list_product_category()
    {
        $product_category = product_category::orderBy('id','DESC')->get();
        return view('admin.product_category')->with('product_category', $product_category);
    }
    // Save product_category Details
    public function save_product_category(Request $request)
    {
        // return $request;
        if ($request->product_category_id != "") {
            
            $product_category = product_category::find($request->product_category_id);
            $message = "Product Category Updated";
        } else {
           
            $product_category = new product_category();
            $message = "Product Category Added";
        }
        $category_image_name='';
        if ($request->category_image != "") {
            $category_image_name = $request->category_image->getClientOriginalName();
            request()->category_image->move('public/upload/product_category', $category_image_name);
        }
        
        $product_category->category_titile	 = $request->category_titile;
        $product_category->description = $request->description;
        if($category_image_name!="")
        {
            $product_category->category_image = $category_image_name;
        }
        $product_category->status = $request->status;
        $product_category->save();
        Session::put('success', $message);
        return redirect('admin/product_category');
    }
    // Fetch product_category Details
    public function fetchproduct_category($product_category_id)
    {
        $product_category = product_category::where('id', $product_category_id)->first();
        return response()->json($product_category);
    }
    // Update Status For Active and Deactive
    public function updateStatusproduct_category($product_category_id)
    {
        $product_category = product_category::where('id', $product_category_id)->first();
        if ($product_category->status == 1) {
            $product_category->status = 0;
            $message = "Product Category Deactived";
        } else {
            $product_category->status = 1;
            $message = "Product Category Actived";
        }
        $product_category->save();
        Session::put('success', $message);
        return redirect('admin/product_category');
    }
    // contact listing
    function list_contact()
    {
        $toReturn['contacts'] = contacts::orderBy('id','DESC')->get();
        return view('admin.contacts')->with('toReturn', $toReturn);
    }
    // search_for_subcategory according to category
    public function search_for_subcategory(Request $Request)
    {
        $id = $Request->id;
        $data = product_subcategory::where('category_id', $id)->where('status', 1)->get();
        return $data;
    }
    public function UpdateUserDetails(Request $request)
    {
        
        $user=User::find(Auth::user()->id);
        $user->name=$request->user_name ?? "admin";
        $user->email=$request->user_email ?? "admin@gmail.com";
        $user->username=$request->user_username ?? "admin";
        $user->contact_no=$request->user_contact_no ?? "";
        $user_profile_pic='';
        if ($request->profile_pic != "") {
            $user_profile_pic = $request->profile_pic->getClientOriginalName();
            request()->profile_pic->move('public/upload/product_category', $user_profile_pic);
        }
        $user->profile_pic=$user_profile_pic ?? "";
        $user->save();
        Session::put('success', "Details is Updated");
        return back();
    }
}
