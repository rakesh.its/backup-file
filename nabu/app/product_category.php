<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product_category extends Model
{
    public $timestamps = false;
    protected $table = 'product_category';
}
