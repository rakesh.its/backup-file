<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class destinations extends Model
{
    public $timestamps = false;
    protected $table = 'destinations';
}
