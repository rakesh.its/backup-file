<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class testimonial extends Model
{
    public $timestamps = false;
    protected $table = 'testimonial';
}
