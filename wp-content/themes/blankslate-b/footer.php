<!--Start Footer-->
<footer>

<div class="footer-row2">
<div class="container">
<div class="row justify-content-between">
<div class="col-lg-3 col-sm-6  ftr-brand-pp">
<a class="navbar-brand mt30 mb25" href="#"> <img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="Logo" width="100" /></a>
<p>Bhrigusoft is a renowned web and mobile app development company & an IT Software Solutions provider based in India.</p>
<a href="#" class="btn-main bg-btn3 lnk mt20">Join Whatsapp <i class="fas fa-chevron-right fa-icon"></i><span class="circle"></span></a>
</div>
<div class="col-lg-3 col-sm-6">
<h5>Contact Us</h5>
<ul class="footer-address-list ftr-details">
<li>
<span><i class="fas fa-envelope"></i></span>
<p>Email <span> <a href="mailto:info@businessname.com">info@bhrigusoft.com</a></span></p>
</li>
<li>
<span><i class="fas fa-phone-alt"></i></span>
<p>Phone <span> <a href="tel:+91.9711696043">+91.9711696043</a></span></p>
</li>
<li>
<span><i class="fas fa-map-marker-alt"></i></span>
<p>Address <span>B2 1506 Panchsheel Green 2 </span></p>
</li>
</ul>
</div>
<div class="col-lg-2 col-sm-6">
<h5>Company</h5>
<ul class="footer-address-list link-hover">

<li><a href="javascript:void(0)">Privacy Policy</a></li>
<li><a href="javascript:void(0)">Terms and Conditions</a></li>
<li><a href="http://localhost/bhrigusoft/contact/">Contact</a></li>
</ul>
</div>
<div class="col-lg-4 col-sm-6 footer-blog-">
<h5>Latest Blogs</h5>


</div>
</div>
</div>
</div>
<div class="footer-brands">
<div class="container">
<div class="row">
<div class="col-lg-4 v-center">
<h5 class="mb10">Top App Development Companies</h5>
<p>Receive expert advice on how to improve your digital marketing strategy, to help you increase traffic, revenue and conversions for your website.</p>
</div>
<div class="col-lg-8 v-center">
<ul class="footer-badges-">
<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/about/badges-a.png" alt="badges"></a></li>
<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/about/badges-b.png" alt="badges"></a></li>
<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/about/badges-c.png" alt="badges"></a></li>
<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/about/badges-d.png" alt="badges"></a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="footer-row3">
<div class="copyright">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="footer-social-media-icons">
<a href="javascript:void(0)" target="blank"><i class="fab fa-facebook"></i></a>
<a href="javascript:void(0)" target="blank"><i class="fab fa-twitter"></i></a>
<a href="javascript:void(0)" target="blank"><i class="fab fa-instagram"></i></a>
<a href="javascript:void(0)" target="blank"><i class="fab fa-linkedin"></i></a>
<a href="javascript:void(0)" target="blank"><i class="fab fa-youtube"></i></a>
<a href="javascript:void(0)" target="blank"><i class="fab fa-pinterest-p"></i></a>
<a href="javascript:void(0)" target="blank"><i class="fab fa-vimeo-v"></i></a>
<a href="javascript:void(0)" target="blank"><i class="fab fa-dribbble"></i></a>
<a href="javascript:void(0)" target="blank"><i class="fab fa-behance"></i></a>
</div>
<div class="footer-">
<p>Copyright &copy; 2020 Bhrigu Soft (Beta Version). All rights reserved. </p>
</div>
</div>
</div>
</div>
</div>
</div>
</footer>
<!--End Footer-->
<!--scroll to top-->
<a id="scrollUp" href="#top"></a>
<!-- js placed at the end of the document so the pages load faster -->
<script src="<?php bloginfo('template_url'); ?>/js/vendor/modernizr-3.5.0.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/popper.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/plugin.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/preloader.js"></script>
<!--common script file-->
<script src="<?php bloginfo('template_url'); ?>/js/main.js"></script>
</body>
</html>
<?php wp_footer(); ?>