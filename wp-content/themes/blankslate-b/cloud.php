<?php
/*
Template Name: Cloud
Template Post Type: post, page, event
*/
// Page code here...

get_header(2); ?>
<!--Breadcrumb Area-->
				<section class="breadcrumb-areav2" data-background="images/banner/6.jpg">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-lg-7">
								<div class="bread-titlev2">
									<h1 class="wow fadeInUp" data-wow-delay=".2s">Need A Premium & Creative Website Designing?</h1>
									<p class="mt20 wow fadeInUp" data-wow-delay=".4s">From Startup to Enterprise be ready and don't worry about design and user experience.</p>
									<a href="#" class="btn-main bg-btn2 lnk mt20 wow zoomInDown" data-wow-delay=".6s">Get Quote <i class="fas fa-chevron-right fa-icon"></i><span class="circle"></span></a>
								</div>
							</div>
						</div>
					</div>
				</section>
<!--End Breadcrumb Area-->

<?php get_footer(); ?>