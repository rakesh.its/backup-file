<?php
/*
Template Name: Web Development
Template Post Type: post, page, event
*/
// Page code here...

get_header(2); ?>
<!--Breadcrumb Area-->
				<section class="breadcrumb-areav2" data-background="images/banner/6.jpg">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-lg-7">
								<div class="bread-titlev2">
									<h1 class="wow fadeInUp txw" data-wow-delay=".2s">Need A Premium & Creative Website Designing?</h1>
									<p class="mt20 wow fadeInUp txw" data-wow-delay=".4s">From Startup to Enterprise be ready and don't worry about design and user experience.</p>
									
								</div>
							</div>
						</div>
					</div>
				</section>
				
				<!--End Hero-->
				<!--Start About-->
				<section class="service pad-tb">
					<div class="container">
						<div class="row">
							<div class="col-lg-4">
								<div class="image-block upset bg-shape wow fadeIn">
									<img src="https://bhrigusoft.com/wp-content/themes/blankslate/images/icons/creative-website-design-services.png" alt="service">
								</div>
							</div>
							<div class="col-lg-8 block-1">
								<div class="common-heading text-l pl25">
									<span>Overview</span>
									<h2>Creative Web Design Service</h2>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry.  Lorem Ipsum is simply dummy text of the printing and typesetting industry. is simply dummy text of the printing and typesetting industry. </p>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.  Lorem Ipsum is simply dummy text of the printing and typesetting industry. is simply dummy text of the printing and typesetting industry.</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--End About-->
				<!--Start Tech-->
				
				<!--End Tech-->
				<!--Start Service-->
				<section class="service-block bg-gradient6 pad-tb">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-lg-6">
								<div class="common-heading ptag">
									<span>Service</span>
									<h2>Our Services</h2>
									<p class="mb30">We think big and have hands in all leading technology platforms to provide you wide array of services.</p>
								</div>
							</div>
						</div>
						<div class="row upset link-hover">
							<div class="col-lg-4 col-sm-6 mt30 wow fadeInUp" data-wow-delay=".2s">
								<div class="s-block">
									<div class="s-card-icon"><img src="https://bhrigusoft.com/wp-content/themes/blankslate/images/icons/graphic.jpg" alt="service" class="img-fluid"/></div>
									<h4>Graphic Designing Services</h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 mt30 wow fadeInUp" data-wow-delay=".4s">
								<div class="s-block">
									<div class="s-card-icon"><img src="https://bhrigusoft.com/wp-content/themes/blankslate/images/icons/responsive-web-designing.jpg" alt="service" class="img-fluid"/></div>
									<h4>Responsive Web Designing</h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 mt30 wow fadeInUp" data-wow-delay=".6s">
								<div class="s-block">
									<div class="s-card-icon"><img src="https://bhrigusoft.com/wp-content/themes/blankslate/images/icons/static-website.jpg" alt="service" class="img-fluid"/></div>
									<h4>Static Website Designing</h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 mt30 wow fadeInUp" data-wow-delay=".8s">
								<div class="s-block">
									<div class="s-card-icon"><img src="https://bhrigusoft.com/wp-content/themes/blankslate/images/icons/dym.jpg" class="img-fluid"/></div>
									<h4>Dynamic Website Designing</h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 mt30 wow fadeInUp" data-wow-delay="1s">
								<div class="s-block">
									<div class="s-card-icon"><img src="https://bhrigusoft.com/wp-content/themes/blankslate/images/icons/psd.jpg" alt="service" class="img-fluid"/></div>
									<h4>Psd to HTML Service</h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6 mt30 wow fadeInUp" data-wow-delay="1.2s">
								<div class="s-block">
									<div class="s-card-icon"><img src="https://bhrigusoft.com/wp-content/themes/blankslate/images/icons/reg.jpg" alt="service" class="img-fluid"/></div>
									<h4>Website Redesign Service</h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								</div>
							</div>
						</div>
						
					</div>
				</section>
				<!--End Service-->
				<!--Start Process-->
				<section class="service-block pad-tb light-dark">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-lg-8">
								<div class="common-heading ptag">
									<span>Process</span>
									<h2>Our App Development Process</h2>
									<p>Our design process follows a proven approach. We begin with a deep understanding of your needs and create a planning template.</p>
								</div>
							</div>
						</div>
						<div class="row upset justify-content-center mt60">
							<div class="col-lg-4 v-center order1">
								<div class="image-block1">
									<img src="https://bhrigusoft.com/wp-content/themes/blankslate/images/icons/req.jpg" alt="Process" class="img-fluid"/>
								</div>
							</div>
							<div class="col-lg-7 v-center order2">
								<div class="ps-block">
									<span>1</span>
									<h3>Requirement Gathering</h3>
									<p>Donec metus lorem, vulputate at sapien sit amet, auctor iaculis lorem. In vel hendrerit nisi. Vestibulum eget risus velit. Aliquam tristique libero at dui sodales, et placerat orci lobortis. Maecenas ipsum neque, elementum id dignissim et, imperdiet vitae mauris.</p>
								</div>
							</div>
						</div>
						<div class="row upset justify-content-center mt60">
							<div class="col-lg-7 v-center order2">
								<div class="ps-block">
									<span>2</span>
									<h3>Prototype</h3>
									<p>Donec metus lorem, vulputate at sapien sit amet, auctor iaculis lorem. In vel hendrerit nisi. Vestibulum eget risus velit. Aliquam tristique libero at dui sodales, et placerat orci lobortis. Maecenas ipsum neque, elementum id dignissim et, imperdiet vitae mauris.</p>
								</div>
							</div>
							<div class="col-lg-4 v-center order1">
								<div class="image-block1">
									<img src="https://bhrigusoft.com/wp-content/themes/blankslate/images/icons/prototype.png" alt="Process" class="img-fluid"/>
								</div>
							</div>
						</div>
						<div class="row upset justify-content-center mt60">
							<div class="col-lg-4 v-center order1">
								<div class="image-block1">
									<img src="https://bhrigusoft.com/wp-content/themes/blankslate/images/icons/depm.jpg" alt="Process" class="img-fluid"/>
								</div>
							</div>
							<div class="col-lg-7 v-center order2">
								<div class="ps-block">
									<span>3</span>
									<h3>Deployment</h3>
									<p>Donec metus lorem, vulputate at sapien sit amet, auctor iaculis lorem. In vel hendrerit nisi. Vestibulum eget risus velit. Aliquam tristique libero at dui sodales, et placerat orci lobortis. Maecenas ipsum neque, elementum id dignissim et, imperdiet vitae mauris.</p>
								</div>
							</div>
						</div>
						<div class="row upset justify-content-center mt60">
							<div class="col-lg-7 v-center order2">
								<div class="ps-block">
									<span>4</span>
									<h3>Support & Maintenance</h3>
									<p>Donec metus lorem, vulputate at sapien sit amet, auctor iaculis lorem. In vel hendrerit nisi. Vestibulum eget risus velit. Aliquam tristique libero at dui sodales, et placerat orci lobortis. Maecenas ipsum neque, elementum id dignissim et, imperdiet vitae mauris.</p>
								</div>
							</div>
							<div class="col-lg-4 v-center order1">
								<div class="image-block1">
									<img src="https://bhrigusoft.com/wp-content/themes/blankslate/images/icons/maintenance.png" alt="Process" class="img-fluid"/>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--End Process-->
			

				


<?php get_footer(); ?>