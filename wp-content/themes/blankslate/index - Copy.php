<?php get_header(); ?>
<!--Start Hero-->
<section class="hero-card-web bg-gradient12 shape-bg3">
<div class="hero-main-rp container-fluid">
<div class="row">
<div class="col-lg-12">
<div class="hero-heading-sec">
<h2 class="wow fadeIn" data-wow-delay="0.3s"><span>Cloud & DevOps.</span> <span>Digital Transformation.</span> <span>Application Development & Management.</span> <span>Digital Marketing & SEO/ SMO and Content Marketing.</span></h2>
<p class="wow fadeIn" data-wow-delay="0.6s">Website, Software and Mobile development solution for conversing, transmuting and innovating businesses.</p>
<a href="#" class="btn-main bg-btn lnk wow fadeIn"  data-wow-delay="0.8s">View Case Studies <i class="fas fa-chevron-right fa-ani"></i><span class="circle"></span></a>
<div class="awards-block-tt  wow fadeIn" data-wow-delay="1s"><img src="<?php bloginfo('template_url'); ?>/images/hero/awards-logo.png" alt="awards-logo" class="img-fluid"/></div>
</div>
</div>
<div class="col-lg-7">

</div>
</div>
</div>
</section>
<!--End Hero-->
<!--Start About-->
<section class="about-sec-rpb pad-tb">
<div class="container">
<div class="row justify-content-center text-center">
<div class="col-lg-10">
<div class="common-heading">
<span>Digital Product Engineering & Technology </span>
<h1 class="mb30"><span class="text-second">A Digital</span> Transformation & IT Solutions Organization</h1>
<p>Bhrigusoft is a <span class="text-radius text-light text-animation bg-b">renowned</span> Digital Transformation & IT Solutions Organization we Provide Digital Product Engineering & Technology Services, We specialized in Cloud (DevOps), Software Service, Application Development and Digital Media Marketing. Our skilled team & our products are developed to bring growth to your business. We believe in delivering the services without compromising on client satisfaction and quality. </p>
<!--<h3 class="mt30 mb30">Big Ideas, creative people, new technology.</h3>
<p>Founded and run by <span class="text-bold"> young entrepreneurs </span> our center provides project-ready tech experts that will work in full compliance with your needs and objectives.<span class="text-bold">You get</span> superb service <span class="text-bold"></span> and user experience since our team take care of our client reputation and brand. </p>-->
</div>
</div>
</div>
</div>
</section>
<!--End About-->
<!--Start Service-->
<section class="service-section-prb pad-tb">
<div class="container">
<div class="row upset">
<div  data-tilt data-tilt-max="5" data-tilt-speed="1000" class="col-lg-6-cus wow fadeInUp" data-wow-delay=".2s">
<div class="service-sec-brp srvc-bg-nx bg-gradient13 text-w">
<h4 class="mb10">INTEGRATED SERVICES</h4>
<p>We craft integrated services bannered by perceptive, cut-through competition placed in strategic platforms - all these designed to narrow our clients' business and communication gaps.</p>
<a href="javascript:void(0)" class="mt20 link-prb">Learn More <i class="fas fa-chevron-right"></i></a>
</div>
</div>
<div  data-tilt data-tilt-max="5" data-tilt-speed="1000" class="col-lg-3-cus wow fadeInUp" data-wow-delay=".4s">
<div class="service-sec-list srvc-bg-nx srcl1">
<img src="<?php bloginfo('template_url'); ?>/images/icons/development.svg" alt="service">
<h5 class="mb10">Web Development</h5>
<ul class="-service-list">
<li> <a href="#">PHP</a> </li>
<li> <a href="#"><strong>.</strong>Python 3</a> </li>
<li> <a href="#">.Net</a> </li>
<li> <a href="#">Joomla</a></li>
</ul>
<p>Our skilled web developers work hard to ensure that your website is flexible, responsive and reliable.</p>
</div>
</div>
<div data-tilt data-tilt-max="5" data-tilt-speed="1000" class="col-lg-3-cus wow fadeInUp" data-wow-delay=".6s">
<div class="service-sec-list srvc-bg-nx srcl2">
<img src="<?php bloginfo('template_url'); ?>/images/icons/ecommerce.svg" alt="service">
<h5 class="mb10">Ecommerce Development</h5>
<ul class="-service-list">
<li> <a href="#">Magento </a> </li>
<li> <a href="#">WP</a> </li>
<li> <a href="#">Shopify </a> </li>
<li> <a href="#">Joomla</a></li>
</ul>
<p>E-commerce development : We customize your online store to be attractive, fast, secure, and easy to navigate.</p>
</div>
</div>
<div  data-tilt data-tilt-max="5" data-tilt-speed="1000" class="col-lg-3-cus mt30- wow fadeInUp" data-wow-delay=".8s">
<div class="service-sec-list srvc-bg-nx srcl3">
<img src="<?php bloginfo('template_url'); ?>/images/icons/app.svg" alt="service">
<h5 class="mb10">Mobile App Development</h5>
<ul class="-service-list">
<li> <a href="#">iPhone </a> </li>
<li> <a href="#">Android</a> </li>
<li> <a href="#">Cross Platform </a></li>
</ul>
<p>Mobile app: Our expert team of app developers for both Android and iOS platforms using the latest market trends and technologies to develop your mobile application eye-catching.</p>
</div>
</div>
<div  data-tilt data-tilt-max="5" data-tilt-speed="1000" class="col-lg-3-cus mt30- wow fadeInUp" data-wow-delay="1s">
<div class="service-sec-list srvc-bg-nx srcl4">
<img src="<?php bloginfo('template_url'); ?>/images/icons/tech.svg" alt="service">
<h5 class="mb10">Trending Technologies</h5>
<ul class="-service-list">
<li> <a href="#">React.JS </a> </li>
<li> <a href="#">Node.JS </a> </li>
<li> <a href="#"> Angular.JS </a></li>
</ul>
<p>Our skilled web developers work hard to ensure that your website is flexible, responsive and reliable</p>
</div>
</div>
<div  data-tilt data-tilt-max="5" data-tilt-speed="1000" class="col-lg-6-cus mt30- wow fadeInUp" data-wow-delay="1.2s">
<div class="service-sec-list srvc-bg-nx srcl5">
<img src="<?php bloginfo('template_url'); ?>/images/icons/seo.svg" alt="service">
<h5 class="mb10">Digital Marketing</h5>
<ul class="-service-list">
<li> <a href="#">SEO </a> </li>
<li> <a href="#">SMO </a> </li>
<li> <a href="#">PPC </a></li>
<li> <a href="#">PPC </a></li>
</ul>
<p>Digital Marketing:  It has exposed a lot of possibilities for marketing purposes. The procedure is very potent for making wealth online.</p>
</div>
</div>
</div>
<div class="-cta-btn mt70">
<div class="free-cta-title v-center zoomInDown wow" data-wow-delay="1.4s">
<p>What can <span>we help you achieve?</span></p>
<a href="#" class="btn-main bg-btn2 lnk">Let's get to work <i class="fas fa-chevron-right fa-icon"></i><span class="circle"></span></a>
</div>
</div>
</div>
</section>
<!--End Service-->


<!--Start work-category-->
<section class="work-category pad-tb">
<div class="container">
<div class="row">
<div class="col-lg-4 v-center">
<div class="common-heading text-l">
<span>Industries we work for</span>
<h2>Helping Businesses in All Domains</h2>
<p>Successfully delivered digital products Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
</div>
</div>
<div class="col-lg-8">
<div class="work-card-set">
<div  class="icon-set wow fadeIn" data-wow-delay=".2s">
<div class="work-card cd1">
<div class="icon-bg"><img src="<?php bloginfo('template_url'); ?>/images/icons/icon-1.png" alt="Industries" /></div>
<p>Social Networking</p>
</div>
</div>
<div  class="icon-set wow fadeIn" data-wow-delay=".4s">
<div class="work-card cd2">
<div class="icon-bg"><img src="<?php bloginfo('template_url'); ?>/images/icons/icon-2.png" alt="Industries" /></div>
<p>Digital Marketing</p>
</div>
</div>
<div class="icon-set wow fadeIn" data-wow-delay=".6s">
<div class="work-card cd3">
<div class="icon-bg"><img src="<?php bloginfo('template_url'); ?>/images/icons/icon-3.png" alt="Industries" /></div>
<p>Ecommerce Development</p>
</div>
</div>
<div class="icon-set wow fadeIn" data-wow-delay=".8s">
<div class="work-card cd4">
<div class="icon-bg"><img src="<?php bloginfo('template_url'); ?>/images/icons/icon-4.png" alt="Industries" /></div>
<p>Video Service</p>
</div>
</div>
<div class="icon-set wow fadeIn" data-wow-delay="1s">
<div class="work-card cd5">
<div class="icon-bg"><img src="<?php bloginfo('template_url'); ?>/images/icons/icon-5.png" alt="Industries" /></div>
<p>Banking Service</p>
</div>
</div>
<div class="icon-set wow fadeIn" data-wow-delay="1.2s">
<div class="work-card cd6">
<div class="icon-bg"><img src="<?php bloginfo('template_url'); ?>/images/icons/icon-6.png" alt="Industries" /></div>
<p>Enterprise Service</p>
</div>
</div>
<div class="icon-set wow fadeIn" data-wow-delay="1.4s">
<div class="work-card cd7">
<div class="icon-bg"><img src="<?php bloginfo('template_url'); ?>/images/icons/icon-7.png" alt="Industries" /></div>
<p>Education Service</p>
</div>
</div>
<div class="icon-set wow fadeIn" data-wow-delay="1.6s">
<div class="work-card cd8">
<div class="icon-bg"><img src="<?php bloginfo('template_url'); ?>/images/icons/icon-8.png" alt="Industries" /></div>
<p>Tour and Travels</p>
</div>
</div>
<div class="icon-set wow fadeIn" data-wow-delay="1.8s">
<div class="work-card cd9">
<div class="icon-bg"><img src="<?php bloginfo('template_url'); ?>/images/icons/icon-9.png" alt="Industries" /></div>
<p>Health Service</p>
</div>
</div>
<div class="icon-set wow fadeIn" data-wow-delay="2s">
<div class="work-card cd10">
<div class="icon-bg"><img src="<?php bloginfo('template_url'); ?>/images/icons/icon-10.png" alt="Industries" /></div>
<p>Event & Ticket</p>
</div>
</div>
<div class="icon-set wow fadeIn" data-wow-delay="2.2s">
<div class="work-card cd11">
<div class="icon-bg"><img src="<?php bloginfo('template_url'); ?>/images/icons/icon-11.png" alt="Industries" /></div>
<p>Restaurant Service</p>
</div>
</div>
<div class="icon-set wow fadeIn" data-wow-delay="2.4s">
<div class="work-card cd12">
<div class="icon-bg"><img src="<?php bloginfo('template_url'); ?>/images/icons/icon-12.png" alt="Industries" /></div>
<p>Business Consultant</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!--End  work-category-->

<!--why choose-->
<section class="why-choos-lg pad-tb deep-dark">
<div class="container">
<div class="row">
<div class="col-lg-6">
<div class="common-heading text-l">
<span>Why Choose Us</span>
<h2 class="mb20">Why The Bhrigusoft <span class="text-second text-bold">Ranked Top</span> Among The Leading Web & App Development Companies</h2>
<p>We believe great businesses are built on great partnerships. We deliver a collaborative, measurable, and business object-focused approach to grow your busines</p>
<div class="itm-media-object mt40 tilt-3d">
<div class="media">
<div class="img-ab- base" data-tilt data-tilt-max="20" data-tilt-speed="1000"><img src="<?php bloginfo('template_url'); ?>/images/icons/computers.svg" alt="icon" class="layer"></div>
<div class="media-body">
<h4>Streamlined Project Management</h4>
<p>We are a team of dynamic professionals in specifications varying from web designing, technology, marketing. We strive to offer the best solutions for your unique requirements</p>
</div>
</div>
<div class="media mt40">
<div class="img-ab- base" data-tilt data-tilt-max="20" data-tilt-speed="1000"><img src="<?php bloginfo('template_url'); ?>/images/icons/worker.svg" alt="icon" class="layer"></div>
<div class="media-body">
<h4>A Dedicated Team of Experts</h4>
<p>We leave no stone unturned as we understand that there should be no room for mistakes.</p>
</div>
</div>
<div class="media mt40">
<div class="img-ab- base" data-tilt data-tilt-max="20" data-tilt-speed="1000"> <img src="<?php bloginfo('template_url'); ?>/images/icons/deal.svg" alt="icon" class="layer"></div>
<div class="media-body">
<h4>Completion of Project in Given Time</h4>
<p>We deliver everything on time as our track record stands true to our commitment.</p>
</div>
</div>
</div>
</div>
</div>
<div class="col-lg-6">
<div  data-tilt data-tilt-max="5" data-tilt-speed="1000" class="single-image bg-shape-dez wow fadeIn" data-wow-duration="2s"><img src="<?php bloginfo('template_url'); ?>/images/about/about-company.jpg" alt="image" class="img-fluid"></div>
<p class="text-center mt30">We are a team Bhrigusoft. We are so glad to tell you about ourselves. We are established as one of Online Marketing expert & Web Solutions in Noida.</p>
<div class="cta-card mt60 text-center">
<h3 class="mb20">Let's Start a  <span class="text-second text-bold">New Project</span> Together</h3>
<p>Our web & online marketing work speaks for itself. We deliver an outstanding service custom-tailored to each and every one of our clients.</p>
<a href="#" class="btn-outline lnk mt30">Request A Quote    <i class="fas fa-chevron-right fa-icon"></i><span class="circle"></span></a>
</div>
</div>
</div>
</div>
</section>
<!--End why choose-->





<?php get_footer(); ?>