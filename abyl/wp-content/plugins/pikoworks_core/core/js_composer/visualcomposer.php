<?php
/**
 * @author  themepiko
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 * Function check if WC Plugin installed
 */
function pikoworks_is_wc(){
    return function_exists( 'is_woocommerce' );
}

if( is_admin() ){
   require_once PIKOWORKSCORE_CORE . 'js_composer/custom-fields.php';
   require_once PIKOWORKSCORE_CORE . 'js_composer/custom-global.php';
}
require_once PIKOWORKSCORE_CORE . 'js_composer/shortcodes/title.php';
require_once PIKOWORKSCORE_CORE . 'js_composer/shortcodes/icon_block.php';
require_once PIKOWORKSCORE_CORE . 'js_composer/shortcodes/button.php';

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) { 
/**
 *if WC Plugin active
 **/   
    
    require_once PIKOWORKSCORE_CORE . 'js_composer/shortcodes/product_categories.php';
    require_once PIKOWORKSCORE_CORE . 'js_composer/shortcodes/product_tabs.php';
   
}

require_once PIKOWORKSCORE_CORE . 'js_composer/shortcodes/progressbars.php';
require_once PIKOWORKSCORE_CORE . 'js_composer/shortcodes/timeline.php';
require_once PIKOWORKSCORE_CORE . 'js_composer/shortcodes/social.php';
require_once PIKOWORKSCORE_CORE . 'js_composer/shortcodes/newsletter.php';
require_once PIKOWORKSCORE_CORE . 'js_composer/shortcodes/brand_logo.php';
require_once PIKOWORKSCORE_CORE . 'js_composer/shortcodes/blog_post.php';
require_once PIKOWORKSCORE_CORE . 'js_composer/shortcodes/google_map.php';
require_once PIKOWORKSCORE_CORE . 'js_composer/shortcodes/instagram.php';
require_once PIKOWORKSCORE_CORE . 'js_composer/shortcodes/video-content.php';
require_once PIKOWORKSCORE_CORE . 'js_composer/shortcodes/background_slide.php';
require_once PIKOWORKSCORE_CORE . 'js_composer/shortcodes/simple_infobox.php';
require_once PIKOWORKSCORE_CORE . 'js_composer/shortcodes/pricing_table.php';

//vc_template
require_once PIKOWORKSCORE_CORE . 'js_composer/shortcodes/vc_template.php';



