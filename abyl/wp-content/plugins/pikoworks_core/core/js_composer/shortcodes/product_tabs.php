<?php
/**
 * @author  themepiko
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
add_action( 'vc_before_init', 'pikoworks_product_tabs' );
function pikoworks_product_tabs(){
 

// Setting shortcode lastest
vc_map( array(
    "name"        => esc_html__( "Product Tabs", 'pikoworks_core'),
    "base"        => "products_tabs",
    "category"    => esc_html__('Pikoworks', 'pikoworks_core' ),
    "icon" => get_template_directory_uri() . "/assets/images/logo/vc-icon.png",
    "description" => esc_html__( "Tabs and Carousel", 'pikoworks_core'),
    "params"      => array(
            array(
                    'type'          => 'dropdown',
                    'heading'       => esc_html__( 'Heading Position', 'pikoworks_core' ),
                    'param_name'    => 'header_position',
                    'value' => array(
                        esc_html__('None', 'pikoworks_core') => 'none',
                        esc_html__('Center', 'pikoworks_core') => 'tabs-heading-center',
                        esc_html__('Left', 'pikoworks_core') => 'tabs-heading-left',
                        esc_html__('Right', 'pikoworks_core') => 'tabs-heading-right',                     
                    ),
                    'admin_label' => true,
                    'std'           => 'tabs-heading-center',
            ),
            array(
                    'type'          => 'dropdown',
                    'heading'       => esc_html__( 'Active tab', 'pikoworks_core' ),
                    'param_name'    => 'active_tab',
                    'value' => array(
                        esc_html__('New Araival', 'pikoworks_core') => '1',
                        esc_html__('Popular', 'pikoworks_core') => '2',
                        esc_html__('Featured', 'pikoworks_core') => '3',
                        esc_html__('On sale', 'pikoworks_core') => '4',                     
                        esc_html__('Top Rated', 'pikoworks_core') => '5',                     
                    ),
                    'std'           => '1',
                    'description'   => esc_html__('NB:if heading position: none. This active tab dependency  enable product type other tab disable because only show one tab ', 'pikoworks_core'),
                    'admin_label' => true,
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__('Carousel Style', 'pikoworks_core'),
                'param_name' => 'is_slider',
                'value' => array(
                    esc_html__('Yes, please', 'pikoworks_core') => 'yes',
                    esc_html__('No', 'pikoworks_core') => 'no',
                    ),
                'std' => 'no',
                'admin_label' => true,
            ),
            array(
                    'type'          => 'dropdown',
                    'heading'       => esc_html__( 'Product Column', 'pikoworks_core' ),
                    'description'   => esc_html__('Default set 3 Column', 'pikoworks_core'),
                    'param_name'    => 'product_cols',
                    'value' => array(
                        esc_html__('Two Column', 'pikoworks_core') => 'two_column',
                        esc_html__('Three Column', 'pikoworks_core') => 'three_column',
                        esc_html__('Four Column', 'pikoworks_core') => 'four_column',                      
                    ),
                    'std'           => 'three_column',
                    'dependency' => array('element'   => 'is_slider', 'value'     => 'no'),
            ),
            array(
                    'type'          => 'dropdown',
                    'heading'       => esc_html__( 'New Product Tab', 'pikoworks_core' ),
                    'description'   => esc_html__('Enable/Disable Tabs', 'pikoworks_core'),
                    'param_name'    => 'new_product',
                    'value' => array(
                        esc_html__('Enable', 'pikoworks_core') => 'enable',
                        esc_html__('Disable', 'pikoworks_core') => 'disable',                    
                    ),
                    'std'           => 'enable',
                    'admin_label' => true,
            ),
            array(
                'type' => 'textfield',
                'param_name' => 'new_product_title',
                'heading' => esc_html__('New Araival Product Title', 'pikoworks_core'),
                'description'   => esc_html__('First Tab Set Default new product', 'pikoworks_core'),
                'value' => esc_html__('New Araival', 'pikoworks_core'),
                'dependency' => array( 'element'   => 'new_product', 'value'     => array( 'enable' )),
            ),
            array(
                'type' => 'textfield',
                'param_name' => 'new_post_load',
                'heading' => esc_html__('New Araival Post load', 'pikoworks_core'),
                'description'   => esc_html__('Default load 8 product', 'pikoworks_core'),
                'value' => '8',
                'dependency' => array( 'element'   => 'new_product', 'value'     => array( 'enable' )),
            ),
            array(
                    'type'          => 'dropdown',
                    'heading'       => esc_html__( 'Popular Product Tab', 'pikoworks_core' ),
                    'description'   => esc_html__('Enable/Disable Tabs', 'pikoworks_core'),
                    'param_name'    => 'popular_product',
                    'value' => array(
                        esc_html__('Enable', 'pikoworks_core') => 'enable',
                        esc_html__('Disable', 'pikoworks_core') => 'disable',                    
                    ),
                    'std'           => 'enable',
                    'admin_label' => true,
            ),
            array(
                'type' => 'textfield',
                'param_name' => 'popular_product_title',
                'heading' => esc_html__('Popular Product Title', 'pikoworks_core'),
                'value' => 'Popular',
                'dependency' => array(
                                'element'   => 'popular_product',
                                'value'     => array( 'enable' ),
                            ),
            ),
            array(
                'type' => 'textfield',
                'param_name' => 'popular_post_load',
                'heading' => esc_html__('Popular Post load', 'pikoworks_core'),
                'description'   => esc_html__('Default load 8 product', 'pikoworks_core'),
                'value' => '8',
                'dependency' => array(
                                'element'   => 'popular_product',
                                'value'     => array( 'enable' ),
                            ),
            ),
            array(
                    'type'          => 'dropdown',
                    'heading'       => esc_html__( 'Featured Product Tab', 'pikoworks_core' ),
                    'description'   => esc_html__('Enable/Disable Tabs', 'pikoworks_core'),
                    'param_name'    => 'featured_product',
                    'value' => array(
                        esc_html__('Enable', 'pikoworks_core') => 'enable',
                        esc_html__('Disable', 'pikoworks_core') => 'disable',                    
                    ),
                    'std'           => 'enable',
                    'admin_label' => true,
            ),
            array(
                'type' => 'textfield',
                'param_name' => 'featured_product_title',
                'heading' => esc_html__('Featured Product Title', 'pikoworks_core'),
                'value' => 'Featured',
                'dependency' => array(
                                'element'   => 'featured_product',
                                'value'     => array( 'enable' ),
                            ),
            ),
            array(
                'type' => 'textfield',
                'param_name' => 'featured_post_load',
                'heading' => esc_html__('Featured Post load', 'pikoworks_core'),
                'description'   => esc_html__('Default load 4 product', 'pikoworks_core'),
                'value' => '4',
                'dependency' => array(
                                'element'   => 'featured_product',
                                'value'     => array( 'enable' ),
                            ),
            ),
            array(
                    'type'          => 'dropdown',
                    'heading'       => esc_html__( 'Onsale Product Tab', 'pikoworks_core' ),
                    'description'   => esc_html__('Enable/Disable Tabs', 'pikoworks_core'),
                    'param_name'    => 'onsale_product',
                    'value' => array(
                        esc_html__('Enable', 'pikoworks_core') => 'enable',
                        esc_html__('Disable', 'pikoworks_core') => 'disable',                    
                    ),
                    'std'           => 'disable',
                    'admin_label' => true,
            ),
            array(
                'type' => 'textfield',
                'param_name' => 'onsale_product_title',
                'heading' => esc_html__('Onsale Product Title', 'pikoworks_core'),
                'value' => 'Onsale',
                'dependency' => array(
                                'element'   => 'onsale_product',
                                'value'     => array( 'enable' ),
                            ),
            ),
            array(
                'type' => 'textfield',
                'param_name' => 'onsale_post_load',
                'heading' => esc_html__('Onsale Post load', 'pikoworks_core'),
                'description'   => esc_html__('Default load 8 product', 'pikoworks_core'),
                'value' => '8',
                'dependency' => array(
                                'element'   => 'onsale_product',
                                'value'     => array( 'enable' ),
                            ),
            ),
            array(
                    'type'          => 'dropdown',
                    'heading'       => esc_html__( 'Top Rated Product Tab', 'pikoworks_core' ),
                    'description'   => esc_html__('Enable/Disable Tabs', 'pikoworks_core'),
                    'param_name'    => 'toprated_product',
                    'value' => array(
                        esc_html__('Enable', 'pikoworks_core') => 'enable',
                        esc_html__('Disable', 'pikoworks_core') => 'disable',                    
                    ),
                    'std'           => 'disable',
                    'admin_label' => true,
            ),
            array(
                'type' => 'textfield',
                'param_name' => 'toprated_product_title',
                'heading' => esc_html__('Top Rated Product Title', 'pikoworks_core'),
                'value' => 'Top Rated',
                'dependency' => array(
                                'element'   => 'toprated_product',
                                'value'     => array( 'enable' ),
                            ),
            ),
            array(
                'type' => 'textfield',
                'param_name' => 'top_post_load',
                'heading' => esc_html__('Top Rated Post load', 'pikoworks_core'),
                'description'   => esc_html__('Default load 8 product', 'pikoworks_core'),
                'value' => '8',
                'dependency' => array(
                                'element'   => 'toprated_product',
                                'value'     => array( 'enable' ),
                            ),
            ),            
            array(
                "type"        => "textfield",
                "heading"     => esc_html__( "Extra class name", 'pikoworks_core' ),
                "param_name"  => "el_class",
                "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer" ),
            ),    
            
             // Carousel
                array(
                    'type'  => 'dropdown',
                    'value' => array(
                        esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                        esc_html__( 'No', 'pikoworks_core' )  => 'false'
                    ),
                    'std'         => 'false',
                    'heading'     => esc_html__( 'AutoPlay', 'pikoworks_core' ),
                    'param_name'  => 'autoplay',
                    'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                    'dependency' => array(
                                    'element'   => 'is_slider',
                                    'value'     => array( 'yes' ),
                                ),
                    'admin_label' => false
                        ),
                array(
                    'type'  => 'dropdown',
                    'value' => array(
                        esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                        esc_html__( 'No', 'pikoworks_core' )  => 'false'
                    ),
                    'std'         => 'false',
                    'heading'     => esc_html__( 'Navigation', 'pikoworks_core' ),
                    'param_name'  => 'navigation',
                    'description' => esc_html__( "Show buton 'next' and 'prev' buttons.", 'pikoworks_core' ),
                    'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                    'dependency' => array(
                                    'element'   => 'is_slider',
                                    'value'     => array( 'yes' ),
                                ),
                    'admin_label' => false,
                ),                           
                array(
                    'type'  => 'dropdown',
                    'value' => array(
                        esc_html__( 'Nav Center', 'pikoworks_core' ) => 'owl-nav-show-inner',
                        esc_html__( 'Outside Center', 'pikoworks_core' )  => 'owl-nav-show',
                        esc_html__( 'Hover Center', 'pikoworks_core' )  => 'owl-nav-show-hover'
                    ),
                    'std'         => 'owl-nav-show-inner',
                    'heading'     => esc_html__( 'Next/Prev Button', 'pikoworks_core' ),
                    'param_name'  => 'navigation_btn',
                    'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                    'dependency' => array(
                                'element'   => 'navigation',
                                'value'     => array( 'true' ),
                            ),
                ),
                array(
                    'type'  => 'dropdown',
                    'value' => array(
                        esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                        esc_html__( 'No', 'pikoworks_core' )  => 'false'
                    ),
                    'std'         => 'false',
                    'heading'     => esc_html__( 'Bullets', 'pikoworks_core' ),
                    'param_name'  => 'dots',
                    'description' => esc_html__( "Show Carousel bullets bottom", 'pikoworks_core' ),
                    'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                    'dependency' => array(
                                    'element'   => 'is_slider',
                                    'value'     => array( 'yes' ),
                                ),
                    'admin_label' => false,
                ),
                array(
                    'type'  => 'dropdown',
                    'value' => array(
                        esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                        esc_html__( 'No', 'pikoworks_core' )  => 'false'
                    ),
                    'std'         => 'false',
                    'heading'     => esc_html__( 'Loop', 'pikoworks_core' ),
                    'param_name'  => 'loop',
                    'description' => esc_html__( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'pikoworks_core' ),
                    'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                    'dependency' => array(
                                    'element'   => 'is_slider',
                                    'value'     => array( 'yes' ),
                                ),
                    'admin_label' => false,
                    ),
                array(
                    "type"        => "pikoworks_number",
                    "heading"     => esc_html__("Slide Speed", 'pikoworks_core'),
                    "param_name"  => "slidespeed",
                    "value"       => "200",
                    "suffix"      => esc_html__("milliseconds", 'pikoworks_core'),
                    "description" => esc_html__('Slide speed in milliseconds', 'pikoworks_core'),
                    'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                    'dependency' => array(
                                    'element'   => 'is_slider',
                                    'value'     => array( 'yes' ),
                                ),
                    'admin_label' => false,
                        ),
                array(
                    "type"        => "pikoworks_number",
                    "heading"     => esc_html__("Margin", 'pikoworks_core'),
                    "param_name"  => "margin",
                    "value"       => "30",
                    "suffix"      => esc_html__("px", 'pikoworks_core'),
                    "description" => esc_html__('Distance( or space) between 2 item', 'pikoworks_core'),
                    'dependency' => array(
                                    'element'   => 'is_slider',
                                    'value'     => array( 'yes' ),
                                ),
                    'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' )
                        ),
               
                array(
                    "type"        => "pikoworks_number",
                    "heading"     => esc_html__("The items on large Device (Screen resolution of device >= 1200px )", 'pikoworks_core'),
                    "param_name"  => "items_large_device",
                    "value"       => "4",
                    "suffix"      => esc_html__("item", 'pikoworks_core'),
                    "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
                    'dependency' => array(
                                    'element'   => 'is_slider',
                                    'value'     => array( 'yes' ),
                                ),
                    'group'       => esc_html__( 'Responsive', 'pikoworks_core' ),
                  ),
                array(
                    "type"        => "pikoworks_number",
                    "heading"     => esc_html__("The items on desktop (Screen resolution of device >= 992px < 1199px )", 'pikoworks_core'),
                    "param_name"  => "items_desktop",
                    "value"       => "3",
                    "suffix"      => esc_html__("item", 'pikoworks_core'),
                    "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
                    'dependency' => array(
                                    'element'   => 'is_slider',
                                    'value'     => array( 'yes' ),
                                ),
                    'group'       => esc_html__( 'Responsive', 'pikoworks_core' ),
                  ),
                array(
                    "type"        => "pikoworks_number",
                    "heading"     => esc_html__("The items on tablet (Screen resolution of device >=768px and < 992px )", 'pikoworks_core'),
                    "param_name"  => "items_tablet",
                    "value"       => "2",
                    "suffix"      => esc_html__("item", 'pikoworks_core'),
                    "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
                    'dependency' => array(
                                    'element'   => 'is_slider',
                                    'value'     => array( 'yes' ),
                                ),
                    'group'       => esc_html__( 'Responsive', 'pikoworks_core' ),
                        ),
                array(
                    "type"        => "pikoworks_number",
                    "heading"     => esc_html__("The items on mobile (Screen resolution of device < 768px)", 'pikoworks_core'),
                    "param_name"  => "items_mobile",
                    "value"       => "1",
                    "suffix"      => esc_html__("item", 'pikoworks_core'),
                    "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
                    'dependency' => array(
                                    'element'   => 'is_slider',
                                    'value'     => array( 'yes' ),
                                ),
                   'group'       => esc_html__( 'Responsive', 'pikoworks_core' ),
                 ),
                array(
                    'type' => 'css_editor',
                    'heading' => esc_html__('Css', 'pikoworks_core'),
                    'param_name' => 'css',
                    'group' => esc_html__('Design options', 'pikoworks_core')
                ),
         
    )
));
}
class WPBakeryShortCode_products_tabs extends WPBakeryShortCode { 
    
    protected function content($atts, $content = null) {
        $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'products_tabs', $atts ) : $atts;
        $atts = shortcode_atts( array(   
        'header_position' => 'tabs-heading-center',
        'active_tab' => '1',
        'new_product' => 'enable',
        'new_product_title' => esc_html__('New Araival','pikoworks_core'),
        'new_post_load' => '8',        
        'popular_product' => 'enable',
        'popular_product_title' => esc_html__('Popular','pikoworks_core'),
        'popular_post_load' => '8',  
        'featured_product' => 'enable',
        'featured_product_title' => esc_html__('Featured','pikoworks_core'),
        'featured_post_load' => '4',  
        'onsale_product' => 'disable',
        'onsale_product_title' => esc_html__('Onsale','pikoworks_core'),
        'onsale_post_load' => '8',  
        'toprated_product' => 'disable',
        'toprated_product_title' => esc_html__('Top Rated','pikoworks_core'),
        'top_post_load' => '8',
	'product_cols' => 'three_column',  
         
        'is_slider'      => 'no',
        //Carousel
        'autoplay'      => "false",
        'navigation'    => "false",
        'navigation_btn' => 'owl-nav-show',
        'slidespeed'    => 250,
        'loop'          => "false",
        'dots'         => "false",
        'margin'        => 10,                 
        //Default
        'use_responsive' => 1,
        'sanam' => '',
        'items_large_device'   => 4,
        'items_desktop'   => 3,
        'items_tablet'   => 2,
        'items_mobile'   => 1,           
            
        'el_class'     =>  '',
        'css' => ''           
            
        ), $atts );
        extract($atts);  
        
        
//column view  
if ( $product_cols == 'two_column' ) {
	$cols = 'col-xs-12 col-sm-6 col-md-6 ptabs column';
        $cols_count = 'columns-2';
}elseif ( $product_cols == 'three_column' ) {
	$cols = 'col-xs-12 col-sm-6 col-md-4 ptabs column';
        $cols_count = 'columns-3';
}elseif ( $product_cols == 'four_column' ) {
    $cols = 'col-xs-12 col-sm-6 col-md-3 ptabs column' ;
    $cols_count = 'columns-4';
}
        
        
    $css_class = 'vc-product-tab products-grid ' .$cols_count . ' ' . $el_class ;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;  

//product type
$args_new_araival = array(
    'post_type' => 'product',
    'post_status' => 'publish',
    'posts_per_page' => esc_attr( $new_post_load ),                        
);
$args_popular = array(
    'post_type' => 'product',
    'post_status' => 'publish',
    'posts_per_page' => esc_attr( $popular_post_load ),
    'meta_key' => 'total_sales',
    'orderby' => 'meta_value_num'
);
$product_visibility_term_ids = wc_get_product_visibility_term_ids();
$args_featured = array(
    'post_type' => 'product',
    'post_status' => 'publish',
    'posts_per_page' => esc_attr( $featured_post_load ),
    'no_found_rows'  => 1,
    'meta_query'     => array(),
    'tax_query'      => array(
            'relation' => 'AND',
        array(
                'taxonomy' => 'product_visibility',
                'field'    => 'term_taxonomy_id',
                'terms'    => $product_visibility_term_ids['featured'],
        ),
    ),    
);

$args_on_sale = array(
    'post_type' => 'product',
    'post_status' => 'publish',
    'posts_per_page' => esc_attr( $onsale_post_load ),
    'meta_query' => array(
        array(
            'key' => '_visibility',
            'value' => array('catalog', 'visible'),
            'compare' => 'IN'
            ),
        array(
            'key' => '_sale_price',
            'value' =>  0,
            'compare'   => '>',
            'type'      => 'NUMERIC'
            )
        )
  );
$args_top_rated = array(
    'post_type' => 'product',
    'post_status' => 'publish',
    'posts_per_page' => esc_attr( $top_post_load ),
    'meta_key' => '_wc_average_rating',
    'orderby' => 'meta_value_num'
);

$new_tab_active = $popular_tab_active = $featured_tab_active = $onsale_tab_active = $toprated_tab_active  = '';
if($active_tab == 1){
    $new_tab_active = 'active';
}elseif($active_tab == 2){
    $popular_tab_active = 'active';
}elseif($active_tab == 3){
    $featured_tab_active = 'active';
}elseif($active_tab == 4){
    $onsale_tab_active = 'active';
}elseif($active_tab == 5){
    $toprated_tab_active = 'active';
}

$head_new = $head_toprated = $head_popular = $head_featued = $head_onsale = '';
//tab heading
if( $new_product == 'enable'){
  $head_new = '<li class="' .esc_attr($new_tab_active).'"><a data-toggle="tab" href="##produtA">'. esc_attr($new_product_title) .'</a></li>'; 
}
if( $popular_product == 'enable'){
  $head_popular = '<li class="' .esc_attr($popular_tab_active).'"><a data-toggle="tab" href="#produtB">'. esc_attr($popular_product_title) .'</a></li>'; 
}
if( $featured_product == 'enable'){
  $head_featued = '<li class="' .esc_attr($featured_tab_active).'"><a data-toggle="tab" href="#produtC">'. esc_attr($featured_product_title) .'</a></li>'; 
}
if( $onsale_product == 'enable'){ 
  $head_onsale = '<li class="' .esc_attr($onsale_tab_active).'"><a data-toggle="tab" href="#produtD">'. esc_attr($onsale_product_title) .'</a></li>';  
}
if( $toprated_product == 'enable'){
  $head_toprated = '<li  class="' .esc_attr($toprated_tab_active).'"><a data-toggle="tab" href="#produtE">'. esc_attr($toprated_product_title) .'</a></li>';  
}


$data_carousel = array(
        "autoplay"      => esc_html($autoplay),
        "navigation"    => esc_html($navigation),
        "margin"        => esc_html($margin),
        "smartSpeed"    => esc_html($slidespeed),
        "loop"          => esc_html($loop),
        "autoheight"    => "false",
        'nav'           => esc_html($navigation),
        'dots'          => esc_html($dots),
    );
    $arr = array(
        '0' => array(
            "items" => esc_html($items_mobile)
        ), 
        '768' => array(
            "items" => esc_html($items_tablet)
        ), 
        '992' => array(
            "items" => esc_html($items_desktop)
        ),
        '1200' => array(
            "items" => esc_html($items_large_device)
        )
    );
    $data_responsive = json_encode($arr);
    $data_carousel["responsive"] = $data_responsive;

    $owl_tabs = '';
    $row = 'row';
    $owl_class = '';
    $owl_data_option = '';
    if($is_slider == 'yes'){
        $owl_class = 'owl-carousel' . ' ' . $navigation_btn;
        $owl_data_option = _data_carousel( $data_carousel);
        $cols = 'column ';
        $row = '';
        $owl_tabs = 'slide';
    }
        
        ob_start();
            ?>
            
            <div class="<?php echo esc_attr( $css_class ) ?>">
                <div class="<?php echo esc_attr($row); ?>">
                    <?php if($header_position != 'none'): ?>
                    <div class="<?php echo esc_attr( $header_position ) ?>">
                        <ul class="nav text-uppercase nav-tabs">
                            <?php echo balanceTags($head_new); ?>
                            <?php echo balanceTags($head_popular); ?>
                            <?php echo balanceTags($head_featued); ?>
                            <?php echo balanceTags($head_onsale); ?>
                            <?php echo balanceTags($head_toprated); ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                <div class="tab-content products <?php echo esc_attr($owl_tabs); ?>">
                    <?php
                     if( class_exists('WooCommerce') && $new_product == 'enable' ):
                         echo '<div id="produtA" class="tab-pane '.esc_attr($new_tab_active).'"> <div class="' . esc_attr($owl_class) . '" ' .  $owl_data_option .'>';
                            $loop_new_araival = new WP_Query( $args_new_araival );
                            if ( $loop_new_araival->have_posts() ) {
                                    while ( $loop_new_araival->have_posts() ) : $loop_new_araival->the_post();?>
                                        <article <?php post_class( $cols ); ?>>
                                            <?php wc_get_template_part( 'vc', 'content-tabs' ); ?>
                                        </article>
                                    <?php    
                                    endwhile;
                            } else {
                                   esc_html_e( 'No products found', 'pikoworks_core' );
                            }
                            wp_reset_postdata();
                         echo '</div></div>'; // end #productA 
                       endif; //woocommerce
                      //popular product
                     if( class_exists('WooCommerce') && $popular_product == 'enable' ):
                         echo '<div id="produtB" class="tab-pane '.esc_attr($popular_tab_active).'"> <div class="' . esc_attr($owl_class) . '" ' .  $owl_data_option .'>';
                            $loop_popular = new WP_Query( $args_popular );
                            if ( $loop_popular->have_posts() ) {
                                    while ( $loop_popular->have_posts() ) : $loop_popular->the_post();?>
                                        <article <?php post_class( $cols ); ?>>
                                           <?php wc_get_template_part( 'vc', 'content-tabs' ); ?>
                                    </article>
                                    <?php    
                                    endwhile;
                            } else {
                                   esc_html_e( 'No products found', 'pikoworks_core' );
                            }
                            wp_reset_postdata(); 
                            echo '</div></div>'; // end #productB
                       endif; //woocommerce
                       //featured product
                     if( class_exists('WooCommerce') && $featured_product == 'enable' ):
                         echo '<div id="produtC" class="tab-pane '.esc_attr($featured_tab_active).'"> <div class="' . esc_attr($owl_class) . '" ' .  $owl_data_option .'>';
                            $loop_featured = new WP_Query( $args_featured );
                            if ( $loop_featured->have_posts() ) {
                                    while ( $loop_featured->have_posts() ) : $loop_featured->the_post();?>
                                        <article <?php post_class( $cols ); ?>>    
                                          <?php wc_get_template_part( 'vc', 'content-tabs' ); ?>
                                    </article>
                                    <?php    
                                    endwhile;
                            } else {
                                   esc_html_e( 'No products found', 'pikoworks_core' );
                            }
                            wp_reset_postdata(); 
                            echo '</div></div>'; // end #productC
                       endif; //woocommerce
                       //on sale product
                     if( class_exists('WooCommerce') && $onsale_product == 'enable' ):
                         echo '<div id="produtD" class="tab-pane '.esc_attr($onsale_tab_active).'"> <div class="' . esc_attr($owl_class) . '" ' .  $owl_data_option .'>';
                            $loop_onsale = new WP_Query( $args_on_sale );
                            if ( $loop_onsale->have_posts() ) {
                                    while ( $loop_onsale->have_posts() ) : $loop_onsale->the_post();?>
                                        <article <?php post_class( $cols ); ?>>    
                                            <?php wc_get_template_part( 'vc', 'content-tabs' ); ?>
                                    </article>
                                    <?php    
                                    endwhile;
                            } else {
                                   esc_html_e( 'No products found', 'pikoworks_core' );
                            }
                            wp_reset_postdata(); 
                            echo '</div></div>'; // end #productC
                       endif; //woocommerce
                       //top rated product
                     if( class_exists('WooCommerce') && $toprated_product == 'enable' ):
                         echo '<div id="produtE" class="tab-pane '.esc_attr($toprated_tab_active).'"> <div class="' . esc_attr($owl_class) . '" ' .  $owl_data_option .'>';
                            $loop_toprated = new WP_Query( $args_top_rated );
                            if ( $loop_toprated->have_posts() ) {
                                    while ( $loop_toprated->have_posts() ) : $loop_toprated->the_post();?>
                                        <article <?php post_class( $cols ); ?>>    
                                           <?php wc_get_template_part( 'vc', 'content-tabs' ); ?>
                                    </article>
                                    <?php    
                                    endwhile;
                            } else {
                                   esc_html_e( 'No products found', 'pikoworks_core' );
                            }
                            wp_reset_postdata(); 
                            echo '</div></div>'; // end #productC
                       endif; //woocommerce           
                    ?>
                    </div> <!--end of .tab-content-->
                </div><!--end of .row-->
            </div>
        <?php
        $result = ob_get_contents();
        ob_clean();
        return $result;
    }    
    
}