<?php
/**
 * @author  themepiko
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
add_action( 'vc_before_init', 'pikoworks_brand_logo' );
function pikoworks_brand_logo(){

// Setting shortcode lastest
vc_map( array(
    "name"        => esc_html__( "Brand logo", 'pikoworks_core'),
    "base"        => "pikoworks_brand_logo",
    "category"    => esc_html__('Pikoworks', 'pikoworks_core' ),
    "icon" => get_template_directory_uri() . "/assets/images/logo/vc-icon.png",
    "description" => esc_html__( "Show our brand client logo", 'pikoworks_core'),
    "params"      => array(
        
        array(
                'type' => 'attach_images',
                'heading' => esc_html__( 'Images', 'pikoworks_core' ),
                'param_name' => 'images',
                'value' => '',
                'description' => esc_html__( 'Select images from media library.', 'pikoworks_core' ),
        ),
        array(
                'type' => 'textfield',
                'heading' => esc_html__( 'image size', 'pikoworks_core' ),
                'param_name' => 'img_size',
                'value' => 'full',
                'admin_label' => true,
                'description' => esc_html__( 'Enter image size. Example: thumbnail, medium, large, or full is sizes defined by current image size. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "full" is image size.', 'pikoworks_core' ),
        ),
        array(
            'type' => 'dropdown',
            'heading' => esc_html__( 'Image hover effect', 'pikoworks_core' ),
            'param_name' => 'hover_effect',
            'admin_label' => true,
            'value' => array(
                esc_html__('hover effect 01', 'pikoworks_core') => 'hover01',
                esc_html__('hover effect 02', 'pikoworks_core') => 'hover02',
                esc_html__('hover effect 03', 'pikoworks_core') => 'hover03',
            ),
        ),
        array(
            'type' => 'checkbox',
            'heading' => esc_html__( 'Hide Brand name buttons', 'pikoworks_core' ),
            'param_name' => 'brand_name',
            'description' => esc_html__( 'If checked, brand name buttons will be hidden.', 'pikoworks_core' ),
            'value' => array( esc_html__( 'Yes', 'pikoworks_core' ) => 'yes' ),
        ),
        array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'On click action', 'pikoworks_core' ),
                'param_name' => 'onclick',
                'value' => array(
                        //esc_html__( 'Open prettyPhoto', 'pikoworks_core' ) => 'link_image',
                        esc_html__( 'None', 'pikoworks_core' ) => 'link_no',
                        esc_html__( 'Open custom links', 'pikoworks_core' ) => 'custom_link',
                ),
                'description' => esc_html__( 'Select action for click event.', 'pikoworks_core' ),
        ),
        array(
                'type' => 'exploded_textarea_safe',
                'heading' => esc_html__( 'Custom links', 'pikoworks_core' ),
                'param_name' => 'custom_links',
                'description' => esc_html__( 'Enter links for each slide (Note: divide links with linebreaks (Enter)).', 'pikoworks_core' ),
                'dependency' => array(
                        'element' => 'onclick',
                        'value' => array( 'custom_link' ),
                ),
        ),
        array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Custom link target', 'pikoworks_core' ),
                'param_name' => 'custom_links_target',
                'description' => esc_html__( 'Select how to open custom links.', 'pikoworks_core' ),
                'dependency' => array(
                        'element' => 'onclick',
                        'value' => array( 'custom_link' ),
                ),
                'value' => array(
                    esc_html__( 'Same window', 'pikoworks_core' ) => '_self',
                    esc_html__( 'New window', 'pikoworks_core' )  => '_blank'
                ),
        ),         
        
        // Carousel
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                esc_html__( 'No', 'pikoworks_core' )  => 'false'
            ),
            'std'         => 'false',
            'heading'     => esc_html__( 'AutoPlay', 'pikoworks_core' ),
            'param_name'  => 'autoplay',
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
            'admin_label' => false
		),
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                esc_html__( 'No', 'pikoworks_core' )  => 'false'
            ),
            'std'         => 'false',
            'heading'     => esc_html__( 'Navigation', 'pikoworks_core' ),
            'param_name'  => 'navigation',
            'description' => esc_html__( "Show buton 'next' and 'prev' buttons.", 'pikoworks_core' ),
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
            'admin_label' => false,
        ),
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( 'Nav Center', 'pikoworks_core' ) => 'owl-nav-show-inner',
                esc_html__( 'Outside Center', 'pikoworks_core' )  => 'owl-nav-show',
                esc_html__( 'Hover Center', 'pikoworks_core' )  => 'owl-nav-show-hover'
            ),
            'std'         => 'owl-nav-show',
            'heading'     => esc_html__( 'Next/Prev Button', 'pikoworks_core' ),
            'param_name'  => 'navigation_btn',
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
            'dependency' => array(
                        'element'   => 'navigation',
                        'value'     => array( 'true' ),
                    ),
        ),
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                esc_html__( 'No', 'pikoworks_core' )  => 'false'
            ),
            'std'         => 'false',
            'heading'     => esc_html__( 'Bullets', 'pikoworks_core' ),
            'param_name'  => 'dots',
            'description' => esc_html__( "Show Carousel bullets bottom", 'pikoworks_core' ),
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
            'admin_label' => false,
	),
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                esc_html__( 'No', 'pikoworks_core' )  => 'false'
            ),
            'std'         => 'false',
            'heading'     => esc_html__( 'Loop', 'pikoworks_core' ),
            'param_name'  => 'loop',
            'description' => esc_html__( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'pikoworks_core' ),
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
            'admin_label' => false,
            ),
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("Slide Speed", 'pikoworks_core'),
            "param_name"  => "slidespeed",
            "value"       => "200",
            "suffix"      => esc_html__("milliseconds", 'pikoworks_core'),
            "description" => esc_html__('Slide speed in milliseconds', 'pikoworks_core'),
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
            'admin_label' => false,
	  	),
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("Margin", 'pikoworks_core'),
            "param_name"  => "margin",
            "value"       => "30",
            "suffix"      => esc_html__("px", 'pikoworks_core'),
            "description" => esc_html__('Distance( or space) between 2 item', 'pikoworks_core'),
            'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' )
	  	),
        
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( 'Yes', 'pikoworks_core' ) => 1,
                esc_html__( 'No', 'pikoworks_core' )  => 0
            ),
            'std'         => 1,
            'heading'     => esc_html__( 'Single/Multiple Carousel', 'pikoworks_core' ),
            'param_name'  => 'use_responsive',
            'description' => esc_html__( "Yes is Multiple or No is Single Item carosuel", 'pikoworks_core' ),
            'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
            'admin_label' => false,            
	),
        array(
            'type'  => 'dropdown',
            'value' => array(
                esc_html__( '--- No Animation ---', 'pikoworks_core' ) => '',
                esc_html__( 'fadeIn', 'pikoworks_core' )  => 'fadeiN',
                esc_html__( 'slideInDown', 'pikoworks_core' )  => 'slidedowN',
                esc_html__( 'bounceInUp', 'pikoworks_core' )  => 'bounceRighT',
                esc_html__( 'zoomIn', 'pikoworks_core' )  => 'zoomiN',
                esc_html__( 'zoomIn two', 'pikoworks_core' )  => 'zoomin_2',
                esc_html__( 'zoomInDown', 'pikoworks_core' )  => 'zoomInDowN',
                esc_html__( 'fadeInLeft', 'pikoworks_core' )  => 'fadeInLefT',
                esc_html__( 'fadeInUp', 'pikoworks_core' )  => 'fadeInuP',
                esc_html__( 'slideInUp', 'pikoworks_core' )  => 'slideInuP',
            ),            
            'heading'     => esc_html__( 'Select Animation', 'pikoworks_core' ),
            'param_name'  => 'sanam',
            'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
            'dependency' => array(
                        'element'   => 'use_responsive',
                        'value'     => array( '0' ),
                    ),
	),
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("The items on large Device (Screen resolution of device >= 1200px )", 'pikoworks_core'),
            "param_name"  => "items_large_device",
            "value"       => "4",
            "suffix"      => esc_html__("item", 'pikoworks_core'),
            "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
            'dependency' => array(
                        'element'   => 'use_responsive',
                        'value'     => array( '1' ),
                    ),
            'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
	  ),
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("The items on desktop (Screen resolution of device >= 992px < 1199px )", 'pikoworks_core'),
            "param_name"  => "items_desktop",
            "value"       => "3",
            "suffix"      => esc_html__("item", 'pikoworks_core'),
            "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
            'dependency' => array(
                        'element'   => 'use_responsive',
                        'value'     => array( '1' ),
                    ),
            'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
	  ),
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("The items on tablet (Screen resolution of device >=768px and < 992px )", 'pikoworks_core'),
            "param_name"  => "items_tablet",
            "value"       => "2",
            "suffix"      => esc_html__("item", 'pikoworks_core'),
            "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
            'dependency' => array(
                        'element'   => 'use_responsive',
                        'value'     => array( '1' ),
                    ),
            'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
	  	),
        array(
            "type"        => "pikoworks_number",
            "heading"     => esc_html__("The items on mobile (Screen resolution of device < 768px)", 'pikoworks_core'),
            "param_name"  => "items_mobile",
            "value"       => "1",
            "suffix"      => esc_html__("item", 'pikoworks_core'),
            "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
            'dependency' => array(
                        'element'   => 'use_responsive',
                        'value'     => array( '1' ),
                    ),
           'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
	 ),       
        array(
            "type"        => "textfield",
            "heading"     => esc_html__( "Extra class name", 'pikoworks_core' ),
            "param_name"  => "el_class",
            "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer" ),
        ),
         array(
            'type'           => 'css_editor',
            'heading'        => esc_html__( 'Css', 'pikoworks_core' ),
            'param_name'     => 'css',
            'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'pikoworks_core' ),
            'group'          => esc_html__( 'Design options', 'pikoworks_core' )
	)
    )
));
}
class WPBakeryShortCode_pikoworks_brand_logo extends WPBakeryShortCode { 
    
    protected function content($atts, $content = null) {
        $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'pikoworks_brand_logo', $atts ) : $atts;
        $atts = shortcode_atts( array(   
            
            'hover_effect'   => 'hover01',      
            'onclick'   => '',      
            'custom_links'   => '',  
            'custom_links_target'   => '', 
            'img_size'   => 'full', 
            'images'   => '',
            'link_image' => '',  
            'brand_name' => '',             
            'el_class' => '',
            //Carousel            
            'autoplay'      => "false",
            'navigation'    => "false",
            'navigation_btn' => 'owl-nav-show',
            'slidespeed'    => 250,
            'loop'          => "false",
            'dots'         => "false",
            'margin'        => 10,                 
            //Default
            'use_responsive' => 1,
            'sanam' => '',
            'items_large_device'   => 4,
            'items_desktop'   => 3,
            'items_tablet'   => 2,
            'items_mobile'   => 1,
            'css'           => '',
            
            
        ), $atts );
        extract($atts);
        
        
        $css_class = 'brand-logo-slide brand-logo-style owl-nav-center ' . $el_class;
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif;
            
        $single_animation = '';
        if($use_responsive == '0'){
            $single_animation = $sanam;
        }
            
       $pretty_rand = 'link_image' === $onclick ? ' data-rel="prettyPhoto[rel-' . get_the_ID() . '-' . rand() . ']"' : '';
    
        if ( 'link_image' === $onclick ) {
            wp_enqueue_script( 'prettyphoto' );
            wp_enqueue_style( 'prettyphoto' );
        }
        if ( '' === $images ) {
                $images = '-1,-2,-3';
        }

        if ( 'custom_link' === $onclick ) {
                $custom_links = vc_value_from_safe( $custom_links );
                $custom_links = explode( ',', $custom_links );
        }

        $images = explode( ',', $images );
        $i = - 1;    

        
        ob_start();
       
            $data_carousel = array(
                "autoplay"      => esc_html($autoplay),
                "navigation"    => esc_html($navigation),
                "margin"        => esc_html($margin),
                "smartSpeed"    => esc_html($slidespeed),
                "loop"          => esc_html($loop),
                "theme"         => 'style-navigation-bottom',
                "autoheight"    => "false",
                'nav'           => esc_html($navigation),
                'dots'          => esc_html($dots),
            );
            if( $use_responsive == '1' ) {
                $arr = array(
                    '0' => array(
                        "items" => esc_html($items_mobile)
                    ), 
                    '768' => array(
                        "items" => esc_html($items_tablet)
                    ), 
                    '992' => array(
                        "items" => esc_html($items_desktop)
                    ),
                    '1200' => array(
                        "items" => esc_html($items_large_device)
                    )
                );
                $data_responsive = json_encode($arr);
                $data_carousel["responsive"] = $data_responsive;               
                
                
            }else{
                $data_carousel['items'] =  1;
                
            }         
           
          
           
            ?>
            <div class="<?php echo esc_attr( $css_class ) ?>">
                
                <div class="owl-carousel <?php echo esc_attr( $single_animation . ' ' . $navigation_btn ) ?>" <?php echo  _data_carousel( $data_carousel ); ?>>
                    
                    <?php foreach ( $images as $attach_id ) :  ?>
                        <?php

                        $i ++;
                        if ( $attach_id > 0 ) {
                                $post_thumbnail = wpb_getImageBySize( array(
                                        'attach_id' => $attach_id,
                                        'thumb_size' => $img_size,
                                ) );
                        } else {
                                $post_thumbnail = array();
                                $post_thumbnail['thumbnail'] = '<img src="' . vc_asset_url( 'vc/no_image.png' ) . '" />';
                                $post_thumbnail['p_img_large'][0] = vc_asset_url( 'vc/no_image.png' );
                        }
                        $thumbnail = $post_thumbnail['thumbnail'];
                        ?>

                    <figure class="<?php echo esc_attr( $hover_effect ) ?>">  
                            <?php if ( 'link_image' === $onclick ) :  ?>
                                    <?php $p_img_large = $post_thumbnail['p_img_large']; ?>
                                    <a class="prettyphoto" href="<?php echo $p_img_large[0] ?>" <?php echo esc_html($pretty_rand); ?>>
                                            <?php echo $thumbnail ?>
                                    </a>
                            <?php elseif ( 'custom_link' === $onclick && isset( $custom_links[ $i ] ) && '' !== $custom_links[ $i ] ) :  ?>
                                    <a href="<?php echo $custom_links[ $i ] ?>"<?php echo( ! empty( $custom_links_target ) ? ' target="' . $custom_links_target . '"' : '' ) ?>>
                                            <?php echo $thumbnail;
                                               if ( 'yes' !== $brand_name ) {
                                                    echo '<span class="overlay">' . get_the_title($attach_id) . ' </span>';
                                                }      
                                             ?>
                                    </a>
                            <?php else : ?>
                                    <?php echo $thumbnail;

                                       if ( 'yes' !== $brand_name ) {
                                            echo '<span class="overlay">' . get_the_title($attach_id) . ' </span>';
                                        }                            
                                    ?>

                            <?php endif ?>                        
                            </figure>                
                    <?php endforeach ?>
                    
                </div>
                
            </div>
            <?php       
        $result = ob_get_contents();
        ob_clean();
        return $result;
    }    
    
}