<?php
/**
 * @author  themepiko
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
add_action( 'vc_before_init', 'pikoworks_socialpage_link' );
function pikoworks_socialpage_link(){
// Setting shortcode lastest
vc_map( array(
    "name"        => esc_html__( "Social Page Link", 'pikoworks_core'),
    "base"        => "socialpage_link",
    "category"    => esc_html__('Pikoworks', 'pikoworks_core' ),
    "icon" => get_template_directory_uri() . "/assets/images/logo/vc-icon.png",
    "description" => esc_html__( "Show social page icon", 'pikoworks_core'),
    "params"      => array(
        array(
            'type' => 'dropdown',
            'param_name' => 'social_style',
            'heading' => esc_html__('Social Style', 'pikoworks_core'),
            'value' => array(
                esc_html__('Layout 01', 'pikoworks_core') => '1',
                esc_html__('Layout 02', 'pikoworks_core') => '2',
            ),
            'std'     => '1',
            'admin_label' => true,            
            'description'   => esc_html__( 'Your social page link collect from theme option', 'pikoworks_core' )
        ),
        array(
            'type' => 'dropdown',
            'param_name' => 'social_align',
            'heading' => esc_html__('Social Alignment', 'pikoworks_core'),
            'value' => array(
                esc_html__('Left', 'pikoworks_core') => 'text-left',
                esc_html__('Center', 'pikoworks_core') => 'text-center',
                esc_html__('Right', 'pikoworks_core') => 'text-right',
            ),
            'std'           => 'text-left',
            'admin_label' => true,
            
        ),
        array(
            'type' => 'textfield',
            'param_name' => 'title_text',
            'heading' => esc_html__('Title', 'pikoworks_core'),
            'value' => 'Follow Us',
            'admin_label' => true,
            'description' => esc_html__('if text empty nothing show'),
        ),     
        array(
            "type"        => "textfield",
            "heading"     => esc_html__( "Extra class name", 'pikoworks_core' ),
            "param_name"  => "el_class",
            "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer" ),
        ),
         array(
            'type'           => 'css_editor',
            'heading'        => esc_html__( 'Css', 'pikoworks_core' ),
            'param_name'     => 'css',
            'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'pikoworks_core' ),
            'group'          => esc_html__( 'Design options', 'pikoworks_core' )
	)
    )
));
}
class WPBakeryShortCode_socialpage_link extends WPBakeryShortCode { 
    
    protected function content($atts, $content = null) {
        $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'socialpage_link', $atts ) : $atts;
        $atts = shortcode_atts( array(            
            'social_style' => '1',
            'title_text' => esc_html__('Follow Us', 'pikoworks_core'),	
            'social_align' => 'text-left',
            'el_class'           => '',           
            'css'           => '',           
            
        ), $atts );
        extract($atts);
        $css_class = 'social-layout-'.$social_style .' ' . $social_align . ' ' . $el_class;
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class .= ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif;         
        
        ob_start(); ?>        
        
        <div class="<?php echo esc_attr( $css_class ) ?>">
            <?php
            if(!empty($title_text)){
            echo '<p>'. esc_attr($title_text) .'</p>';    
            }
            if ( trim( $GLOBALS['pikoworks']['twitter'] . $GLOBALS['pikoworks']['facebook'] . $GLOBALS['pikoworks']['googleplus'] . $GLOBALS['pikoworks']['dribbble'] . 
                $GLOBALS['pikoworks']['behance'] . $GLOBALS['pikoworks']['tumblr'] . $GLOBALS['pikoworks']['instagram'] . $GLOBALS['pikoworks']['pinterest'] .  $GLOBALS['pikoworks']['soundcloud'] .
                $GLOBALS['pikoworks']['youtube'] . $GLOBALS['pikoworks']['vimeo'] . $GLOBALS['pikoworks']['linkedin'] . $GLOBALS['pikoworks']['flickr'] ) != '' ) {
                    echo '<div class="social-page-icon">';
                    get_template_part( 'template-parts/social', 'items' );
                    echo '</div><!-- /.social-wrap -->';
            }
            ?>
        </div>

        <?php
        $result = ob_get_contents();
        ob_clean();
        return $result;
    }    
    
}