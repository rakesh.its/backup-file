<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
    exit;
} 

/**
 * widgets tabs
 * @author  themepiko
 * @link http://themepiko.com/
 */
 
class pikoworks_widget_tab extends WP_Widget{	
        
        function __construct() {
		parent::__construct( 'piko-widget-tabs', esc_html__('Post Tabs', 'pikoworks_core'), // Name
			array( 'description' => esc_html__( 'Popular, recent & comment tabs', 'pikoworks_core' ),  'classname'     =>  'piko-tabs-widget',  ) // Args
		);
	}
        
        function update($new_instance, $old_instance){
            $instance = $old_instance;
            $instance = array(); 
            $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        
            $instance['posts'] = $new_instance['posts'];
            $instance['comments'] = $new_instance['comments'];
            $instance['tags'] = $new_instance['tags'];
            $instance['show_popular_posts'] = $new_instance['show_popular_posts'];
            $instance['show_recent_posts'] = $new_instance['show_recent_posts'];
            $instance['show_comments'] = $new_instance['show_comments'];
            $instance['show_tags'] = $new_instance['show_tags'];
            $instance['orderby'] = $new_instance['orderby'];

            return $instance;
        }
	
	public function form($instance){
            
            if ( isset( $instance['title'] )) { 
            $title = $instance['title'];  
        }
        else { 
            $title = ''; 
        }        
        // Widget admin form
        ?> 
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title', 'pikoworks_core' ); ?>: </label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"  />
        </p>
        
        <?php 
                
		extract($instance);
		$defaults = array('posts' => 5, 'comments' => '5', 'tags' => 5, 'show_popular_posts' => 'on', 'show_recent_posts' => 'on', 'show_comments' => 'on', 'show_tags' =>  'on', 'orderby' => 'Comments');
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('orderby'); ?>">Popular Posts Order By:</label>
			<select id="<?php echo $this->get_field_id('orderby'); ?>" name="<?php echo $this->get_field_name('orderby'); ?>" class="widefat" style="width:100%;">
				<option <?php if ('Comments' == $instance['orderby']) echo 'selected="selected"'; ?>>Comments</option>
				<option <?php if ('Views' == $instance['orderby']) echo 'selected="selected"'; ?>>Views</option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('posts'); ?>"><?php esc_attr_e('Number of popular posts:','pikoworks_core'); ?></label>
			<input class="widefat" type="text" style="width: 30px;" id="<?php echo $this->get_field_id('posts'); ?>" name="<?php echo $this->get_field_name('posts'); ?>" value="<?php echo esc_attr($instance['posts']); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('tags'); ?>"><?php esc_attr_e('Number of recent posts:','pikoworks_core'); ?></label>
			<input class="widefat" type="text" style="width: 30px;" id="<?php echo $this->get_field_id('tags'); ?>" name="<?php echo $this->get_field_name('tags'); ?>" value="<?php echo esc_attr($instance['tags']); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('comments'); ?>"><?php esc_attr_e('Number of comments:','pikoworks_core'); ?></label>
			<input class="widefat" type="text" style="width: 30px;" id="<?php echo $this->get_field_id('comments'); ?>" name="<?php echo $this->get_field_name('comments'); ?>" value="<?php echo esc_attr($instance['comments']); ?>" />
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked($instance['show_popular_posts'], 'on'); ?> id="<?php echo $this->get_field_id('show_popular_posts'); ?>" name="<?php echo $this->get_field_name('show_popular_posts'); ?>" />
			<label for="<?php echo $this->get_field_id('show_popular_posts'); ?>"><?php esc_attr_e('Show popular posts','pikoworks_core'); ?></label>
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked($instance['show_recent_posts'], 'on'); ?> id="<?php echo $this->get_field_id('show_recent_posts'); ?>" name="<?php echo $this->get_field_name('show_recent_posts'); ?>" />
			<label for="<?php echo $this->get_field_id('show_recent_posts'); ?>"><?php esc_attr_e('Show recent posts','pikoworks_core'); ?></label>
		</p>
		<p>
			<input class="checkbox" type="checkbox" <?php checked($instance['show_comments'], 'on'); ?> id="<?php echo $this->get_field_id('show_comments'); ?>" name="<?php echo $this->get_field_name('show_comments'); ?>" />
			<label for="<?php echo $this->get_field_id('show_comments'); ?>"><?php esc_attr_e('Show comments','pikoworks_core'); ?></label>
		</p>
		<?php 
	}
	
	
	public function widget($args, $instance){		
		extract($args);
		extract($instance);
		?>
		<?php echo $before_widget; ?>

		<?php
		global $data, $post;
		extract($args);                
                $title = apply_filters( 'widget_title', $instance['title'] );         
        
                // before and after widget defined by themes file        
                if ( trim( $title ) != '' ) {
                    if ( isset( $args['before_title']) ){
                        echo $args['before_title'];
                        echo $title;
                        echo $args['after_title'];
                    }    
                }

		$posts = $instance['posts'];
		$comments = $instance['comments'];
		$tags_count = $instance['tags'];
		$show_popular_posts = isset($instance['show_popular_posts']) ? 'true' : 'false';
		$show_recent_posts = isset($instance['show_recent_posts']) ? 'true' : 'false';
		$show_comments = isset($instance['show_comments']) ? 'true' : 'false';
		$show_tags = isset($instance['show_tags']) ? 'true' : 'false';

		if(isset($instance['orderby'])) {
			$orderby = $instance['orderby'];
		} else {
			$orderby = 'Comments';
		}
		?>
                <ul class="nav nav-tabs">
                    <?php if($show_popular_posts == 'true'): ?>
                    <li class="active"><a data-toggle="tab" href="#sectionA"><?php esc_html_e('Popular', 'pikoworks_core' ); ?></a></li>
                    <?php endif; ?>
                    <?php if($show_recent_posts == 'true'): ?>
                    <li><a data-toggle="tab" href="#sectionB"><?php esc_html_e('New', 'pikoworks_core'); ?></a></li>
                    <?php endif; ?>
                    <?php if($show_comments == 'true'): ?>
                    <li><a data-toggle="tab" href="#sectionC"><i class="fa fa-comments-o" aria-hidden="true"></i></a></li>
                    <?php endif; ?>
                </ul>  <!--tab filter-->
                <div class="tab-content tab-widget">
                    <!--polular post-->
                    <?php if($show_popular_posts == 'true'): ?>
                    <div id="sectionA" class="tab-pane in active">
                        <?php
                            if($orderby == 'Comments') {
                                    $order_string = '&orderby=comment_count';
                            } else {
                                    $order_string = '&meta_key=webnus_views&orderby=meta_value_num';
                            }
                            $popular_posts = new WP_Query('showposts='.$posts.$order_string.'&order=DESC');
                            if($popular_posts->have_posts()): ?>
                            <?php while($popular_posts->have_posts()): $popular_posts->the_post(); ?>
                        
                                <div class="media">
                                    <div class="media-left">
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">   
                                             <?php 
                                             if( has_post_thumbnail()){
                                                the_post_thumbnail(array(64, 64));
                                                }elseif( has_post_format( 'video' ) ){ 
                                                    echo '<i class="widgets-post-icon fa fa-4x fa-video-camera" aria-hidden="true"></i>';
                                                }elseif(has_post_format( 'gallery' )){
                                                    echo '<i class="widgets-post-icon fa fa-4x fa-picture-o" aria-hidden="true"></i>';
                                                }elseif (has_post_format( 'audio' )) {
                                                    echo ' <i class="widgets-post-icon fa fa-4x fa-volume-up" aria-hidden="true"></i>';       
                                                }elseif (has_post_format( 'quote' )) {
                                                    echo '<i class="widgets-post-icon fa fa-4x fa-quote-left" aria-hidden="true"></i>';       
                                                }elseif (has_post_format( 'link' )) {
                                                     echo '<i class="widgets-post-icon fa fa-4x fa-quote-left" aria-hidden="true"></i>';       
                                                }elseif (has_post_format( 'chat' )) {
                                                        echo '<i class="widgets-post-icon fa fa-4x fa-comments-o" aria-hidden="true"></i>';  
                                                }  else {
                                                    echo '<i class="widgets-post-icon fa fa-4x fa-info-circle" aria-hidden="true"></i> ';   
                                                } 
                                             
                                             ?>
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <a href="<?php the_permalink(); ?>"  title="<?php the_title(); ?>" ><?php the_title(); ?></a><br>
                                         <i><?php echo get_the_date(); ?></i>
                                    </div>
                                </div>   <!--end tab post-->
                        
                            <?php endwhile; ?>
                        
                        <?php endif; ?>
                                
                    </div>
                    <?php endif; ?>
                    
                    <?php if($show_recent_posts == 'true'): ?>
                    <div id="sectionB" class="tab-pane">
                        <?php
                        $recent_posts = new WP_Query('showposts='.$tags_count);
                        if($recent_posts->have_posts()):
                        ?>
                        <?php while($recent_posts->have_posts()): $recent_posts->the_post(); ?>
                        
                            <div class="media">
                                    <div class="media-left">
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">   
                                             <?php 
                                             if( has_post_thumbnail()){
                                             the_post_thumbnail(array(64, 64));
                                            }elseif( has_post_format( 'video' ) ){ 
                                                echo '<i class="widgets-post-icon fa fa-4x fa-video-camera" aria-hidden="true"></i>';
                                            }elseif(has_post_format( 'gallery' )){
                                                echo '<i class="widgets-post-icon fa fa-4x fa-picture-o" aria-hidden="true"></i>';
                                            }elseif (has_post_format( 'audio' )) {
                                                echo ' <i class="widgets-post-icon fa fa-4x fa-volume-up" aria-hidden="true"></i>';       
                                            }elseif (has_post_format( 'quote' )) {
                                                echo '<i class="widgets-post-icon fa fa-4x fa-quote-left" aria-hidden="true"></i>';       
                                            }elseif (has_post_format( 'link' )) {
                                                 echo '<i class="widgets-post-icon fa fa-4x fa-quote-left" aria-hidden="true"></i>';       
                                            }elseif (has_post_format( 'chat' )) {
                                                    echo '<i class="widgets-post-icon fa fa-4x fa-comments-o" aria-hidden="true"></i>';  
                                            }  else {
                                                echo '<i class="widgets-post-icon fa fa-4x fa-info-circle" aria-hidden="true"></i> ';   
                                            } 
                                             
                                             
                                             ?>
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <a href="<?php the_permalink(); ?>"  title="<?php the_title(); ?>" ><?php the_title(); ?></a><br>
                                         <i><?php echo get_the_date(); ?></i>
                                    </div>
                                </div>   <!--end tab post-->
                                
                        <?php endwhile; ?>                      
                        
                        <?php endif; ?>
                        
                    </div>
                    <?php endif; ?>
                    
                    <?php if($show_comments == 'true'): ?>
                    <div id="sectionC" class="tab-pane">
                        <?php
                            $number = $instance['comments'];
                            global $wpdb;
                            $recent_comments = "SELECT DISTINCT ID, post_title, post_password, comment_ID, comment_post_ID, comment_author, comment_author_email, comment_date_gmt, comment_approved, comment_type, comment_author_url, SUBSTRING(comment_content,1,110) AS com_excerpt FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID = $wpdb->posts.ID) WHERE comment_approved = '1' AND comment_type = '' AND post_password = '' ORDER BY comment_date_gmt DESC LIMIT $number";
                            $the_comments = $wpdb->get_results($recent_comments);
                            foreach($the_comments as $comment) : ?>
                                <div class="media">
                                    <div class="media-left">
                                        <a href="<?php comments_link(); ?>">                                            
                                            <?php echo get_avatar($comment, '52'); ?>
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <span><?php echo strip_tags($comment->comment_author); ?> <?php esc_html_e('says:', 'pikoworks_core') ?></span>
                                        <a  href="<?php echo get_permalink($comment->ID); ?>#comment-<?php echo $comment->comment_ID; ?>" title="<?php echo strip_tags($comment->comment_author); ?> on <?php echo $comment->post_title; ?>"><?php echo strip_tags($comment->com_excerpt); ?>...</a>
                       
                                    </div>                                    
                                </div> 
                            <?php endforeach; ?>
                        
                    </div>
                    <?php endif; ?>
                </div> <!--end of tab content-->
		
		<?php echo $after_widget; ?>
		<?php 
	} 
}


