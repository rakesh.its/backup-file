<?php
/**
 * @author Themepiko
 */
if (!class_exists('piko_custom_post_shortcodes')) {
    class piko_custom_post_shortcodes  {

        private static $instance;

        public static function init()
        {
            if (!isset(self::$instance)) {
                self::$instance = new piko_custom_post_shortcodes;
                add_action('init', array(self::$instance, 'includes'), 0);
                add_action('init', array(self::$instance, 'register_vc_map'), 10);
            }
            return self::$instance;
        }

        public function includes()
        {
            include_once(ABSPATH . 'wp-admin/includes/plugin.php');
            if (!is_plugin_active('js_composer/js_composer.php')) {
                return;
            }
            require_once PIKOWORKSCORE_CORE . '/post-type/custom-fields.php';
            require_once PIKOWORKSCORE_CORE . '/js_composer/custom-fields.php';
            global $pikoworks;
            $cpt_disable = isset( $pikoworks['optn_cpt_disable'] ) ? $pikoworks['optn_cpt_disable'] : array('');
           
            if (!isset($cpt_disable) || (array_key_exists('portfolio', $cpt_disable) && ($cpt_disable['portfolio'] == '0' || $cpt_disable['portfolio'] == '' || is_array('portfolio')))) {
                include_once(PIKOWORKSCORE_CORE . 'post-type/portfolio/portfolio.php');
            }
             if (!isset($cpt_disable) || (array_key_exists('service', $cpt_disable) && ($cpt_disable['service'] == '0' || $cpt_disable['service'] == ''))) {
                include_once(PIKOWORKSCORE_CORE . 'post-type/service/service.php');
            }
             if (!isset($cpt_disable) || (array_key_exists('ourteam', $cpt_disable) && ($cpt_disable['ourteam'] == '0' || $cpt_disable['ourteam'] == ''))) {
                include_once(PIKOWORKSCORE_CORE . 'post-type/ourteam/ourteam.php');
            }
             if (!isset($cpt_disable) || (array_key_exists('testimonial', $cpt_disable) && ($cpt_disable['testimonial'] == '0' || $cpt_disable['testimonial'] == ''))) {
                include_once(PIKOWORKSCORE_CORE . 'post-type/testimonial/testimonial.php');
            }

        }

        public static function  substr($str, $txt_len, $end_txt = '...')
        {
            if (empty($str)) return '';
            if (strlen($str) <= $txt_len) return $str;

            $i = $txt_len;
            while ($str[$i] != ' ') {
                $i--;
                if ($i == -1) break;
            }
            while ($str[$i] == ' ') {
                $i--;
                if ($i == -1) break;
            }

            return substr($str, 0, $i + 1) . $end_txt;
        }


        public function register_vc_map()
        {

            global $pikoworks, $cpt_disable ;
            $cpt_disable = isset( $pikoworks['optn_cpt_disable'] ) ? $pikoworks['optn_cpt_disable'] : array('');

            if (function_exists('vc_map')) {
                
                 $pikoworks_vc_anim_effects_in = array(
                    esc_html__( '--- No Animation ---', 'pikoworks_core' ) => '',
                    esc_html__( 'bounce', 'pikoworks_core' ) => 'bounce',
                    esc_html__( 'flash', 'pikoworks_core' ) => 'flash',
                    esc_html__( 'pulse', 'pikoworks_core' ) => 'pulse',
                    esc_html__( 'rubberBand', 'pikoworks_core' ) => 'rubberBand',
                    esc_html__( 'shake', 'pikoworks_core' ) => 'shake',
                    esc_html__( 'swing', 'pikoworks_core' ) => 'swing',
                    esc_html__( 'tada', 'pikoworks_core' ) => 'tada',
                    esc_html__( 'wobble', 'pikoworks_core' ) => 'wobble',
                    esc_html__( 'jello', 'pikoworks_core' ) => 'jello',
                    esc_html__( 'bounceIn', 'pikoworks_core' ) => 'bounceIn',
                    esc_html__( 'bounceInDown', 'pikoworks_core' ) => 'bounceInDown',
                    esc_html__( 'bounceInLeft', 'pikoworks_core' ) => 'bounceInLeft',
                    esc_html__( 'bounceInRight', 'pikoworks_core' ) => 'bounceInRight',
                    esc_html__( 'bounceInUp', 'pikoworks_core' ) => 'bounceInUp',
                    esc_html__( 'fadeIn', 'pikoworks_core' ) => 'fadeIn',
                    esc_html__( 'fadeInDown', 'pikoworks_core' ) => 'fadeInDown',
                    esc_html__( 'fadeInDownBig', 'pikoworks_core' ) => 'fadeInDownBig',
                    esc_html__( 'fadeInLeft', 'pikoworks_core' ) => 'fadeInLeft',
                    esc_html__( 'fadeInLeftBig', 'pikoworks_core' ) => 'fadeInLeftBig',
                    esc_html__( 'fadeInRight', 'pikoworks_core' ) => 'fadeInRight',
                    esc_html__( 'fadeInRightBig', 'pikoworks_core' ) => 'fadeInRightBig',
                    esc_html__( 'fadeInUp', 'pikoworks_core' ) => 'fadeInUp',
                    esc_html__( 'fadeInUpBig', 'pikoworks_core' ) => 'fadeInUpBig',
                    esc_html__( 'flip', 'pikoworks_core' ) => 'flip',
                    esc_html__( 'flipInX', 'pikoworks_core' ) => 'flipInX',
                    esc_html__( 'flipInY', 'pikoworks_core' ) => 'flipInY',
                    esc_html__( 'lightSpeedIn', 'pikoworks_core' ) => 'lightSpeedIn',
                    esc_html__( 'rotateIn', 'pikoworks_core' ) => 'rotateIn',
                    esc_html__( 'rotateInDownLeft', 'pikoworks_core' ) => 'rotateInDownLeft',
                    esc_html__( 'rotateInDownRight', 'pikoworks_core' ) => 'rotateInDownRight',
                    esc_html__( 'rotateInUpLeft', 'pikoworks_core' ) => 'rotateInUpLeft',
                    esc_html__( 'rotateInUpRight', 'pikoworks_core' ) => 'rotateInUpRight',
                    esc_html__( 'slideInUp', 'pikoworks_core' ) => 'slideInUp',
                    esc_html__( 'slideInDown', 'pikoworks_core' ) => 'slideInDown',
                    esc_html__( 'slideInLeft', 'pikoworks_core' ) => 'slideInLeft',
                    esc_html__( 'slideInRight', 'pikoworks_core' ) => 'slideInRight',
                    esc_html__( 'zoomIn', 'pikoworks_core' ) => 'zoomIn',
                    esc_html__( 'zoomInDown', 'pikoworks_core' ) => 'zoomInDown',
                    esc_html__( 'zoomInLeft', 'pikoworks_core' ) => 'zoomInLeft',
                    esc_html__( 'zoomInRight', 'pikoworks_core' ) => 'zoomInRight',
                    esc_html__( 'zoomInUp', 'pikoworks_core' ) => 'zoomInUp',
                    esc_html__( 'rollIn', 'pikoworks_core' ) => 'rollIn',
                );
                
                
                $add_image_size = array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Image size', 'pikoworks_core'),
                        'param_name' => 'image_size',
                        'value' => array('360x202' => '360x202', '585x585' => '585x585', '590x393' => '590x393', '570x460', '200x140' => '200x140',),                       
                    );
                $add_css_animation = array(
                    'type'          => 'dropdown',
                    'heading'       => esc_html__( 'CSS Animation', 'pikoworks_core' ),
                    'param_name'    => 'css_animation',
                    'value'         => $pikoworks_vc_anim_effects_in,
                    'description'   => esc_html__( 'Choose your animation', 'pikoworks_core' ),
                    'group'          => esc_html__( 'Animation', 'pikoworks_core' ),
                    'admin_label' => true,
                );
                $add_delay_animation = array(
                    'type'          => 'textfield',
                    'heading'       => esc_html__( 'Animation Delay', 'pikoworks_core' ),
                    'param_name'    => 'animation_delay',
                    'std'           => '0.5',
                    'description'   => esc_html__( 'Delay unit is second.', 'pikoworks_core' ),
                    'dependency' => array(
                                    'element'   => 'css_animation',
                                    'not_empty' => true,
                                ),
                    'group'          => esc_html__( 'Animation', 'pikoworks_core' ),                    
                );
                $add_el_class = array(
                    'type' => 'textfield',
                    'heading' => esc_html__('Extra class name', 'pikoworks_core'),
                    'param_name' => 'el_class',
                    'admin_label' => false,
                    'description' => esc_html__('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'pikoworks_core'),
                );                
                if (!isset($cpt_disable) || (array_key_exists('portfolio', $cpt_disable) && ($cpt_disable['portfolio'] == '0' || $cpt_disable['portfolio'] == ''))) {
                    $portfolio_categories = get_terms(PIKO_PORTFOLIO_CATEGORY_TAXONOMY, array('hide_empty' => 0, 'orderby' => 'ASC'));
                    $portfolio_cat = array();
                    if (is_array($portfolio_categories)) {
                        foreach ($portfolio_categories as $cat) {
                            $portfolio_cat[$cat->name] = $cat->slug;
                        }
                    }
                    

                    $args = array(
                        'posts_per_page' => -1,
                        'post_type' => PIKO_PORTFOLIO_POST_TYPE,
                        'post_status' => 'publish');
                    $list_portfolio = array();
                    $post_array = get_posts($args);
                    foreach ($post_array as $post) : setup_postdata($post);
                    $list_portfolio[$post->post_title] = $post->ID;
                    endforeach;
                    wp_reset_postdata();
                    vc_map(array(
                        'name' => esc_html__('Portfolio', 'pikoworks_core'),
                        'base' => 'piko_portfolio',
                        'icon' => get_template_directory_uri() . '/assets/images/logo/vc-icon.png',
                        'description' => esc_html__( 'Display Different type Project layout', 'pikoworks_core'),
                        'category' => 'Pikoworks',
                        
                        
                        
                        'params' => array(
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Different Layout style', 'pikoworks_core'),
                                'param_name' => 'layout_type',
                                'admin_label' => true,
                                'value' => array(esc_html__('Grid', 'pikoworks_core') => 'grid',
                                    esc_html__('Title with category', 'pikoworks_core') => 'title',                                    
                                    esc_html__('Masonry Style-01', 'pikoworks_core') => 'masonry',
                                    esc_html__('Masonry Style-02', 'pikoworks_core') => 'masonry-style-02',
                                    esc_html__('Masonry Classic', 'pikoworks_core') => 'masonry-classic',
                                    esc_html__('One page', 'pikoworks_core') => 'one-page',
                                    esc_html__('Remove Tab menu', 'pikoworks_core') => 'left-menu'
                                )
                            ),                            
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Overlay Effect Style', 'pikoworks_core'),
                                'param_name' => 'overlay_style',
                                'admin_label' => true,
                                'value' => array(
                                    esc_html__('Icon', 'pikoworks_core') => 'icon',
                                    esc_html__('Title', 'pikoworks_core') => 'title',
                                    esc_html__('Title + Category', 'pikoworks_core') => 'title-category',
                                    esc_html__('Title + Category + Link button', 'pikoworks_core') => 'title-category-link',
                                    esc_html__('Title + Excerpt', 'pikoworks_core') => 'title-excerpt',
                                    esc_html__('Title + Excerpt + Link button + Align center', 'pikoworks_core') => 'title-excerpt-link',
                                    esc_html__('Title + Excerpt + Link button + Align left', 'pikoworks_core') => 'left-title-excerpt-link',
                                ),
                                'dependency' => Array('element' => 'layout_type', 'value' => array('grid', 'masonry', 'left-menu', 'title-more-link', 'more-link', 'one-page', 'masonry-style-02', 'masonry-classic'))
                            ),
                             array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Hover direction Effect', 'pikoworks_core'),
                                'param_name' => 'is_directioanl',
                                'value' => array(
                                    esc_html__('Normal', 'pikoworks_core') => '',
                                    esc_html__('Direction', 'pikoworks_core') => 'hover-dir',
                                    esc_html__('Animation', 'pikoworks_core') => 'hover-animation',
                                ),
//                                'dependency' => array(
//                                                'element'   => 'layout_style',
//                                                'value'     => array( 'style1' ),
//                                            ),                                
                            ), 
                            array(
                                'type' => 'checkbox',
                                'heading'       => 'Overlay Zoom effect',
                                'param_name' => 'hover_zoom',
                                'value' => array(esc_html__('Yes', 'pikoworks_core') => 'poverlay-style-2'),
                                'dependency' => Array('element' => 'is_directioanl', 'value' => array('', 'hover-animation'))
                            ),
                             array(
                                    'type' => 'pikoworks_number',
                                    'param_name' => 'excerpt',
                                    'heading' => esc_html__('Excerpt word', 'pikoworks_core'),
                                    'description' => esc_html__('Default use Content Excerpt 25 words ', 'pikoworks_core'),
                                    'value' => '20',
                                    'dependency' => Array('element' => 'overlay_style', 'value' => array('title-excerpt', 'title-excerpt-link', 'left-title-excerpt-link'))

                            ),
                             array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Display Category Tab Name', 'pikoworks_core'),
                                'param_name' => 'show_category',
                                'admin_label' => true,
                                'value' => array(
                                    esc_html__('None', 'pikoworks_core') => '',
                                    esc_html__('Left', 'pikoworks_core') => 'left',
                                    esc_html__('Center', 'pikoworks_core') => 'center',
                                    esc_html__('Right', 'pikoworks_core') => 'right'),
                                'dependency' => Array('element' => 'layout_type', 'value' => array('grid', 'title', 'masonry', 'masonry-classic','masonry-style-02'))
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Enable Title', 'pikoworks_core'),
                                'param_name' => 'show_title',
                                'admin_label' => true,
                                'value' => array(esc_html__('Enable', 'pikoworks_core') => 'yes',
                                    esc_html__('Disable', 'pikoworks_core') => 'no'
                                ),
                                'dependency' => Array('element' => 'show_category', 'value' => array('left','right','center'))

                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => esc_html__('Title', 'pikoworks_core'),
                                'param_name' => 'title',
                                'value' => '',
                                'dependency' => Array('element' => 'show_title', 'value' => array('yes'))

                            ),                          
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Target Source', 'pikoworks_core'),
                                'param_name' => 'data_source',
                                'admin_label' => true,
                                'value' => array(
                                    esc_html__('From Category', 'pikoworks_core') => '',
                                    esc_html__('From Title(IDs)', 'pikoworks_core') => 'list_id')
                            ),

                            array(
                                'type' => 'pikoworks_taxonomy',
                                'heading' => esc_html__('Portfolio Category', 'pikoworks_core'),
                                'param_name' => 'category',
                                'admin_label' => true,
                                "taxonomy"    => PIKO_PORTFOLIO_CATEGORY_TAXONOMY,
                                "value"       => '',
                                'parent'      => 0,
                                'multiple'    => true,
                                'description' => esc_html__('Select multiple specific Category or all Category', 'pikoworks_core'),
                                'dependency' => Array('element' => 'data_source', 'value' => array(''))
                            ),
                            array(
                                'type' => 'pikoworks_title',
                                'heading' => esc_html__('Select Portfolio', 'pikoworks_core'),
                                'param_name' => 'portfolio_ids',
                                'options' => $list_portfolio,
                                'multiple'    => true,
                                'description' => esc_html__('Select multiple specific title or all Title IDs', 'pikoworks_core'),
                                'dependency' => Array('element' => 'data_source', 'value' => array('list_id'))
                            ),                            
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Category action', 'pikoworks_core'),
                                'param_name' => 'tab_category_action',
                                'admin_label' => true,
                                'value' => array(
                                    esc_html__('Isotope Filter', 'pikoworks_core') => 'filter',
                                    esc_html__('Ajax filter', 'pikoworks_core') => 'ajax',
                                  ),
                                'dependency' => Array('element' => 'show_category', 'value' => array('left', 'center', 'right'))
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Number of column', 'pikoworks_core'),
                                'param_name' => 'column',
                                'value' => array('2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6'),
                                'dependency' => Array('element' => 'layout_type', 'value' => array('grid', 'title'))
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Number of column masonry', 'pikoworks_core'),
                                'param_name' => 'column_masonry',
                                'value' => array('3' => '3', '4' => '4', '5' => '5'),
                                'dependency' => Array('element' => 'layout_type', 'value' => array('masonry'))
                            ),
                            array(
                                'type' => 'pikoworks_number',
                                'heading' => esc_html__('Number of item per page then pagination', 'pikoworks_core'),
                                'param_name' => 'item',
                                'value' => '',
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Show Pagination After', 'pikoworks_core'),
                                'param_name' => 'show_pagging',
                                'value' => array('None' => '', esc_html__('Load more Button', 'pikoworks_core') => '1', esc_html__('Slider Owl', 'pikoworks_core') => '2'),
                                'dependency' => Array('element' => 'layout_type', 'value' => array('grid', 'title', 'one-page-ajax'))
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Show Pagination Masonry', 'pikoworks_core'),
                                'param_name' => 'show_pagging_masonry',
                                'value' => array('None' => '', esc_html__('Load more Button', 'pikoworks_core') => '1'),
                                'dependency' => Array('element' => 'layout_type', 'value' => array('masonry','masonry-classic','masonry-style-02'))
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Order Post Date By', 'pikoworks_core'),
                                'param_name' => 'order',
                                'value' => array(esc_html__('Descending', 'pikoworks_core') => 'DESC', esc_html__('Ascending', 'pikoworks_core') => 'ASC')
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Padding/ Gap each item', 'pikoworks_core'),
                                'param_name' => 'padding',
                                'value' => array(esc_html__('No padding', 'pikoworks_core') => '', '02 px' => 'col-padding-02', '05 px' => 'col-padding-05', '10 px' => 'col-padding-10', '15 px' => 'col-padding-15', '20 px' => 'col-padding-20', '40 px' => 'col-padding-40'),
                                'dependency' => Array('element' => 'layout_type', 'value' => array('grid', 'title', 'masonry', 'one-page', 'masonry-style-02', 'masonry-classic'))
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Image size', 'pikoworks_core'),
                                'param_name' => 'image_size',
                               'value' => array('360x275' => '360x275', '585x585' => '585x585', '590x393' => '590x393', '570x460'), 
                                'dependency' => Array('element' => 'layout_type', 'value' => array('grid', 'title'))
                            ),                            
                             // Carousel                           
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                                ),
                                'std'         => 'false',
                                'heading'     => esc_html__( 'AutoPlay', 'pikoworks_core' ),
                                'param_name'  => 'autoplay',
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'admin_label' => false,
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                                ),
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                                ),
                                'std'         => 'false',
                                'heading'     => esc_html__( 'Navigation', 'pikoworks_core' ),
                                'param_name'  => 'navigation',
                                'description' => esc_html__( "Show buton 'next' and 'prev' buttons.", 'pikoworks_core' ),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'admin_label' => false,
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                            ),
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Nav Center', 'pikoworks_core' ) => 'owl-nav-show-inner',
                                    esc_html__( 'Outside Center', 'pikoworks_core' )  => 'owl-nav-show',
                                    esc_html__( 'Hover Center', 'pikoworks_core' )  => 'owl-nav-show-hover'
                                ),
                                'std'         => 'owl-nav-show-inner',
                                'heading'     => esc_html__( 'Next/Prev Button', 'pikoworks_core' ),
                                'param_name'  => 'navigation_btn',
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'dependency' => array(
                                            'element'   => 'navigation',
                                            'value'     => array( 'true' ),
                                        ),
                            ),
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                                ),
                                'std'         => 'false',
                                'heading'     => esc_html__( 'Bullets', 'pikoworks_core' ),
                                'param_name'  => 'dots',
                                'description' => esc_html__( "Show Carousel bullets bottom", 'pikoworks_core' ),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'admin_label' => false,
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                            ),
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                                ),
                                'std'         => 'false',
                                'heading'     => esc_html__( 'Loop', 'pikoworks_core' ),
                                'param_name'  => 'loop',
                                'description' => esc_html__( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'pikoworks_core' ),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'admin_label' => false,
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                                ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("Slide Speed", 'pikoworks_core'),
                                "param_name"  => "slidespeed",
                                "value"       => "200",
                                "suffix"      => esc_html__("milliseconds", 'pikoworks_core'),
                                "description" => esc_html__('Slide speed in milliseconds', 'pikoworks_core'),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'admin_label' => false,
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                                    ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("Margin", 'pikoworks_core'),
                                "param_name"  => "margin",
                                "value"       => "30",
                                "suffix"      => esc_html__("px", 'pikoworks_core'),
                                "description" => esc_html__('Distance( or space) between 2 item', 'pikoworks_core'),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                            ),                            
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("The items on large Device (Screen resolution of device >= 1200px )", 'pikoworks_core'),
                                "param_name"  => "items_large_device",
                                "value"       => "4",
                                "suffix"      => esc_html__("item", 'pikoworks_core'),
                                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),                                
                                'group'       => esc_html__( 'Responsive', 'pikoworks_core' ),
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                              ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("The items on desktop (Screen resolution of device >= 992px < 1199px )", 'pikoworks_core'),
                                "param_name"  => "items_desktop",
                                "value"       => "3",
                                "suffix"      => esc_html__("item", 'pikoworks_core'),
                                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),                                
                                'group'       => esc_html__( 'Responsive', 'pikoworks_core' ),
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                              ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("The items on tablet (Screen resolution of device >=768px and < 992px )", 'pikoworks_core'),
                                "param_name"  => "items_tablet",
                                "value"       => "2",
                                "suffix"      => esc_html__("item", 'pikoworks_core'),
                                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),                                
                                'group'       => esc_html__( 'Responsive', 'pikoworks_core' ),
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                                    ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("The items on mobile (Screen resolution of device < 768px)", 'pikoworks_core'),
                                "param_name"  => "items_mobile",
                                "value"       => "1",
                                "suffix"      => esc_html__("item", 'pikoworks_core'),
                                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),                                
                               'group'       => esc_html__( 'Responsive', 'pikoworks_core' ),
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                             ),

                            $add_el_class,                            
                        )
                    ));
                }

                if (!isset($cpt_disable) || (array_key_exists('ourteam', $cpt_disable) && ($cpt_disable['ourteam'] == '0' || $cpt_disable['ourteam'] == ''))) {
                    $ourteam_cat = array();
                    $ourteam_categories = get_terms('ourteam_category', array('hide_empty' => 0, 'orderby' => 'ASC'));
                    if (is_array($ourteam_categories)) {
                        foreach ($ourteam_categories as $cat) {
                            $ourteam_cat[$cat->name] = $cat->slug;
                        }
                    }
                    vc_map(array(
                        'name' => esc_html__('Our Team', 'pikoworks_core'),
                        'base' => 'piko_ourteam',
                        'description' => esc_html__('Differnt style Team', 'pikoworks_core'),
                        'icon' => get_template_directory_uri() . '/assets/images/logo/vc-icon.png',
                        'category' => 'Pikoworks',
                        'params' => array(
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Layout Style', 'pikoworks_core'),
                                'param_name' => 'layout_style',
                                'admin_label' => true,
                                'value' => array(
                                    esc_html__('style 1', 'pikoworks_core') => 'style1', 
                                    esc_html__('style 2', 'pikoworks_core') => 'style2',
                                    esc_html__('style 3', 'pikoworks_core') => 'style3',
                                    esc_html__('style 4', 'pikoworks_core') => 'style4'
                                    ),
                                'description' => esc_html__('Select Layout Style.', 'pikoworks_core')
                            ),
                            array(
                                'type' => 'checkbox',
                                'heading' => esc_html__('Carousel Style', 'pikoworks_core'),
                                'param_name' => 'is_slider',
                                'value' => array(esc_html__('Yes, please', 'pikoworks_core') => 'yes'),
                                'dependency' => array(
                                                'element'   => 'layout_style',
                                                'value'     => array( 'style1', 'style4' ),
                                            ),                                
                            ),
                            array(
                                'type' => 'checkbox',
                                'heading' => esc_html__('Show excerpt word', 'pikoworks_core'),
                                'param_name' => 'show_excerpt',
                                'value' => array(esc_html__('Yes, please', 'pikoworks_core') => 'yes'),
                                'dependency' => array(
                                                'element'   => 'layout_style',
                                                'value'     => array( 'style4' ),
                                            ),  
                                                                
                            ),                            
                            array(
                                'type' => 'textfield',
                                'heading' => esc_html__('Column', 'pikoworks_core'),
                                'param_name' => 'column',
                                'value' => '4'
                            ),                            
                            array(
                                'type' => 'dropdown',
                                'param_name' => 'target_team',
                                'value' => array(
                                    esc_html__('All Team Member', 'pikoworks_core') => 'target_team',
                                    esc_html__('Multi Category Team', 'pikoworks_core') => 'cat_team',
                                ),                
                                'heading' => esc_html__('Target Team Member', 'pikoworks_core'),                               
                                'admin_label' => true,
                            ),                           
                            array(
                                'type' => 'pikoworks_taxonomy',
                                'heading' => esc_html__('Category', 'pikoworks_core'),
                                'param_name' => 'category',
                                'taxonomy' => 'ourteam_category',
                                'multiple'    => true,
                                'dependency' => array(
                                                'element'   => 'target_team',
                                                'value'     => array( 'cat_team' ),
                                            ),
                            ), 
                             array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("Number team Member load", 'pikoworks_core'),
                                "param_name"  => "number",
                                "value"       => 8,                                
                                "description" => esc_html__('Enter number of Product if you use "-1" load all product load. or Enter specific as you want like as 7, 8,10...', 'pikoworks_core')
                            ),
                            array(
                                "type"       => "dropdown",
                                "heading"    => esc_html__("Order by", 'pikoworks_core'),
                                "param_name" => "orderby",
                                "value"      => array(
                                    esc_html__('None', 'pikoworks_core')     => 'none',
                                    esc_html__('ID', 'pikoworks_core')       => 'ID',
                                    esc_html__('Author', 'pikoworks_core')   => 'author',
                                    esc_html__('Name', 'pikoworks_core')     => 'name',
                                    esc_html__('Date', 'pikoworks_core')     => 'date',
                                    esc_html__('Modified', 'pikoworks_core') => 'modified',
                                    esc_html__('Rand', 'pikoworks_core')     => 'rand',
                                    ),
                                'std'         => 'date',
                                'dependency' => array(
                                                'element'   => 'target_team',
                                                'value'     => array( 'target_team' ),
                                            ),
                                "description" => esc_html__("Select how to sort retrieved posts.",'pikoworks_core')
                            ),
                            array(
                                "type"       => "dropdown",
                                "heading"    => esc_html__("Order", 'pikoworks_core'),
                                "param_name" => "order",
                                "value"      => array(
                                    esc_html__( 'Descending', 'pikoworks_core' ) => 'DESC',
                                    esc_html__( 'Ascending', 'pikoworks_core' )  => 'ASC'
                                    ),
                                'std'         => 'DESC',
                                'dependency' => array(
                                                'element'   => 'target_team',
                                                'value'     => array( 'target_team' ),
                                            ),
                                "description" => esc_html__("Designates the ascending or descending order.",'pikoworks_core')
                            ),                             
                             array(
                                    'type' => 'textfield',
                                    'param_name' => 'excerpt',
                                    'heading' => esc_html__('Excerpt word', 'pikoworks_core'),
                                    'description' => esc_html__('Default use Excerpt 25 words ', 'pikoworks_core'),
                                    'value' => '25',
                                    'admin_label' => false,
                                ),
                            
                            // Carousel
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                                ),
                                'std'         => 'false',
                                'heading'     => esc_html__( 'AutoPlay', 'pikoworks_core' ),
                                'param_name'  => 'autoplay',
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'dependency' => array(
                                                'element'   => 'is_slider',
                                                'value'     => array( 'yes' ),
                                            ),
                                'admin_label' => false
                                    ),
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                                ),
                                'std'         => 'false',
                                'heading'     => esc_html__( 'Navigation', 'pikoworks_core' ),
                                'param_name'  => 'navigation',
                                'description' => esc_html__( "Show buton 'next' and 'prev' buttons.", 'pikoworks_core' ),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'dependency' => array(
                                                'element'   => 'is_slider',
                                                'value'     => array( 'yes' ),
                                            ),
                                'admin_label' => false,
                            ),                           
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Nav Center', 'pikoworks_core' ) => 'owl-nav-show-inner',
                                    esc_html__( 'Outside Center', 'pikoworks_core' )  => 'owl-nav-show',
                                    esc_html__( 'Hover Center', 'pikoworks_core' )  => 'owl-nav-show-hover'
                                ),
                                'std'         => 'owl-nav-show-inner',
                                'heading'     => esc_html__( 'Next/Prev Button', 'pikoworks_core' ),
                                'param_name'  => 'navigation_btn',
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'dependency' => array(
                                            'element'   => 'navigation',
                                            'value'     => array( 'true' ),
                                        ),
                            ),
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                                ),
                                'std'         => 'false',
                                'heading'     => esc_html__( 'Bullets', 'pikoworks_core' ),
                                'param_name'  => 'dots',
                                'description' => esc_html__( "Show Carousel bullets bottom", 'pikoworks_core' ),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'dependency' => array(
                                                'element'   => 'is_slider',
                                                'value'     => array( 'yes' ),
                                            ),
                                'admin_label' => false,
                            ),
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                                ),
                                'std'         => 'false',
                                'heading'     => esc_html__( 'Loop', 'pikoworks_core' ),
                                'param_name'  => 'loop',
                                'description' => esc_html__( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'pikoworks_core' ),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'dependency' => array(
                                                'element'   => 'is_slider',
                                                'value'     => array( 'yes' ),
                                            ),
                                'admin_label' => false,
                                ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("Slide Speed", 'pikoworks_core'),
                                "param_name"  => "slidespeed",
                                "value"       => "200",
                                "suffix"      => esc_html__("milliseconds", 'pikoworks_core'),
                                "description" => esc_html__('Slide speed in milliseconds', 'pikoworks_core'),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'dependency' => array(
                                                'element'   => 'is_slider',
                                                'value'     => array( 'yes' ),
                                            ),
                                'admin_label' => false,
                                    ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("Margin", 'pikoworks_core'),
                                "param_name"  => "margin",
                                "value"       => "30",
                                "suffix"      => esc_html__("px", 'pikoworks_core'),
                                "description" => esc_html__('Distance( or space) between 2 item', 'pikoworks_core'),
                                'dependency' => array(
                                                'element'   => 'is_slider',
                                                'value'     => array( 'yes' ),
                                            ),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' )
                                    ),

                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 1,
                                    esc_html__( 'No', 'pikoworks_core' )  => 0
                                ),
                                'std'         => 1,
                                'heading'     => esc_html__( 'Single/Multiple Carousel', 'pikoworks_core' ),
                                'param_name'  => 'use_responsive',
                                'description' => esc_html__( "Yes is Multiple or No is Single Item carosuel", 'pikoworks_core' ),
                                'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
                                'dependency' => array(
                                                'element'   => 'is_slider',
                                                'value'     => array( 'yes' ),
                                            ),
                                'admin_label' => false,            
                            ),
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( '--- No Animation ---', 'pikoworks_core' ) => '',
                                    esc_html__( 'fadeIn', 'pikoworks_core' )  => 'fadeiN',
                                    esc_html__( 'slideInDown', 'pikoworks_core' )  => 'slidedowN',
                                    esc_html__( 'bounceInUp', 'pikoworks_core' )  => 'bounceRighT',
                                    esc_html__( 'zoomIn', 'pikoworks_core' )  => 'zoomiN',
                                    esc_html__( 'zoomIn two', 'pikoworks_core' )  => 'zoomin_2',
                                    esc_html__( 'zoomInDown', 'pikoworks_core' )  => 'zoomInDowN',
                                    esc_html__( 'fadeInLeft', 'pikoworks_core' )  => 'fadeInLefT',
                                    esc_html__( 'fadeInUp', 'pikoworks_core' )  => 'fadeInuP',
                                    esc_html__( 'slideInUp', 'pikoworks_core' )  => 'slideInuP',
                                ),            
                                'heading'     => esc_html__( 'Select Animation', 'pikoworks_core' ),
                                'param_name'  => 'sanam',
                                'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
                                'dependency' => array(
                                            'element'   => 'use_responsive',
                                            'value'     => array( '0' ),
                                        ),
                            ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("The items on large Device (Screen resolution of device >= 1200px )", 'pikoworks_core'),
                                "param_name"  => "items_large_device",
                                "value"       => "4",
                                "suffix"      => esc_html__("item", 'pikoworks_core'),
                                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
                                'dependency' => array(
                                            'element'   => 'use_responsive',
                                            'value'     => array( '1' ),
                                        ),
                                'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
                              ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("The items on desktop (Screen resolution of device >= 992px < 1199px )", 'pikoworks_core'),
                                "param_name"  => "items_desktop",
                                "value"       => "3",
                                "suffix"      => esc_html__("item", 'pikoworks_core'),
                                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
                                'dependency' => array(
                                            'element'   => 'use_responsive',
                                            'value'     => array( '1' ),
                                        ),
                                'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
                              ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("The items on tablet (Screen resolution of device >=768px and < 992px )", 'pikoworks_core'),
                                "param_name"  => "items_tablet",
                                "value"       => "2",
                                "suffix"      => esc_html__("item", 'pikoworks_core'),
                                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
                                'dependency' => array(
                                            'element'   => 'use_responsive',
                                            'value'     => array( '1' ),
                                        ),
                                'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
                                    ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("The items on mobile (Screen resolution of device < 768px)", 'pikoworks_core'),
                                "param_name"  => "items_mobile",
                                "value"       => "1",
                                "suffix"      => esc_html__("item", 'pikoworks_core'),
                                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
                                'dependency' => array(
                                            'element'   => 'use_responsive',
                                            'value'     => array( '1' ),
                                        ),
                               'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
                             ),                            
                            $add_el_class,
                        )
                    ));
                }
                
                if (!isset($cpt_disable) || (array_key_exists('testimonial', $cpt_disable) && ($cpt_disable['testimonial'] == '0' || $cpt_disable['testimonial'] == ''))) {
                    $testimonial_cat = array();
                    $testimonial_categories = get_terms('testimonial_category', array('hide_empty' => 0, 'orderby' => 'ASC'));
                    if (is_array($testimonial_categories)) {
                        foreach ($testimonial_categories as $cat) {
                            $testimonial_cat[$cat->name] = $cat->slug;
                        }
                    }
                    vc_map(array(
                        'name' => esc_html__('Testimonial', 'pikoworks_core'),
                        'description' => esc_html__('Display Differnt style', 'pikoworks_core'),
                        'base' => 'piko_testimonial',
                        'icon' => get_template_directory_uri() . '/assets/images/logo/vc-icon.png',
                        'category' => 'Pikoworks',
                        'params' => array(                                                      
                            array(
                                'type' => 'dropdown',
                                'param_name' => 'target_team',
                                'value' => array(
                                    esc_html__('All Team Member', 'pikoworks_core') => 'target_team',
                                    esc_html__('Multi Category Team', 'pikoworks_core') => 'cat_team',
                                ),                
                                'heading' => esc_html__('Target Testimonial', 'pikoworks_core'),                               
                                'admin_label' => true,
                            ),                           
                            array(
                                'type' => 'pikoworks_taxonomy',
                                'heading' => esc_html__('Category', 'pikoworks_core'),
                                'param_name' => 'category',
                                'multiple'    => true,
                                'taxonomy' => 'testimonial_category',
                                'dependency' => array(
                                                'element'   => 'target_team',
                                                'value'     => array( 'cat_team' ),
                                            ),
                            ), 
                            array(
                                'type'          => 'dropdown',
                                'heading'       => esc_html__( 'select style', 'pikoworks_core' ),
                                'param_name'    => 'type',
                                'value' => array(
                                    esc_html__('style 1', 'pikoworks_core') => 'style1',
                                    esc_html__('style 2', 'pikoworks_core') => 'style2',
                                    esc_html__('style 3', 'pikoworks_core') => 'style3',
                                ),
                                'std'           => 'style1',
                                'admin_label' => true,  
                            ), 
                            array(
                                'type' => 'checkbox',
                                'heading' => esc_html__('Enable client image', 'pikoworks_core'),
                                'param_name' => 'is_client_image',
                                'value' => array(esc_html__('Yes, please', 'pikoworks_core') => 'yes'),
                                "description" => esc_html__('If not check font comment icon show', 'pikoworks_core'),
                                'dependency' => array(
                                                'element'   => 'type',
                                                'value'     => array( 'style2' ),
                                            ),                                
                            ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("Number testimonial load", 'pikoworks_core'),
                                "param_name"  => "number",
                                "value"       => 7,
                                "description" => esc_html__('Enter number of estimonial if you use "-1" load all product load. or Enter specific as you want like as 7, 8,10...', 'pikoworks_core')
                            ),    
                            array(
                                "type"       => "dropdown",
                                "heading"    => esc_html__("Order by", 'pikoworks_core'),
                                "param_name" => "orderby",
                                "value"      => array(
                                    esc_html__('None', 'pikoworks_core')     => 'none',
                                    esc_html__('ID', 'pikoworks_core')       => 'ID',
                                    esc_html__('Author', 'pikoworks_core')   => 'author',
                                    esc_html__('Name', 'pikoworks_core')     => 'name',
                                    esc_html__('Date', 'pikoworks_core')     => 'date',
                                    esc_html__('Modified', 'pikoworks_core') => 'modified',
                                    esc_html__('Rand', 'pikoworks_core')     => 'rand',
                                    ),
                                'std'         => 'date',
                                "description" => esc_html__("Select how to sort retrieved posts.",'pikoworks_core')
                            ),
                            array(
                                "type"       => "dropdown",
                                "heading"    => esc_html__("Order", 'pikoworks_core'),
                                "param_name" => "order",
                                "value"      => array(
                                    esc_html__( 'Descending', 'pikoworks_core' ) => 'DESC',
                                    esc_html__( 'Ascending', 'pikoworks_core' )  => 'ASC'
                                    ),
                                'std'         => 'DESC',
                                "description" => esc_html__("Designates the ascending or descending order.",'pikoworks_core')
                            ),
                             array(
                                    'type' => 'textfield',
                                    'param_name' => 'excerpt',
                                    'heading' => esc_html__('Excerpt word', 'pikoworks_core'),
                                    'description' => esc_html__('Default use Excerpt 55 words ', 'pikoworks_core'),
                                    'value' => '55'
                                ),
                            array(
                                    'type'          => 'dropdown',
                                    'heading'       => esc_html__( 'Show Company name with link', 'pikoworks_core' ),
                                    'param_name'    => 'show_company_link',
                                    'admin_label' => true,
                                    'value' => array(
                                        esc_html__( 'Yes', 'pikoworks_core' ) => 'yes',
                                        esc_html__( 'No', 'pikoworks_core' ) => 'no',	    
                                    ),
                                    'std'           => 'no',
                                ),
                             // Carousel
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                                ),
                                'std'         => 'false',
                                'heading'     => esc_html__( 'AutoPlay', 'pikoworks_core' ),
                                'param_name'  => 'autoplay',
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'admin_label' => false
                                    ),
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                                ),
                                'std'         => 'false',
                                'heading'     => esc_html__( 'Navigation', 'pikoworks_core' ),
                                'param_name'  => 'navigation',
                                'description' => esc_html__( "Show buton 'next' and 'prev' buttons.", 'pikoworks_core' ),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'admin_label' => false,
                            ),
                             array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Nav Center', 'pikoworks_core' ) => 'owl-nav-show-inner',
                                    esc_html__( 'Outside Center', 'pikoworks_core' )  => 'owl-nav-show',
                                    esc_html__( 'Hover Center', 'pikoworks_core' )  => 'owl-nav-show-hover'
                                ),
                                'std'         => 'owl-nav-show-inner',
                                'heading'     => esc_html__( 'Next/Prev Button', 'pikoworks_core' ),
                                'param_name'  => 'navigation_btn',
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'dependency' => array(
                                            'element'   => 'navigation',
                                            'value'     => array( 'true' ),
                                        ),
                            ),
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                                ),
                                'std'         => 'false',
                                'heading'     => esc_html__( 'Bullets', 'pikoworks_core' ),
                                'param_name'  => 'dots',
                                'description' => esc_html__( "Show Carousel bullets bottom", 'pikoworks_core' ),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'admin_label' => false,
                            ),
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                                ),
                                'std'         => 'false',
                                'heading'     => esc_html__( 'Loop', 'pikoworks_core' ),
                                'param_name'  => 'loop',
                                'description' => esc_html__( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'pikoworks_core' ),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'admin_label' => false,
                                ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("Slide Speed", 'pikoworks_core'),
                                "param_name"  => "slidespeed",
                                "value"       => "200",
                                "suffix"      => esc_html__("milliseconds", 'pikoworks_core'),
                                "description" => esc_html__('Slide speed in milliseconds', 'pikoworks_core'),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'admin_label' => false,
                                    ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("Margin", 'pikoworks_core'),
                                "param_name"  => "margin",
                                "value"       => "30",
                                "suffix"      => esc_html__("px", 'pikoworks_core'),
                                "description" => esc_html__('Distance( or space) between 2 item', 'pikoworks_core'),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' )
                                    ),

                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 1,
                                    esc_html__( 'No', 'pikoworks_core' )  => 0
                                ),
                                'std'         => 1,
                                'heading'     => esc_html__( 'Single/Multiple Carousel', 'pikoworks_core' ),
                                'param_name'  => 'use_responsive',
                                'description' => esc_html__( "Yes is Multiple or No is Single Item carosuel", 'pikoworks_core' ),
                                'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
                                'admin_label' => false,            
                            ),
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( '--- No Animation ---', 'pikoworks_core' ) => '',
                                    esc_html__( 'fadeIn', 'pikoworks_core' )  => 'fadeiN',
                                    esc_html__( 'slideInDown', 'pikoworks_core' )  => 'slidedowN',
                                    esc_html__( 'bounceInUp', 'pikoworks_core' )  => 'bounceRighT',
                                    esc_html__( 'zoomIn', 'pikoworks_core' )  => 'zoomiN',
                                    esc_html__( 'zoomIn two', 'pikoworks_core' )  => 'zoomin_2',
                                    esc_html__( 'zoomInDown', 'pikoworks_core' )  => 'zoomInDowN',
                                    esc_html__( 'fadeInLeft', 'pikoworks_core' )  => 'fadeInLefT',
                                    esc_html__( 'fadeInUp', 'pikoworks_core' )  => 'fadeInuP',
                                    esc_html__( 'slideInUp', 'pikoworks_core' )  => 'slideInuP',
                                ),            
                                'heading'     => esc_html__( 'Select Animation', 'pikoworks_core' ),
                                'param_name'  => 'sanam',
                                'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
                                'dependency' => array(
                                            'element'   => 'use_responsive',
                                            'value'     => array( '0' ),
                                        ),
                            ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("The items on large Device (Screen resolution of device >= 1200px )", 'pikoworks_core'),
                                "param_name"  => "items_large_device",
                                "value"       => "4",
                                "suffix"      => esc_html__("item", 'pikoworks_core'),
                                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
                                'dependency' => array(
                                            'element'   => 'use_responsive',
                                            'value'     => array( '1' ),
                                        ),
                                'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
                              ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("The items on desktop (Screen resolution of device >= 992px < 1199px )", 'pikoworks_core'),
                                "param_name"  => "items_desktop",
                                "value"       => "3",
                                "suffix"      => esc_html__("item", 'pikoworks_core'),
                                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
                                'dependency' => array(
                                            'element'   => 'use_responsive',
                                            'value'     => array( '1' ),
                                        ),
                                'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
                              ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("The items on tablet (Screen resolution of device >=768px and < 992px )", 'pikoworks_core'),
                                "param_name"  => "items_tablet",
                                "value"       => "2",
                                "suffix"      => esc_html__("item", 'pikoworks_core'),
                                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
                                'dependency' => array(
                                            'element'   => 'use_responsive',
                                            'value'     => array( '1' ),
                                        ),
                                'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
                                    ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("The items on mobile (Screen resolution of device < 768px)", 'pikoworks_core'),
                                "param_name"  => "items_mobile",
                                "value"       => "1",
                                "suffix"      => esc_html__("item", 'pikoworks_core'),
                                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),
                                'dependency' => array(
                                            'element'   => 'use_responsive',
                                            'value'     => array( '1' ),
                                        ),
                               'group'       => esc_html__( 'Multi Columns', 'pikoworks_core' ),
                             ),                           
                             array(
                                'type'           => 'css_editor',
                                'heading'        => esc_html__( 'Css', 'pikoworks_core' ),
                                'param_name'     => 'css',
                                'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'pikoworks_core' ),
                                'group'          => esc_html__( 'Design options', 'pikoworks_core' )
                            ),
                            $add_el_class,
                        )
                    ));
                }
                
                
                
                if (!isset($cpt_disable) || (array_key_exists('service', $cpt_disable) && ($cpt_disable['service'] == '0' || $cpt_disable['service'] == ''))) {
                    $service_categories = get_terms(PIKOWORKS_SERVICE_CATEGORY_TAXONOMY, array('hide_empty' => 0, 'orderby' => 'ASC'));
                    $service_cat = array();
                    if (is_array($service_categories)) {
                        foreach ($service_categories as $cat) {
                            $service_cat[$cat->name] = $cat->slug;
                        }
                    }
                    

                    $args = array(
                        'posts_per_page' => -1,
                        'post_type' => PIKOWORKS_SERVICE_POST_TYPE,
                        'post_status' => 'publish');
                    $list_service = array();
                    $post_array = get_posts($args);
                    foreach ($post_array as $post) : setup_postdata($post);
                    $list_service[$post->post_title] = $post->ID;
                    endforeach;
                    wp_reset_postdata();
                    vc_map(array(
                        'name' => esc_html__('Service', 'pikoworks_core'),
                        'base' => 'piko_service',
                        'icon' => get_template_directory_uri() . '/assets/images/logo/vc-icon.png',
                        'description' => esc_html__( 'Display Different service layout', 'pikoworks_core'),
                        'category' => 'Pikoworks',
                        'params' => array(
                             array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Different Layout style', 'pikoworks_core'),
                                'param_name' => 'overlay_style',
                                'admin_label' => true,
                                'value' => array(
                                    esc_html__('grid', 'pikoworks_core') => 'style-1',
                                    esc_html__('list', 'pikoworks_core') => 'style-2',
                                ),                              

                            ), 
                            array(
                                'type' => 'checkbox',
                                'heading'       => 'Overlay Image Transparent color',
                                'param_name' => 'image_overlay',
                                'value' => array(esc_html__('Yes', 'pikoworks_core') => 'overlay-style-2'),
                                'dependency' => array('element'   => 'overlay_style', 'value' => 'style-1'),
                            ),
                             array(
                                    'type' => 'pikoworks_number',
                                    'param_name' => 'excerpt',
                                    'heading' => esc_html__('Excerpt word', 'pikoworks_core'),
                                    'description' => esc_html__('Default use Content Excerpt 25 words ', 'pikoworks_core'),
                                    'value' => '25',
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Target Source', 'pikoworks_core'),
                                'param_name' => 'data_source',
                                'admin_label' => true,
                                'value' => array(
                                    esc_html__('From Category', 'pikoworks_core') => '',
                                    esc_html__('From Title(IDs)', 'pikoworks_core') => 'list_id')
                            ),

                            array(
                                'type' => 'pikoworks_taxonomy',
                                'heading' => esc_html__('Service Category', 'pikoworks_core'),
                                'param_name' => 'category',
                                'admin_label' => true,
                                'taxonomy' => PIKOWORKS_SERVICE_CATEGORY_TAXONOMY,
                                'multiple'    => true,
                                'parent'    => 0,
                                'description' => esc_html__('Select multiple specific Category or all Category', 'pikoworks_core'),
                                'dependency' => Array('element' => 'data_source', 'value' => array(''))
                            ),
                            array(
                                'type' => 'pikoworks_title',
                                'heading' => esc_html__('Select Service', 'pikoworks_core'),
                                'param_name' => 'service_ids',
                                'options' => $list_service,
                                'multiple'    => true,
                                'description' => esc_html__('Select multiple specific title or all Title IDs', 'pikoworks_core'),
                                'dependency' => Array('element' => 'data_source', 'value' => array('list_id'))
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Enable Carousel', 'pikoworks_core'),
                                'param_name' => 'show_pagging',
                                'value' => array('No' => '1', esc_html__('Yes', 'pikoworks_core') => '2'),
                                'admin_label' => true,
                                'dependency' => Array('element' => 'overlay_style', 'value' => array('style-1'))
                            ), 
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Number of column', 'pikoworks_core'),
                                'param_name' => 'column',
                                'value' => array('2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6'),
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('1'))
                            ),
                                                       
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Order Post Date By', 'pikoworks_core'),
                                'param_name' => 'order',
                                'value' => array(esc_html__('Descending', 'pikoworks_core') => 'DESC', esc_html__('Ascending', 'pikoworks_core') => 'ASC')
                            ),
                            array(
                                'type' => 'dropdown',
                                'heading' => esc_html__('Padding/ Gap each item', 'pikoworks_core'),
                                'param_name' => 'padding',
                                'admin_label' => true,
                                'value' => array(esc_html__('No padding', 'pikoworks_core') => '', '05 px' => 'col-padding-05', '10 px' => 'col-padding-10', '15 px' => 'col-padding-15', '20 px' => 'col-padding-20', '40 px' => 'col-padding-40'),
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('1'))
                            ),
                            array(
                                'type' => 'checkbox',
                                'heading' => esc_html__('Title and content center', 'pikoworks_core'),
                                'param_name' => 'is_content_center',
                                'value' => array(esc_html__('Yes, please', 'pikoworks_core') => 'content-center'),
                            ),
                            $add_image_size,
                            
                             array(
                                'type'          => 'dropdown',
                                'heading'       => esc_html__( 'Show Read More Button', 'pikoworks_core' ),
                                'param_name'    => 'show_read_more_btn',
                                'description'   => esc_html__( 'Read More button text edit go to: Themeoption -> custom post-> service', 'pikoworks_core' ),
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 'yes',
                                    esc_html__( 'No', 'pikoworks_core' ) => 'no',	    
                                ),
                                'std'           => 'yes',                                
                            ),                                                        
                             // Carousel                           
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                                ),
                                'std'         => 'false',
                                'heading'     => esc_html__( 'AutoPlay', 'pikoworks_core' ),
                                'param_name'  => 'autoplay',
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'admin_label' => false,
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                                ),
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                                ),
                                'std'         => 'false',
                                'heading'     => esc_html__( 'Navigation', 'pikoworks_core' ),
                                'param_name'  => 'navigation',
                                'description' => esc_html__( "Show buton 'next' and 'prev' buttons.", 'pikoworks_core' ),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'admin_label' => false,
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                            ),
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Nav Center', 'pikoworks_core' ) => 'owl-nav-show-inner',
                                    esc_html__( 'Outside Center', 'pikoworks_core' )  => 'owl-nav-show',
                                    esc_html__( 'Hover Center', 'pikoworks_core' )  => 'owl-nav-show-hover'
                                ),
                                'std'         => 'owl-nav-show-inner',
                                'heading'     => esc_html__( 'Next/Prev Button', 'pikoworks_core' ),
                                'param_name'  => 'navigation_btn',
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'dependency' => array(
                                            'element'   => 'navigation',
                                            'value'     => array( 'true' ),
                                        ),
                            ),
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                                ),
                                'std'         => 'false',
                                'heading'     => esc_html__( 'Bullets', 'pikoworks_core' ),
                                'param_name'  => 'dots',
                                'description' => esc_html__( "Show Carousel bullets bottom", 'pikoworks_core' ),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'admin_label' => false,
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                            ),
                            array(
                                'type'  => 'dropdown',
                                'value' => array(
                                    esc_html__( 'Yes', 'pikoworks_core' ) => 'true',
                                    esc_html__( 'No', 'pikoworks_core' )  => 'false'
                                ),
                                'std'         => 'false',
                                'heading'     => esc_html__( 'Loop', 'pikoworks_core' ),
                                'param_name'  => 'loop',
                                'description' => esc_html__( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'pikoworks_core' ),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'admin_label' => false,
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                                ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("Slide Speed", 'pikoworks_core'),
                                "param_name"  => "slidespeed",
                                "value"       => "200",
                                "suffix"      => esc_html__("milliseconds", 'pikoworks_core'),
                                "description" => esc_html__('Slide speed in milliseconds', 'pikoworks_core'),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'admin_label' => false,
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                                    ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("Margin", 'pikoworks_core'),
                                "param_name"  => "margin",
                                "value"       => "30",
                                "suffix"      => esc_html__("px", 'pikoworks_core'),
                                "description" => esc_html__('Distance( or space) between 2 item', 'pikoworks_core'),
                                'group'       => esc_html__( 'Carousel settings', 'pikoworks_core' ),
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                            ),                            
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("The items on large Device (Screen resolution of device >= 1200px )", 'pikoworks_core'),
                                "param_name"  => "items_large_device",
                                "value"       => "4",
                                "suffix"      => esc_html__("item", 'pikoworks_core'),
                                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),                                
                                'group'       => esc_html__( 'Responsive', 'pikoworks_core' ),
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                              ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("The items on desktop (Screen resolution of device >= 992px < 1199px )", 'pikoworks_core'),
                                "param_name"  => "items_desktop",
                                "value"       => "3",
                                "suffix"      => esc_html__("item", 'pikoworks_core'),
                                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),                                
                                'group'       => esc_html__( 'Responsive', 'pikoworks_core' ),
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                              ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("The items on tablet (Screen resolution of device >=768px and < 992px )", 'pikoworks_core'),
                                "param_name"  => "items_tablet",
                                "value"       => "2",
                                "suffix"      => esc_html__("item", 'pikoworks_core'),
                                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),                                
                                'group'       => esc_html__( 'Responsive', 'pikoworks_core' ),
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                                    ),
                            array(
                                "type"        => "pikoworks_number",
                                "heading"     => esc_html__("The items on mobile (Screen resolution of device < 768px)", 'pikoworks_core'),
                                "param_name"  => "items_mobile",
                                "value"       => "1",
                                "suffix"      => esc_html__("item", 'pikoworks_core'),
                                "description" => esc_html__('The number of item on desktop', 'pikoworks_core'),                                
                               'group'       => esc_html__( 'Responsive', 'pikoworks_core' ),
                                'dependency' => Array('element' => 'show_pagging', 'value' => array('2',))
                             ),

                            $add_el_class,                            
                        )
                    ));
                }
               
            }
        }

    }

    if (!function_exists('piko_init_custom_post_shortcodes')) {
        function piko_init_custom_post_shortcodes()
        {
            return piko_custom_post_shortcodes::init();
        }

        piko_init_custom_post_shortcodes();
    }
}