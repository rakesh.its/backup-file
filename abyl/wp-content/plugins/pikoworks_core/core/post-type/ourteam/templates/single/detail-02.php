<?php
/**
 * @author  themepiko
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="clearfix margin-b50">
        <div class="col-sm-6 col-md-3">
            <div class="team-img">
                <?php            
                 if(count($imgThumbs)>0) :?>
                    <figure>
                        <?php the_post_thumbnail( 'full' ); ?>
                    </figure>
                <?php endif; ?>
                <div class="team-name">
                    <h4><?php echo get_the_title() ?></h4>            
                    <span class="position"><?php echo esc_html($job) ?></span>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-5">
            <?php 
                $address = get_post_meta(get_the_ID(), 'piko_ourteam_address', TRUE);
                if(isset($address) && !empty($address)): 
            ?>
            <div class="table-responsive">
                <table class="table table-striped">
                    <tbody> 
                <?php                
                foreach ($address['ourteam'] as $col) {
                    $aboutTitle = isset($col['aboutTitle']) ? $col['aboutTitle'] : '';
                    $aboutDetails = isset($col['aboutDetails']) ? $col['aboutDetails'] : '';
                    ?>                
                    <tr> <th scope="row"><?php echo esc_attr($aboutTitle) ?></th> <td colspan="2"><?php echo esc_attr($aboutDetails) ?></td></tr> 
                    <?php
                }
                ?>
                    </tbody> 
                </table>
            </div>
            <?php endif; ?>
            
             <?php 
             //social icon
                $meta = get_post_meta(get_the_ID(), 'piko_ourteam_social', TRUE);
                if(isset($meta) && !empty($meta)): 
            ?>
            <ul>
                <?php                
                foreach ($meta['ourteam'] as $col) {
                    $socialName = isset($col['socialName']) ? $col['socialName'] : '';
                    $socialLink = isset($col['socialLink']) ? $col['socialLink'] : '';
                    $socialIcon = isset($col['socialIcon']) ? $col['socialIcon'] : '';
                    ?>
                <li><a href="<?php echo esc_url($socialLink) ?>" target="_blank" 
                           title="<?php echo esc_attr($socialName) ?>"><i
                                class="<?php echo esc_attr($socialIcon) ?>" aria-hidden="true"></i></a>
                    </li>
                    <?php
                }
                ?>
            </ul>
            <?php endif; ?>            
           
        </div>
        <div class="col-md-4">
              <?php 
                $skill = get_post_meta(get_the_ID(), 'pikoworks_ourteam_skill', TRUE);
                if(isset($skill) && !empty($skill)): 
            ?>
            <div class="progress-wrap">
                <h4><?php echo esc_html($skill_title) ?></h4>
                <?php                
                foreach ($skill['ourteam'] as $col) {
                    $skillTitle = isset($col['skillTitle']) ? $col['skillTitle'] : '';
                    $skillValue = isset($col['skillValue']) ? $col['skillValue'] : '';
                    ?>
                    <div class="progress-container">
                        <h4 class="progress-title"><?php echo esc_attr($skillTitle) ?></h4>
                        <div class="progress progress-sm">
                            <div class="progress-bar progress-animate" role="progressbar" data-width="<?php echo esc_attr($skillValue) ?>" aria-valuenow="<?php echo esc_attr($skillValue) ?>" aria-valuemin="0" aria-valuemax="100">
                                <span class="progress-val"><?php echo esc_attr($skillValue) ?>%</span>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?> 
            </div>
            <?php endif; ?>           
            
        </div>
    </div>
    <div class="clearfix">
        <div class="team-content col-xs-12">
            <?php 
              if ( '' !== get_post()->post_content ) {
                    the_content();   
                } else { 
                       the_excerpt();
                }                                      
              ?>
        </div>
    </div>
</article>