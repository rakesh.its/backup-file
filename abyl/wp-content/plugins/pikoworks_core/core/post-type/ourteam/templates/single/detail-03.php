<?php
/**
 * @author  themepiko
 */

$primary_class = pikoworks_primary_team_class();
$secondary_class = pikoworks_secondary_team_class();
$sidebar_position = isset( $GLOBALS['pikoworks']['team_single_sidebar_pos'] ) ? trim( $GLOBALS['pikoworks']['team_single_sidebar_pos'] ) : 'fullwidth';
 $chosen_sidebar = $GLOBALS['pikoworks']['team_single_sidebar'];
?>
<div class="clearfix">
    <article id="post-<?php the_ID(); ?>" class="<?php echo esc_attr('ourteam-wrap ' .$primary_class); ?>">    
        <?php 
          if ( '' !== get_post()->post_content ) {
                the_content();   
            } else { 
                   the_excerpt();
            }                                      
          ?>
</article>
<?php if ( $sidebar_position != 'fullwidth' ): ?>
	<aside id="secondary" class="widget-area <?php echo esc_attr( $secondary_class ); ?>" role="complementary">
		<?php dynamic_sidebar( $chosen_sidebar ); ?>
	</aside><!-- .sidebar .widget-area -->
<?php endif; ?>
</div>