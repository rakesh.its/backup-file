<?php
/**
 * The template part for displaying content
 */
?> 
<section class="ourteam <?php echo esc_attr($class_col); ?>">
        <div class="<?php echo esc_attr($class_warp) ?> avatar image-overlay-wrap">
            <figure class="piko-overflow-hidden">
                <?php echo wp_kses_post($img['thumbnail']); ?>        
                <div class="icon-overlay">
                                   
                    <?php 
                        $meta = get_post_meta(get_the_ID(), 'piko_ourteam_social', TRUE);
                        if(isset($meta) && !empty($meta)): 
                            foreach ($meta['ourteam'] as $col) :
                                $socialName = isset($col['socialName']) ? $col['socialName'] : '';
                                $socialLink = isset($col['socialLink']) ? $col['socialLink'] : '';
                                $socialIcon = isset($col['socialIcon']) ? $col['socialIcon'] : '';
                                ?>
                                <a href="<?php echo esc_url($socialLink) ?>" target="_blank" 
                                           title="<?php echo esc_attr($socialName) ?>"><i
                                                class="<?php echo esc_attr($socialIcon) ?>" aria-hidden="true"></i></a>
                                
                                <?php
                                endforeach;
                        endif; ?>                                        
                    
                </div>
            </figure>
        </div>        
        <div class="<?php echo esc_attr($class_warp) ?> wrap text-center">
            <header>
                <h5><a href="<?php echo esc_url( get_permalink()); ?>" ><?php echo get_the_title() ?></a></h5>
            </header>
            <span class="position"><?php echo esc_html($job) ?></span>
            <?php           
            if ($show_excerpt == 'yes') : ?>
              <?php 
                  if ( ! has_excerpt() ) {
                    echo '<p>'. wp_trim_words( get_the_content(), esc_attr($excerpt)) . '</p>';
                    } else { 
                          echo '<p>'. wp_trim_words( get_the_excerpt(), esc_attr($excerpt)) . '</p>';
                    }                                      
                  ?>
            <?php endif; ?>
        </div>
</section>



