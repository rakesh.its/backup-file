<?php
/**
 * Custom post team
 * @author themepiko
 */
if( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
if (!defined('PIKO_OURTEAM_POST_TYPE'))
    define('PIKO_OURTEAM_POST_TYPE', 'ourteam');
if (!defined('PIKO_OURTEAM_CATEGORY_TAXONOMY'))
    define('PIKO_OURTEAM_CATEGORY_TAXONOMY', 'ourteam-category');

global $pikoworks;
$ourteam_metabox_social = new WPAlchemy_MetaBox(
        array(
            'id' => 'piko_ourteam_social',
            'title' => esc_html__('Team Archive/Single Social Settings', 'pikoworks_core'),
            'template' => plugin_dir_path(__FILE__) . '/metaboxes/custom-field.php',
            'types' => array('ourteam'),
            'autosave' => TRUE,
            'priority' => 'high',
            'context' => 'normal'
        )        
    );

$ourteam_metabox_address = new WPAlchemy_MetaBox(        
        array(
            'id' => 'piko_ourteam_address',
            'title' => esc_html__('Team Single About/Address Settings', 'pikoworks_core'),
            'template' => plugin_dir_path(__FILE__) . '/metaboxes/custom-address.php',
            'types' => array('ourteam'),
            'autosave' => TRUE,
            'priority' => 'low',
            'context' => 'normal'
        )
    );
$ourteam_metabox_experience = new WPAlchemy_MetaBox(        
        array(
            'id' => 'pikoworks_ourteam_experience',
            'title' => esc_html__('Team Single Experience Settings', 'pikoworks_core'),
            'template' => plugin_dir_path(__FILE__) . '/metaboxes/custom-experience.php',
            'types' => array('ourteam'),
            'autosave' => TRUE,
            'priority' => 'low',
            'context' => 'normal'
        )
    );
$ourteam_metabox_skill = new WPAlchemy_MetaBox(        
        array(
            'id' => 'pikoworks_ourteam_skill',
            'title' => esc_html__('Team Single Skill Settings', 'pikoworks_core'),
            'template' => plugin_dir_path(__FILE__) . '/metaboxes/custom-skill.php',
            'types' => array('ourteam'),
            'autosave' => TRUE,
            'priority' => 'low',
            'context' => 'normal'
        )
    );



if (!class_exists('piko_Shortcode_Ourteam')) {
    class piko_Shortcode_Ourteam
    {
        function __construct()
        {
            
            add_action('init', array($this, 'register_post_types'), 5);
            add_action('init', array($this, 'register_taxonomies'), 6);
            add_shortcode('piko_ourteam', array($this, 'ourteam_shortcode'));
            add_filter('rwmb_meta_boxes', array($this, 'register_meta_boxes'));
            add_filter('single_template', array($this, 'get_ourteam_single_template'));
            if (is_admin()) {
                add_filter('manage_edit-ourteam_columns', array($this, 'add_columns'));
                add_action('manage_ourteam_posts_custom_column', array($this, 'set_columns_value'), 10, 2);
                add_action('admin_menu', array($this, 'addMenuChangeSlug'));
            }
        }
        function register_post_types()
        {
            $post_type = PIKO_OURTEAM_POST_TYPE;

            if (post_type_exists($post_type)) {
                return;
            }
            
            $post_type_slug = get_option('piko-' . $post_type . '-config');
            if (!isset($post_type_slug) || !is_array($post_type_slug)) {
                $slug = 'ourteam';
                $name = $singular_name = 'Our Team';
            } else {
                $slug = $post_type_slug['slug'];
                $name = $post_type_slug['name'];
                $singular_name = $post_type_slug['singular_name'];
            }

            register_post_type($post_type,
                array(
                    'label' => esc_html__('Our Team', 'pikoworks_core'),
                    'description' => esc_html__('Our Team Description', 'pikoworks_core'),
                    'labels' => array(
                        'name' => esc_attr($name),
                        'singular_name' => esc_attr($singular_name),
                        'menu_name' => esc_html__($name, 'pikoworks_core'),
                        'parent_item_colon' => esc_html__('Parent Item:', 'pikoworks_core'),
                        'all_items' => esc_html__(sprintf('All %s', $name), 'pikoworks_core'),
                        'view_item' => esc_html__('View Item', 'pikoworks_core'),
                        'add_new_item' => esc_html__(sprintf('Add New  %s', $name), 'pikoworks_core'),
                        'add_new' => esc_html__('Add New', 'pikoworks_core'),
                        'edit_item' => esc_html__('Edit Item', 'pikoworks_core'),
                        'update_item' => esc_html__('Update Item', 'pikoworks_core'),
                        'search_items' => esc_html__('Search Item', 'pikoworks_core'),
                        'not_found' => esc_html__('Not found', 'pikoworks_core'),
                        'not_found_in_trash' => esc_html__('Not found in Trash', 'pikoworks_core'),
                    ),
                    'supports' => array('title', 'excerpt', 'thumbnail', 'editor'),
                    'public' => true,
                    'show_ui' => true,
                    '_builtin' => false,
                    'has_archive' => true,
                    'rewrite' => array('slug' => $slug, 'with_front' => true),
                    'menu_icon' => 'dashicons-groups',
                )
            );
            flush_rewrite_rules();
        }

        function register_taxonomies()
        {
            if (taxonomy_exists(PIKO_OURTEAM_CATEGORY_TAXONOMY)) {
                return;
            }

            $post_type = PIKO_OURTEAM_POST_TYPE;
            $taxonomy_slug = PIKO_OURTEAM_CATEGORY_TAXONOMY;
            $taxonomy_name = 'Our Team Categories';

            $post_type_slug = get_option('piko-' . $post_type . '-config');
            if (isset($post_type_slug) && is_array($post_type_slug) &&
                array_key_exists('taxonomy_slug', $post_type_slug) && $post_type_slug['taxonomy_slug'] != ''
            ) {
                $taxonomy_slug = $post_type_slug['taxonomy_slug'];
                $taxonomy_name = $post_type_slug['taxonomy_name'];
            }
            register_taxonomy('ourteam_category', 'ourteam',
                array('hierarchical' => true,
                    'label' => $taxonomy_name,
                    'query_var' => true,
                    'rewrite' => array('slug' => $taxonomy_slug))
            );
            flush_rewrite_rules();
        }      

        function addMenuChangeSlug()
        {
            add_submenu_page('edit.php?post_type=ourteam', 'Setting', 'Settings', 'edit_posts', wp_basename(__FILE__), array($this, 'initPageSettings'));
        }

        function initPageSettings()
        {
            $template_path = ABSPATH . 'wp-content/plugins/pikoworks_core/core/post-type/posttype-settings/settings.php';
            if (file_exists($template_path))
                require_once $template_path;
        }
        function get_ourteam_single_template($single)
        {
            global $post;
            
            /* Checks for single template by post type */
            if ($post->post_type == PIKO_OURTEAM_POST_TYPE) {                
                $template_path = PIKOWORKSCORE_CORE . 'post-type/ourteam/templates/single/single-team.php';
                if (file_exists($template_path))
                    return $template_path;
            }
            return $single;
        }

        function add_columns($columns)
        {
            unset(
                $columns['cb'],
                $columns['title'],
                $columns['date']
            );
            $cols = array_merge(array('cb' => ('')), $columns);
            $cols = array_merge($cols, array('title' => esc_html__('Name', 'pikoworks_core')));
            $cols = array_merge($cols, array('job' => esc_html__('Job Position', 'pikoworks_core')));
            $cols = array_merge($cols, array('thumbnail' => esc_html__('Picture', 'pikoworks_core')));
            $cols = array_merge($cols, array('date' => esc_html__('Join Date', 'pikoworks_core')));
            return $cols;
        }

        function set_columns_value($column, $post_id)
        {
            switch ($column) {
                case 'id': {
                    echo wp_kses_post($post_id);
                    break;
                }
                case 'job': {
                    echo get_post_meta($post_id, 'job', true);
                    break;
                }
                case 'thumbnail': {
                    echo get_the_post_thumbnail($post_id, 'thumbnail');
                    break;
                }
            }
        }

        function register_meta_boxes($meta_boxes)
        {
            global $meta_boxes; 
            $prefix = 'pikoworks_';
            $meta_boxes[] = array(
                'id' => $prefix . 'ourteam_meta_box',
                'title' => esc_html__('OurTeam Single Page setting', 'pikoworks_core'),
                'pages' => array('ourteam'),
                'fields' => array(
                    array(
                        'name'  => esc_html__( 'Team Single page layout', 'pikoworks_core' ),
                        'desc' => esc_html__( 'option Override theme option', 'pikoworks_core' ),
                        'id'    => $prefix . 'team_detail_style',
                        'type'  => 'image_set',
                        'allowClear' => true,
                        'options' => array(
                                '1' => get_template_directory_uri() . '/assets/images/theme-options/team-single1.jpg',
                                '2' => get_template_directory_uri() . '/assets/images/theme-options/team-single2.jpg',
                                '3' => get_template_directory_uri() . '/assets/images/theme-options/team-single3.jpg'
                        ),
                        'std'	=> '',
                        'multiple' => false,
                    ),
                    array(
                        'name' => esc_html__('Job Position', 'pikoworks_core'),
                        'id' => $prefix .'job',
                        'type' => 'text',
                    ),
                    array(
                        'name' => esc_html__('Link Resume text', 'pikoworks_core'),
                        'id' => $prefix .'resume_text',
                        'type' => 'text',
                        'std'         => esc_html__('Resume','pikoworks_core')
                    ),
                    array(
                        'name' => esc_html__('About Heading', 'pikoworks_core'),
                        'id' => $prefix .'team_single_about',
                        'type' => 'text',
                        'required-field' => array($prefix . 'team_detail_style','=',array('1','2')),
                        'std'         => esc_html__('About','pikoworks_core')
                    ),
                    array(
                        'name' => esc_html__('My Experience Title', 'pikoworks_core'),
                        'id' => $prefix .'team_single_experience',
                        'type' => 'text',
                        'required-field' => array($prefix . 'team_detail_style','=',array('1')),
                        'std'         => esc_html__('My Experience','pikoworks_core')
                    ),
                    array(
                        'name' => esc_html__('My Skill Title', 'pikoworks_core'),
                        'id' => $prefix .'team_single_skill',
                        'type' => 'text',
                        'required-field' => array($prefix . 'team_detail_style','=',array('1','2')),
                        'std'         => esc_html__('My Skill','pikoworks_core')
                    ),                                        
                )
            );
            return $meta_boxes;
        }

        function ourteam_shortcode($atts)
        {
            
            extract(shortcode_atts(array(
                'layout_style' => 'style1', 
                'number'        => 8,
                'order' => 'DESC',
                'orderby' => 'date',
                'show_excerpt'   => '',
                'excerpt'   => '25',
                'show_lightbox'   => '',
                
                'overlay_btn' => 'top-to-center',                
                'column' => '4',                
                'is_slider' => '',                
                'category' => '',                
                //Carousel            
                'autoplay'      => "false",
                'navigation'    => "false",
                'navigation_btn' => 'owl-nav-show-inner',
                'slidespeed'    => 250,
                'loop'          => "false",
                'dots'         => "false",
                'margin'        => 10,                 
                //Default
                'use_responsive' => 1,
                'sanam' => '',
                'items_large_device'   => 4,
                'items_desktop'   => 3,
                'items_tablet'   => 2,
                'items_mobile'   => 1,               
               
                'el_class'      => '',
                
                
            ), $atts));

            
            global $meta;
            $args = array(
                'post_type' => 'ourteam',                
                'post_status' => 'publish',
                'orderby' => esc_attr( $orderby ),
                'order'   => esc_attr( $order ),
                'posts_per_page' => esc_attr( $number ),    
            );
            if ($category != '') {
                $args['tax_query'] = array(
                    array(
                        'taxonomy' => 'ourteam_category',
                        'field' => 'slug',
                        'terms' => explode(',', $category),
                        'operator' => 'IN'
                    )
                );
            }
            
            $data = new WP_Query($args);
            global $pikoworks;            
            ob_start();
            $class_warp = '';
            if ($layout_style == 'style1') {
              $class_warp = 'col-sm-6 col-xs-12 ';
            }            
            $css_class = 'ourteam-wrap ourteam-archive columns-' . $column . ' ' . $layout_style . ' ' . ' ' . $el_class; 
            
            $single_animation = '';
            if($use_responsive == '0'){
                $single_animation = $sanam;
            }
            
            $data_carousel = array(
                "autoplay"      => esc_html($autoplay),
                "navigation"    => esc_html($navigation),
                "margin"        => esc_html($margin),
                "smartSpeed"    => esc_html($slidespeed),
                "loop"          => esc_html($loop),
                "autoheight"    => "false",
                'nav'           => esc_html($navigation),
                'dots'          => esc_html($dots),
            );                     
            if( $use_responsive == '1' ) {
                $arr = array(
                    '0' => array(
                        "items" => esc_html($items_mobile)
                    ), 
                    '768' => array(
                        "items" => esc_html($items_tablet)
                    ), 
                    '992' => array(
                        "items" => esc_html($items_desktop)
                    ),
                    '1200' => array(
                        "items" => esc_html($items_large_device)
                    )
                );
                $data_responsive = json_encode($arr);
                $data_carousel["responsive"] = $data_responsive;
                
                if( ( $data->post_count <= 1  ) ){
                    $data_carousel['loop'] = 'false';
                }
                
            }else{
                $data_carousel['items'] =  1;
                
            }            
            $class_col = '';
            if($is_slider === 'yes'){
                $owl_class = 'owl-carousel' . ' ' . $navigation_btn;
                $owl_data_option = _data_carousel( $data_carousel );                
            }else{
                $class_col = 'col-lg-' . (12 / esc_attr($column)) . ' col-md-' . (12 / esc_attr($column)) . ' col-sm-6  col-xs-12';
                $owl_data_option = '';
                $owl_class = 'row';
            }
            $style2_row = 'row';
            if($layout_style == 'style2'){
                $style2_row = '';
            }
            
            if ($data->have_posts()) :?>
            <div class="<?php echo esc_attr( $css_class) ?>">
                <div class="<?php echo esc_attr( $owl_class) ?>"  <?php echo $owl_data_option ?>>
                <?php
                while ($data->have_posts()): $data->the_post();
                    $prefix = 'pikoworks_';
                    $job = get_post_meta(get_the_ID(), $prefix . 'job', true);
                    $resume = get_post_meta(get_the_ID(), $prefix .'resume_text', true);
                    $img_id = get_post_thumbnail_id();
                    $img = wpb_getImageBySize(array('attach_id' => $img_id, 'thumb_size' => '270x270'));
                    if($column == 3){
                     $img = wpb_getImageBySize(array('attach_id' => $img_id, 'thumb_size' => '390x271'));
   
                    }                    
                    $img_src=wp_get_attachment_image_src( $img_id, "attached-image" );
                
                
                
                    $plugin_path = untrailingslashit(plugin_dir_path(__FILE__));
                    switch ($layout_style) {
                        case 'style4':{
                            include($plugin_path . '/templates/style3.php');
                            break;
                        }
                        case 'style3':
                        {
                            include($plugin_path . '/templates/style2.php');
                            break;
                        }
                        default:
                        {
                            include($plugin_path . '/templates/style1.php');
                        }
                    }
                endwhile;
                ?>                
                </div>
            </div>
            <?php endif; //have post
            wp_reset_postdata();
            $content = ob_get_clean();
            return $content;
        }
    }

    new piko_Shortcode_Ourteam();
}