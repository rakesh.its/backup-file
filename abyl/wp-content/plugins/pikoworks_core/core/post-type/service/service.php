<?php
/**
 * Custom post service
 * @author themepiko
 */
if( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
if (!defined('PIKOWORKS_SERVICE_POST_TYPE'))
    define('PIKOWORKS_SERVICE_POST_TYPE', 'service');
if (!defined('PIKOWORKS_SERVICE_CATEGORY_TAXONOMY'))
    define('PIKOWORKS_SERVICE_CATEGORY_TAXONOMY', 'service-category');
if (!defined('PIKOWORKS_SERVICE_DIR_PATH'))
    define('PIKOWORKS_SERVICE_DIR_PATH', plugin_dir_path(__FILE__));

if (!class_exists('pikoworks_custom_post_type_service')) {
    class pikoworks_custom_post_type_service
    {
        function __construct()
        {
            add_action('init', array($this, 'register_taxonomies'), 5);
            add_action('init', array($this, 'register_post_types'), 6);
            add_shortcode('piko_service', array($this, 'service_shortcode'));
            add_filter('rwmb_meta_boxes', array($this, 'register_meta_boxes'));
            add_filter('single_template', array($this, 'get_service_single_template'));

            if (is_admin()) {
                add_filter('manage_edit-' . PIKOWORKS_SERVICE_POST_TYPE . '_columns', array($this, 'add_services_columns'));
                add_action('manage_' . PIKOWORKS_SERVICE_POST_TYPE . '_posts_custom_column', array($this, 'set_services_columns_value'), 10, 2);
                add_action('restrict_manage_posts', array($this, 'service_manage_posts'));
                add_filter('parse_query', array($this, 'convert_taxonomy_term_in_query'));
                add_action('admin_menu', array($this, 'addMenuChangeSlug'));
            }

        }

        function front_scripts()
        {
            global $pikoworks;
            $min_suffix = (isset($pikoworks['enable_minifile']) && $pikoworks['enable_minifile'] == 1) ? '.min' : '';
//            wp_enqueue_style('pikoworks-service', PIKOWORKSCORE_CORE_URL . 'post-type/service/assets/css/service'.$min_suffix.'.css', array(),false); 

        }
        function register_post_types()
        {

            $post_type = PIKOWORKS_SERVICE_POST_TYPE;

            if (post_type_exists($post_type)) {
                return;
            }

            $post_type_slug = get_option('piko-' . $post_type . '-config');
            if (!isset($post_type_slug) || !is_array($post_type_slug)) {
                $slug = 'service';
                $name = $singular_name = 'Service';
            } else {
                $slug = $post_type_slug['slug'];
                $name = $post_type_slug['name'];
                $singular_name = $post_type_slug['singular_name'];
            }

            register_post_type($post_type,
                array(
                    'label' => esc_html__('Service', 'pikoworks_core'),
                    'description' => esc_html__('Service Description', 'pikoworks_core'),
                    'labels' => array(
                        'name' => $name,
                        'singular_name' => $singular_name,
                        'menu_name' => esc_html__($name, 'pikoworks_core'),
                        'parent_item_colon' => esc_html__('Parent Item:', 'pikoworks_core'),
                        'all_items' => esc_html__(sprintf('All %s', $name), 'pikoworks_core'),
                        'view_item' => esc_html__('View Item', 'pikoworks_core'),
                        'add_new_item' => esc_html__(sprintf('Add New  %s', $name), 'pikoworks_core'),
                        'add_new' => esc_html__('Add New', 'pikoworks_core'),
                        'edit_item' => esc_html__('Edit Item', 'pikoworks_core'),
                        'update_item' => esc_html__('Update Item', 'pikoworks_core'),
                        'search_items' => esc_html__('Search Item', 'pikoworks_core'),
                        'not_found' => esc_html__('Not found', 'pikoworks_core'),
                        'not_found_in_trash' => esc_html__('Not found in Trash', 'pikoworks_core'),
                    ),
                    'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
                    'public' => true,
                    'show_ui' => true,
                    '_builtin' => false,
                    'has_archive' => true,
                    'menu_icon' => 'dashicons-megaphone',
                    'rewrite' => array('slug' => $slug, 'with_front' => true),
                )
            );
            flush_rewrite_rules();
        }

        function register_taxonomies()
        {
            if (taxonomy_exists(PIKOWORKS_SERVICE_CATEGORY_TAXONOMY)) {
                return;
            }

            $post_type = PIKOWORKS_SERVICE_POST_TYPE;
            $taxonomy_slug = PIKOWORKS_SERVICE_CATEGORY_TAXONOMY;
            $taxonomy_name = 'Service Categories';

            $post_type_slug = get_option('piko-' . $post_type . '-config');
            if (isset($post_type_slug) && is_array($post_type_slug) &&
                array_key_exists('taxonomy_slug', $post_type_slug) && $post_type_slug['taxonomy_slug'] != ''
            ) {
                $taxonomy_slug = $post_type_slug['taxonomy_slug'];
                $taxonomy_name = $post_type_slug['taxonomy_name'];
            }
            register_taxonomy(PIKOWORKS_SERVICE_CATEGORY_TAXONOMY, PIKOWORKS_SERVICE_POST_TYPE,
                array(
                    'hierarchical' => true,
                    'label' => $taxonomy_name,
                    'query_var' => true,
                    'rewrite' => array('slug' => $taxonomy_slug)
                    )
            );
            flush_rewrite_rules();
        }

        function service_shortcode($atts)
        {
            $this->front_scripts();
            extract(shortcode_atts(array(
                'layout_type' => 'grid',
                'data_source' => '',
                'show_pagging' => '',
                'show_category' => '',
                'excerpt' => '20',
                'category' => '',
                'service_ids' => '',
                'column' => '2',
                'item' => '',
                'order' => 'DESC',
                'padding' => '',
                'is_content_center' => '',
                'image_size' => '360x202',
                'overlay_style' => 'style-1',
                'el_class' => '',
                'current_page' => '1',
                
                
                'image_overlay' => '', // theme version 1.2
                
                'show_read_more_btn' => 'yes',
                'read_more_text' => esc_html__('Read More', 'pikoworks_core'), 
                //Carousel            
                'autoplay'      => "false",
                'navigation'    => "false",
                'navigation_btn' => 'owl-nav-show-inner',
                'slidespeed'    => 250,
                'loop'          => "false",
                'dots'         => "false",
                'margin'        => 30,                 
                //Default
                'items_large_device'   => 4,
                'items_desktop'   => 3,
                'items_tablet'   => 2,
                'items_mobile'   => 1,
                
                
            ), $atts));
            
            
            if ($show_pagging == '2' || $item=='') {
                $offset = 0;
                $post_per_page = -1;
            } else {
                $post_per_page = $item;
                $offset = ($current_page - 1) * $item;
            }
            
            $css_class = 'service-wrap service-' .$overlay_style . ' ' . $image_overlay .' ' . $el_class;            
                        
            $plugin_path = untrailingslashit(plugin_dir_path(__FILE__));
            $template_path = $plugin_path . '/templates/listing.php';
           
            ob_start();
            include($template_path);
            $result = ob_get_contents();
            ob_end_clean();
            return $result;
        }

        function register_meta_boxes($meta_boxes)
        {

            $widgets_list = array();
            foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
                $widgets_list[$sidebar['id']] = ucwords( $sidebar['name'] );
            }
            global $meta_boxes;
            $prefix = 'piko_';
            
            
            $meta_boxes[] = array(
                'title' => esc_html__('Service Single Layout', 'pikoworks_core'),
                'id' => $prefix .'meta-box-service-single-page',
                'pages' => array(PIKOWORKS_SERVICE_POST_TYPE),
                'tab' => true,
                'fields' => array(                    
                    array(
                        'name'     => esc_html__( 'View Detail Style', 'pikoworks_core' ),
                        'id'       => $prefix .'service_detail_style',
                        'type'     => 'image_set',
                        'options'  => array(
                            'none'      =>   get_template_directory_uri() . '/assets/images/theme-options/service_single_layout.jpg',
                            'detail-01' => get_template_directory_uri() . '/assets/images/theme-options/service_single_layout_1.jpg',
                            'detail-02' => get_template_directory_uri() . '/assets/images/theme-options/service_single_layout_2.jpg',
                            'detail-03' => get_template_directory_uri() . '/assets/images/theme-options/service_single_layout_3.jpg',
                            'detail-04' => get_template_directory_uri() . '/assets/images/theme-options/service_single_layout_4.jpg',
                        ),
                        'multiple'    => false,
                        'std'         => 'none',
                    ),
                    array(
                        'name' => esc_html__('Upload an image', 'pikoworks_core'),
                        'desc' => esc_html__('if uploaded multiple image then Enable slide'),
                        'id' => $prefix .'service_sec_img',
                        'type' => 'image_advanced',
                        'required-field' => array($prefix . 'service_detail_style','=',array('none', 'detail-01','detail-02','detail-03')),
                    ),
                    array (
                        'name' 	=> esc_html__('Enable Slide', 'pikoworks_core'),
                        'id' 	=> $prefix . 'enable_carousel',
                        'type' 	=> 'checkbox',
                        'desc' => esc_html__("if check upload multiple image please", 'pikoworks_core'),
                        'std'	=> '0',
                        'required-field' => array($prefix . 'service_detail_style','=',array('none', 'detail-01','detail-02','detail-03')),
                     ),
                    array(
                        'name' => esc_html__('Video Embaded', 'pikoworks_core'),
                        'id' => $prefix .'service_video',
                        'type' => 'oembed',
                        'required-field' => array($prefix . 'service_detail_style','=',array('none', 'detail-01','detail-02','detail-03')),
                    ),
                    array (
                        'name' 	=> esc_html__('Single page Sidebar', 'pikoworks_core'),
                        'id' 	=> $prefix .'service_detail_sidebar',
                        'type' 	=> 'select',
                        'placeholder' => esc_html__('Select a Sidebar','pikoworks_core'),
                        'options' => $widgets_list,
                        'required-field' => array($prefix . 'service_detail_style','=',array('detail-04', 'detail-01','detail-02','detail-03')),
                    ),
                )
            );           
           
            return $meta_boxes;
        }

        function get_service_single_template($single)
        {
            global $post;
            /* Checks for single template by post type */
            if ($post->post_type == PIKOWORKS_SERVICE_POST_TYPE) {
                $plugin_path = untrailingslashit(PIKOWORKS_SERVICE_DIR_PATH);
                $template_path = $plugin_path . '/templates/single/single-service.php';
                if (file_exists($template_path))
                    return $template_path;
            }
            return $single;
        }

        function add_services_columns($columns)
        {
            unset(
            $columns['cb'],
            $columns['title'],
            $columns['date']
            );
            $cols = array_merge(array('cb' => ('')), $columns);
            $cols = array_merge($cols, array('title' => esc_html__('Name', 'pikoworks_core')));
            $cols = array_merge($cols, array('thumbnail' => esc_html__('Thumbnail', 'pikoworks_core')));
            $cols = array_merge($cols, array(PIKOWORKS_SERVICE_CATEGORY_TAXONOMY => esc_html__('Categories', 'pikoworks_core')));
            $cols = array_merge($cols, array('date' => esc_html__('Date', 'pikoworks_core')));
            return $cols;
        }

        function set_services_columns_value($column, $post_id)
        {

            switch ($column) {
                case 'id':
                {
                    echo wp_kses_post($post_id);
                    break;
                }
                case 'thumbnail':
                {
                    echo get_the_post_thumbnail($post_id, 'thumbnail');
                    break;
                }
                case PIKOWORKS_SERVICE_CATEGORY_TAXONOMY:
                {
                    $terms = wp_get_post_terms(get_the_ID(), array(PIKOWORKS_SERVICE_CATEGORY_TAXONOMY));
                    $cat = '<ul>';
                    foreach ($terms as $term) {
                        $cat .= '<li><a href="' . get_term_link($term, PIKOWORKS_SERVICE_CATEGORY_TAXONOMY) . '">' . $term->name . '<a/></li>';
                    }
                    $cat .= '</ul>';
                    echo wp_kses_post($cat);
                    break;
                }
            }
        }

        function service_manage_posts()
        {
            global $typenow;
            if ($typenow == PIKOWORKS_SERVICE_POST_TYPE) {
                $selected = isset($_GET[PIKOWORKS_SERVICE_CATEGORY_TAXONOMY]) ? $_GET[PIKOWORKS_SERVICE_CATEGORY_TAXONOMY] : '';
                $args = array(
                    'show_count' => true,
                    'show_option_all' => esc_html__('Show All Categories', 'pikoworks_core'),
                    'taxonomy' => PIKOWORKS_SERVICE_CATEGORY_TAXONOMY,
                    'name' => PIKOWORKS_SERVICE_CATEGORY_TAXONOMY,
                    'selected' => $selected,

                );
                wp_dropdown_categories($args);
            }
        }

        function convert_taxonomy_term_in_query($query)
        {
            global $pagenow;
            $qv = & $query->query_vars;
            if ($pagenow == 'edit.php' &&
                isset($qv[PIKOWORKS_SERVICE_CATEGORY_TAXONOMY]) &&
                is_numeric($qv[PIKOWORKS_SERVICE_CATEGORY_TAXONOMY])
            ) {
                $term = get_term_by('id', $qv[PIKOWORKS_SERVICE_CATEGORY_TAXONOMY], PIKOWORKS_SERVICE_CATEGORY_TAXONOMY);
                $qv[PIKOWORKS_SERVICE_CATEGORY_TAXONOMY] = $term->slug;
            }
        }

        function addMenuChangeSlug()
        {
            add_submenu_page('edit.php?post_type=service', 'Setting', 'Settings', 'edit_posts', wp_basename(__FILE__), array($this, 'initPageSettings'));
        }

        function initPageSettings()
        {
            $template_path = ABSPATH . 'wp-content/plugins/pikoworks_core/core/post-type/posttype-settings/settings.php';
            if (file_exists($template_path))
                require_once $template_path;
        }

    }

    new pikoworks_custom_post_type_service();
}