<?php
/**
 * Custom post
 * @author themepiko
 */

$args = array(
    'offset' => esc_attr($offset),
    'orderby' => 'post__in',
    'post__in' => explode(",", $portfolio_ids),
    'posts_per_page' => esc_attr($post_per_page),
    'post_type' => PIKO_PORTFOLIO_POST_TYPE,
    'post_status' => 'publish');

if ($data_source == '') {
    $args = array(
        'offset' => esc_attr($offset),
        'posts_per_page' => esc_attr($post_per_page),
        'orderby' => 'post_date',
        'order' => esc_attr($order),
        'post_type' => PIKO_PORTFOLIO_POST_TYPE,
        PIKO_PORTFOLIO_CATEGORY_TAXONOMY => strtolower($category),
        'post_status' => 'publish');
}

$posts_array = new WP_Query($args);
$total_post = $posts_array->found_posts;
$col_class = '';
if ($data_section_id == '')
    $data_section_id = uniqid();

?>
<div class="portfolio-container">
    <div class="portfolio overflow-hidden <?php echo esc_attr( $css_class) ?> "
        id="portfolio-<?php echo esc_attr($data_section_id) ?>">       
            <div class="portfolio-tabs">            
           <div 
               class="portfolio-wrapper <?php echo esc_html( sprintf('%s %s', $padding, $layout_type) )?>" 
               data-section-id="<?php echo esc_attr($data_section_id) ?>"
               id="portfolio-container-<?php echo esc_attr($data_section_id) ?>"
               >

            <?php
            $index = 0;

            while ($posts_array->have_posts()) : $posts_array->the_post();
                $index++;
                $permalink = get_permalink();
                $title_post = get_the_title();
                $terms = wp_get_post_terms(get_the_ID(), array(PIKO_PORTFOLIO_CATEGORY_TAXONOMY));
                $cat = $cat_filter = '';
                foreach ($terms as $term) {
                    $cat_filter .= $term->slug . ' ';
                    $cat .= $term->name . ', ';
                }
                $cat = rtrim($cat, ', ');

                ?>

                <?php
                 include(plugin_dir_path(__FILE__) . '/loop/one-page-item.php');
                ?>
            <?php
            endwhile;
            wp_reset_postdata();
            ?>

        </div>
        <?php if ($show_pagging == '1' && $post_per_page > 0 && $total_post / $post_per_page > 1 && $total_post > ($post_per_page * $current_page)) { ?>
            <div style="clear: both"></div>
            <div class="paging" id="load-more-<?php echo esc_attr($data_section_id) ?>">
                <a href="javascript:void(0)" class="piko-button button-3x ladda-button "
                   data-source="<?php echo esc_attr($data_source) ?>"
                   data-tab-category-action="<?php echo esc_attr($tab_category_action) ?>"
                   data-load-type="<?php echo esc_attr($tab_category_action) ?>"
                   data-overlay-style="<?php echo esc_attr($overlay_style) ?>"
                   data-category="<?php echo esc_attr($category) ?>"
                   data-portfolio-ids="<?php echo esc_attr($portfolio_ids) ?>"
                   data-section-id="<?php echo esc_attr($data_section_id) ?>"
                   data-current-page="<?php echo esc_attr($current_page + 1) ?>"
                   data-column="<?php echo esc_attr($column); ?>"
                   data-offset="<?php echo esc_attr($offset) ?>"
                   data-current-page="<?php echo esc_attr($current_page) ?>"
                   data-post-per-page="<?php echo esc_attr($post_per_page) ?>"
                   data-show-paging="<?php echo esc_attr($show_pagging) ?>"
                   data-padding="<?php echo esc_attr($padding) ?>"
                   data-layout-type="<?php echo esc_attr($layout_type) ?>"
                   data-order="<?php echo esc_attr($order) ?>"
                   data-style="zoom-out" data-spinner-color="#fff"
                    ><?php esc_html_e('Load more', 'pikoworks_core') ?></a>
            </div>
        <?php } ?>

    </div>
</div>
</div>


<?php if (isset($ajax_load) && $ajax_load == '0') { ?>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                <?php if (piko_enable_hover_direction($overlay_style)) {?>
                $('.portfolio-item.hover-dir > .entry-thumbnail').hoverdir();
                <?php } ?>
                PortfolioAjaxAction.init('<?php echo esc_url(get_site_url() . '/wp-admin/admin-ajax.php') ?>', '<?php echo esc_attr($tab_category_action) ?>', '<?php echo esc_attr($data_section_id)?>');
            })

        })(jQuery);
    </script>
<?php } ?>


