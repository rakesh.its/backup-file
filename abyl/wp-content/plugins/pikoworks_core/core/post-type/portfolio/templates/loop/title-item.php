<div class="portfolio-item title<?php echo esc_attr($cat_filter. ' ' . $is_directioanl ) ?>">
    <?php
    $post_thumbnail_id = get_post_thumbnail_id(  get_the_ID() );
    $arrImages = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
    $width = 585;
    $height = 585;
    if($image_size=='590x393')
    {
        $width = 590;
        $height = 393;
    }
    if($image_size=='570x460')
    {
        $width = 570;
        $height = 460;
    }
    $thumbnail_url = '';
    if(count($arrImages)>0){
        $resize = matthewruddy_image_resize($arrImages[0],$width,$height);
        if($resize!=null && is_array($resize) )
            $thumbnail_url = $resize['url'];
    }

    $url_origin = $arrImages[0];
    $cat = '';
    foreach ( $terms as $term ){
        $cat .= $term->name.', ';
    }
    $cat = rtrim($cat,', ');
    
    $disable_link = isset( $GLOBALS['pikoworks']['portfolio_disable_link_detail'] ) ? $GLOBALS['pikoworks']['portfolio_disable_link_detail'] : '1';

    ?>

    <figure class="entry-thumbnail">
        <img width="<?php echo esc_attr($width) ?>" height="<?php echo esc_attr($height) ?>" src="<?php echo esc_url($thumbnail_url) ?>" alt="<?php echo get_the_title() ?>"/>
        <figcaption class="entry-thumbnail-hover">
            <div class="entry-hover-wrapper">
                <div class="entry-hover-inner title-bottom">                     
                    <a class="link" href="<?php echo esc_url($url_origin) ?>" data-rel="prettyPhoto[pp_gal_<?php echo get_the_ID() ?>]"  title="<?php echo get_the_title() ?>">
                        <span class="lnr-magnifier" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        </figcaption>
    </figure>
    <div class="entry-title-wrapper">
        <div class="entry-title-inner">
            <?php if ( $disable_link =='1'){?>
                <h5><?php the_title() ?></h5>
            <?php } else{?>
                <h4><a href="<?php echo get_permalink(get_the_ID()) ?>" class="title"><?php the_title() ?></a></h4>
            <?php }?>
            <span class="category"><?php echo wp_kses_post($cat) ?></span>
        </div>
    </div>
    <?php
    include(plugin_dir_path(__FILE__) . '/gallery.php');
    ?>
</div>
