<?php

/* 
 * woocommerce core function
 */
/* 
 * woocommerce product category list use breadcrubm
 */

function pikoworks_wc_product_cats_list() {
    global $pikoworks, $wp_query;
    
    $html = '';
    
    $header_shop_product_cats = isset( $pikoworks['optn_header_shop_product_cats'] ) ? $pikoworks['optn_header_shop_product_cats'] : 'auto'; // "auto" or "manually"
    $header_shop_cats_list = isset( $pikoworks['optn_show_header_product_cats'] ) ? $pikoworks['optn_show_header_product_cats'] : 0; // "off" or "on"
    
    if($header_shop_cats_list == 0){
        return;
    }
    
    if ( $header_shop_product_cats == 'auto' ) {
        
        // Array of active product categories (in single product)
        $active_pro_cats = array();
        
        // Get list or categoris in current single product
        if ( is_product() ) {
            $terms = wp_get_post_terms( get_the_ID(), 'product_cat' );
            if ( !is_wp_error( $terms ) ) {
                if ( !empty( $terms ) ) {
                    foreach ( $terms as $term ):
                        $active_pro_cats[] = $term->term_id;
                    endforeach;
                }
            }
        }
        
        $args = array(
           'hierarchical' => true,
           'show_option_none' => '',
           'hide_empty' => 0,
           'taxonomy' => 'product_cat'
        );
        
        if ( is_product_category() ) {
            $cur_pro_cat = $wp_query->get_queried_object();
            if ( $cur_pro_cat ) {
                $args['parent'] = $cur_pro_cat->term_id;   
            }
        }
        
        $pro_cats = get_categories( $args );
        
        if ( !empty( $pro_cats ) ) {
            foreach ( $pro_cats as $pro_cat ):
            
                $cat_link = get_term_link( $pro_cat->slug, $pro_cat->taxonomy );
                $li_class = 'product_cat_li';
                if ( is_shop() ) { 
                    if ( $pro_cat->category_parent == 0 ) { // Show only parent
                        if ( in_array( $pro_cat->term_id, $active_pro_cats ) ) {
                            $li_class .= ' active_product_category';
                        }
                        $html .= '<li class="' . esc_attr( $li_class ) . '"><a href="' . esc_url( $cat_link ) . '">' . sanitize_text_field( $pro_cat->name ) . '</a></li>';
                    }
                }
                else{
                    if ( in_array( $pro_cat->term_id, $active_pro_cats ) ) {
                        $li_class .= ' active_product_category';
                    }
                    $html .= '<li class="' . esc_attr( $li_class ) . '"><a href="' . esc_url( $cat_link ) . '">' . sanitize_text_field( $pro_cat->name ) . '</a></li>';
                }
                
                
            endforeach;
        }
        
    }
    else{ // manually
        $cat_slugs = isset( $pikoworks['optn_woo_product_cats_show_on_archive'] ) ? $pikoworks['optn_woo_product_cats_show_on_archive'] : '';
        $cat_slugs = preg_replace( '/\s+/', '', $cat_slugs );
        
        if ( trim( $cat_slugs ) != '' ) {
            $cat_slugs = explode( ',', $cat_slugs );
            
            foreach ( $cat_slugs as $cat_slug ):
                
                $category = get_term_by( 'slug', trim( $cat_slug ), 'product_cat' );
                if ( $category != false ) {
                    $html .= '<li class="product_cat_li"><a href="' . esc_url( get_term_link( $category, 'product_cat' ) ) . '">' . sanitize_text_field( $category->name ) . '</a></li>';
                }
                
            endforeach;
            
        }
    }
    
    if ( trim( $html ) != '' ) {
        $html = '<ul class="list-categories product-list-categories">
                    ' .  $html  . '
                </ul>';
    }   
    
    echo do_shortcode( $html );
    
}

/**
 * woocommerce action
 * */
function pikoworks_header_add_to_cart_fragment( $fragments ) { 
    ob_start();
    $count = WC()->cart->cart_contents_count ;  
    ?><span class="cart-items" ><?php echo esc_attr($count); ?></span><?php
 
    $fragments['span.cart-items'] = ob_get_clean();
     
    return $fragments;    
}
add_filter( 'woocommerce_add_to_cart_fragments', 'pikoworks_header_add_to_cart_fragment' );

function pikoworks_wc_shop_loop_item_title(){
    ?>    
    <h3 class="product-title"><a href="<?php esc_url( the_permalink()) ?>"><?php the_title(); ?></a></h3>
    <?php
}


function pikoworks_wc_template_loop_product_thumbnail() {
 /**
 * woocommerce product thumbnail overlay
 * */
    
    global $post, $product;
    
    $image_html = '';
    $img_count = 0;
    $attachment_ids = $product->get_gallery_image_ids();
				
    if ( has_post_thumbnail() ) {
        $image_html = wp_get_attachment_image( get_post_thumbnail_id(), 'shop_catalog' );					
    }
    
    ?>                 
	<?php        
        if ( has_post_thumbnail() ) {
            
            if ($attachment_ids) {
		
		foreach ( $attachment_ids as $attachment_id ) {
                    if ( get_post_meta( $attachment_id, '_woocommerce_exclude_image', true ) )
                            continue;
                    echo '<div class="product-image">'. wp_get_attachment_image( $attachment_id, 'shop_catalog' ).'</div>';
                    $img_count++;
                    if ($img_count == 1) break;
                    }
                    echo '<div class="product-image">'. do_shortcode($image_html) .'</div>';	
                    
                    } else {
                        echo '<div class="product-image">'. do_shortcode($image_html)  .'</div>';					
			echo '<div class="product-image">'. do_shortcode($image_html)  .'</div>';                        
                    }
                    
        } else {
		echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), esc_html__( 'Placeholder', 'piko-construct' ) ), $post->ID );
	}   
}

function pikoworks_wc_template_loop_product_action(){
    ?>
     <div class="product-button">                    
            <?php
            if ( shortcode_exists( 'yith_wcwl_add_to_wishlist' ) ):
                echo do_shortcode( '[yith_wcwl_add_to_wishlist product_id="' . get_the_ID() . '"]' ); 
            endif; //yith_wcwl_add_to_wishlist

            if ( class_exists( 'YITH_WCQV_Frontend' ) ):
                $label = get_option( 'yith-wcqv-button-label' );
                echo '<a href="#" class="button yith-wcqv-button" data-product_id="' . get_the_ID() . '">' . esc_html( $label ) . '</a>';
            endif; // YITH_WCQV_Frontend
            ?>
            <?php if (in_array('yith-woocommerce-compare/init.php', apply_filters('active_plugins', get_option('active_plugins')))): ?>
                <a href="<?php the_permalink(); ?>?action=yith-woocompare-add-product&amp;id=<?php the_ID(); ?>"
                class="compare" data-product_id="<?php the_ID(); ?>"><?php esc_attr_e('Add to Compare', 'piko-construct'); ?></a>
            <?php endif; // yith-woocommerce-compare ?> 
        </div>   
    <?php
    
}


function pikoworks_wc_template_single_product_thumbnail() {
    global $post, $product;
    
    $image_html = '';
    $img_count = 0;
    $attachment_ids = $product->get_gallery_image_ids();
    $attr = array(        
        'id' => 'product-zoom',
        'data-zoom-image' => wp_get_attachment_url( get_post_thumbnail_id() ),
    );
    $post_img_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), array('150', '150') );
    $post_img_medium = wp_get_attachment_image_src( get_post_thumbnail_id(), array('600', '600') );
    $post_img_full = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
    $post_image_title = get_the_title( get_post_thumbnail_id() );
    $post_image_caption = get_post_field( 'post_excerpt', get_post_thumbnail_id() );
    
    if ( has_post_thumbnail() ) {
        $image_html = wp_get_attachment_image( get_post_thumbnail_id(), 'shop_catalog' , '', $attr );			
    }
    ?>
    
    <div class="product-gallery-container">
        <div class="product-zoom-wrapper">
            <div class="product-zoom-container">
                <div class="images">
                <?php
                if ( has_post_thumbnail() ) {  
                        $attachment_count = count( $product->get_gallery_image_ids() );
			$gallery          = $attachment_count > 0 ? '[product-gallery]' : '';
			$props            = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
			$image            = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
				'title'	 => $props['title'],
				'alt'    => $props['alt'],
                                'id' => 'product-zoom',
                                'data-zoom-image' => wp_get_attachment_url( get_post_thumbnail_id() ),
			) );
			echo apply_filters(
				'woocommerce_single_product_image_html',
				sprintf( '%s', $image ),
				$post->ID
			);
                
                } else {
                    echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img id="product-zoom" src="%s" data-zoom-image="%s" alt="%s" />', wc_placeholder_img_src(), wc_placeholder_img_src(), esc_html__( 'Placeholder', 'piko-construct' ) ), $post->ID );
                } ?>
               </div>
            </div><!-- End .product-zoom-container -->
        </div><!-- End .product-zoom-wrapper -->
        <div class="swiper-container product-gallery-carousel vertical-center-nav">            
            <div class="swiper-button-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
            <div class="swiper-button-next icon dark"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
            <div class="swiper-wrapper popup-gallery">
                <?php
                if ( has_post_thumbnail() ) : ?>
                <div class="swiper-slide">
                    <a href="javascript:void(0)" data-image="<?php echo esc_url($post_img_medium[0]); ?>" data-zoom-image="<?php echo esc_url($post_img_full[0]); ?>" title="<?php esc_attr( $post_image_caption) ?>" class="product-gallery-item">
                        <img src="<?php echo esc_url($post_img_thumb[0]); ?>" alt="<?php esc_attr( $post_image_title) ?>">
                    </a>
                </div>
                <?php
                else:
                    echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<a href="javascript:void(0)" data-image="%s" data-zoom-image="%s" class="product-gallery-item" title="%s"> <img class="placeholder-thumb" src="%s" alt="%s"/></a>', wc_placeholder_img_src(), wc_placeholder_img_src(), esc_html__( 'Placeholder', 'piko-construct' ), wc_placeholder_img_src(), esc_html__( 'Placeholder', 'piko-construct' ) ), $post->ID );
                endif; ?>
                <?php                
                if ($attachment_ids) {
                    foreach ( $attachment_ids as $attachment_id ) {
                        if ( get_post_meta( $attachment_id, '_woocommerce_exclude_image', true ) )
                                continue;
                        $img_thumb = wp_get_attachment_image_src( $attachment_id, array('150', '150') );
                        $img_medium = wp_get_attachment_image_src( $attachment_id, array('600', '600'));
                        $img_full = wp_get_attachment_image_src( $attachment_id, 'full');
                        
                        $image_title 	= get_the_title( $attachment_id );
			$image_caption 	= get_post_field( 'post_excerpt', $attachment_id );
                        
                        ?>
                        <div class="swiper-slide">
                            <a href="javascript:void(0)" data-image="<?php echo esc_url($img_medium[0]); ?>" data-zoom-image="<?php echo esc_url($img_full[0]); ?>" title="<?php esc_attr( $image_caption) ?>" class="product-gallery-item">
                                <img src="<?php echo esc_url($img_thumb[0]); ?>" alt="<?php esc_attr( $image_title) ?>">
                            </a>
                        </div>
                        <?php
                        $img_count++;
                    }
                }
                ?>
            </div><!-- End .swiper-wrapper -->
        </div><!-- End .swiper-container -->
    </div><!-- End .product-gallery-container -->
    <?php
}

function pikoworks_wc_template_loop_category_thumbnail() {
 /**
 * woocommerce product category thumbnail overlay
 * */
    
    global $post, $product;
    
    $image_html = '';
    $img_count = 0;
    $attachment_ids = $product->get_gallery_image_ids();
				
    if ( has_post_thumbnail() ) {
        $image_html = wp_get_attachment_image( get_post_thumbnail_id(), 'shop_catalog' );					
    }
    echo '<figure class="category-img-wrap">';    
        if ($attachment_ids) {
            echo '<div class="image-product-gallery">';
            echo '<a href="javascript:void(0)" class="change">' . do_shortcode($image_html) . '</a>';
            foreach ( $attachment_ids as $attachment_id ) {
                if ( get_post_meta( $attachment_id, '_woocommerce_exclude_image', true ) )
                        continue;
                echo '<a href="javascript:void(0)" class="change">'. wp_get_attachment_image( $attachment_id, 'shop_catalog' ) . '</a>';
                $img_count++;
                if ($img_count == 2) break;
                }
                
                echo '</div><div class="image-product"> <a href="javascript:void(0)" class="woocommerce-main-image">'. do_shortcode($image_html) .'</a></div>';
                } else {
                    echo '<div class="image-product">'. do_shortcode($image_html)  .'</div>';					
                    echo '<div class="image-product-gallery">'. do_shortcode($image_html)  .'</div>';                        
                }
    echo '</figure>';       
}


if (!function_exists('pikoworks_woocommerce_custom_sales_price')) {

    /**
     * Sale price Percentage
     */ 
    function pikoworks_woocommerce_custom_sales_price($product) {

        global $post, $product;
        if (!$product->is_in_stock() || $product->is_type('grouped'))
            return;
        $sale_price = get_post_meta($product->get_id(), '_price', true);
        $regular_price = get_post_meta($product->get_id(), '_regular_price', true);
        if (empty($regular_price)) { //then this is a variable product
            $available_variations = $product->get_available_variations();
            $variation_id = $available_variations[0]['variation_id'];
            $variation = new WC_Product_Variation($variation_id);
            $regular_price = $variation->get_regular_price();
            $sale_price = $variation->get_sale_price();
            if (empty($sale_price)) {
                $variation_id = $available_variations[1]['variation_id'];
                $variation = new WC_Product_Variation($variation_id);
                $sale_price = $variation->get_sale_price();
            }
        }
        $percentage = ceil(( ($regular_price - $sale_price) / $regular_price ) * 100);
        if (!empty($regular_price) && !empty($sale_price) && $regular_price > $sale_price) :
            return sprintf('<span class="onsale">-%1$s</span>', esc_attr($percentage) . esc_attr__('%', 'piko-construct'), $post, $product);
        endif;
    }

    add_filter('woocommerce_sale_flash', 'pikoworks_woocommerce_custom_sales_price');
}




if ( !function_exists( 'pikoworks_woocommerce_add_badge_new_in_list' ) ) {
	function pikoworks_woocommerce_add_badge_new_in_list()
	{
		global $post;
                $enable_new = pikoworks_get_option_data( 'optn_show_new_product_label', 1 );
                $days_count = pikoworks_get_option_data( 'optn_new_product_label', '30' );
                $label_text = pikoworks_get_option_data( 'optn_new_product_label_text', esc_html__('New', 'piko-construct') );
                if($enable_new == 0){
                    return;
                }
		$post_date = get_the_time( 'Y-m-d', $post );
		$post_date_stamp = strtotime( $post_date );
		$newness = esc_attr($days_count);
		if ( ( time() - ( 60 * 60 * 24 * $newness ) ) < $post_date_stamp ) {
			$class = 'onsale new-badge';
			echo '<span class="' . esc_attr($class) . '">' . esc_attr($label_text) . '</span>';
		}
	}
}
function pikoworks_woocommerce_add_badge_out_of_stock() {
    global $product; 
    $out_of_stock_label = pikoworks_get_option_data( 'optn_product_out_of_stock_label', esc_html__('Out of stock', 'piko-construct') );
    if(empty($out_of_stock_label)){
        return;
    }
    if ( !$product->is_in_stock() ) {
        echo '<span class="onsale outofstock">' . esc_attr($out_of_stock_label). '</span>';
    }
}
if ( !function_exists( 'pikoworks_wc_template_loop_product_coundown' ) ) {
    function pikoworks_wc_template_loop_product_coundown() {
        $id = get_the_ID();
        $time = pikoworks_get_max_date_sale( $id );
        $y = date( 'Y', $time );
        $m = date( 'm', $time );
        $d = date( 'd', $time );

        $sale_price_dates_from = ( $date = get_post_meta( $id, '_sale_price_dates_from', true ) ) ? date_i18n( 'Y-m-d', $date ) : '';

        if ( $sale_price_dates_from !== '')  {                    
        ?>                    
            <div class="countdown-lastest product-countdown" data-y="<?php echo esc_attr( $y );?>" data-m="<?php echo esc_attr( $m );?>" data-d="<?php echo esc_attr( $d );?>" data-h="00" data-i="00" data-s="00" ></div>   
        <?php
        } 
    }
}
/**
 * Remove by js after document ready 
 **/
function pikoworks_remove_wishlist_single_product_open() {
    ?>
    <div class="pikoworks-wishlist-content-remove hidden">
    <?php
}

function pikoworks_remove_wishlist_single_product_close() {
    ?>
    </div><!-- pikoworks-remove-wishlist-single-product -->
    <?php
}

// Remove all default wishlist hook
if ( class_exists( 'YITH_WCWL_Init' ) ) {
    add_action( 'woocommerce_single_product_summary', 'pikoworks_remove_wishlist_single_product_open', 31 );
    add_action( 'woocommerce_single_product_summary', 'pikoworks_remove_wishlist_single_product_close', 32 );
    
}
if ( class_exists( 'YITH_Woocompare_Frontend' ) ) {
    add_action( 'woocommerce_single_product_summary', 'pikoworks_remove_wishlist_single_product_open', 34 );
    add_action( 'woocommerce_single_product_summary', 'pikoworks_remove_wishlist_single_product_close', 36 );
    
}
function pikoworks_wc_wishlist_button($product) {
        $btn_wishlist = '';
        $btn_compare = '';
    
	if ( shortcode_exists('yith_wcwl_add_to_wishlist') ) {
		$btn_wishlist = do_shortcode( "[yith_wcwl_add_to_wishlist]" );
	}
	if ( shortcode_exists('yith_compare_button') ) {
                $btn_compare = do_shortcode('[yith_compare_button]'); 
	}
        ?>
    <div class="single-product-wrap">
        <?php
        echo do_shortcode( $btn_wishlist);
        echo do_shortcode($btn_compare);
        ?>
        <div class="clear"></div>   
    </div>
    <?php
        
        
}
//add_action( 'woocommerce_single_product_summary', 'pikoworks_wc_wishlist_button', 30 );

add_filter( 'woocommerce_breadcrumb_defaults', 'pikoworks_woo_breadcrumbs' );
function pikoworks_woo_breadcrumbs() {
    global $pikoworks;
    $breadcrubm_name =  isset( $pikoworks['optn_breadcrumb_name'] ) ? $pikoworks['optn_breadcrumb_name'] : esc_html__('Home', 'piko-construct');
    $breadcrubm_delimiter =  isset( $pikoworks['optn_breadcrumb_delimiter'] ) ? $pikoworks['optn_breadcrumb_delimiter'] : 'fa-angle-right';
//    filtaring breadcrumbs
    return array(
            'delimiter'   => '<i class="fa '. esc_attr($breadcrubm_delimiter) .'" aria-hidden="true"></i>   &nbsp;',
            'wrap_before' => '<nav class="woocommerce-breadcrumb" itemprop="breadcrumb">',
            'wrap_after'  => '</nav>',
            'before'      => '',
            'after'       => '',
            'home'        => esc_attr($breadcrubm_name),
        );
}

if( ! function_exists( 'pikoworks_get_max_date_sale') ) {
/**
 * Get max date sale variable 
 **/
    function pikoworks_get_max_date_sale( $product_id ) {
        $time = 0;
        // Get variations
        $args = array(
            'post_type'     => 'product_variation',
            'post_status'   => array( 'private', 'publish' ),
            'numberposts'   => -1,
            'orderby'       => 'menu_order',
            'order'         => 'asc',
            'post_parent'   => $product_id
        );
        $variations = get_posts( $args );
        $variation_ids = array();
        if( $variations ){
            foreach ( $variations as $variation ) {
                $variation_ids[]  = $variation->ID;
            }
        }
        $sale_price_dates_to = false;
    
        if( !empty(  $variation_ids )   ){
            global $wpdb;
            $sale_price_dates_to = $wpdb->get_var( "
                SELECT
                meta_value
                FROM $wpdb->postmeta
                WHERE meta_key = '_sale_price_dates_to' and post_id IN(" . join( ',', $variation_ids ) . ")
                ORDER BY meta_value DESC
                LIMIT 1
            " );
    
            if( $sale_price_dates_to != '' ){
                return $sale_price_dates_to;
            }
        }
    
        if( ! $sale_price_dates_to ){
            $sale_price_dates_to = get_post_meta( $product_id, '_sale_price_dates_to', true );

            if($sale_price_dates_to == ''){
                $sale_price_dates_to = '0';
            }

            return $sale_price_dates_to;
        }
    }
}



function pikoworks_product_share(){
global $pikoworks;
$social_share = isset($pikoworks['single_product_share_socials']) ? $pikoworks['single_product_share_socials']: array();
    
     if ( !empty( $social_share ) ): ?>       
                <?php if ( in_array( 'facebook', $social_share ) ): ?>                    
                    <li class="social fa fa-facebook">
                    <a class="shear-icon-wrap" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank">
                       <span class="text"><?php echo sprintf( esc_html__( 'Share "%s" on Facebook', 'piko-construct' ), get_the_title() ); ?></span>
                    </a>
                    </li>        
                <?php endif; ?>
                <?php if ( in_array( 'twitter', $social_share ) ): ?>
                <li class="social fa fa-twitter">
                    <a class="shear-icon-wrap" href="https://twitter.com/home?status=<?php the_permalink(); ?>" target="_blank">
                       <span class="text"><?php echo sprintf( esc_html__( 'Post status "%s" on Twitter', 'piko-construct' ), get_the_title() ); ?></span>
                    </a>
                </li>    
                <?php endif; ?>
                <?php if ( in_array( 'gplus', $social_share ) ): ?>
                <li class="social fa fa-google-plus">
                    <a class="shear-icon-wrap" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank">
                        <span class="text"><?php echo sprintf( esc_html__( 'Share "%s" on Google Plus', 'piko-construct' ), get_the_title() ); ?></span>
                    </a>
                </li>    
                <?php endif; ?>
                <?php if ( in_array( 'pinterest', $social_share ) ): ?>
                <li class="social fa fa-pinterest">
                    <a class="shear-icon-wrap" href="https://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&amp;description=<?php echo urlencode( get_the_excerpt() ); ?>" target="_blank">
                        <span class="text"><?php echo sprintf( esc_html__( 'Pin "%s" on Pinterest', 'piko-construct' ), get_the_title() ); ?></span>
                    </a>
                </li>    
                <?php endif; ?>
                <?php if ( in_array( 'linkedin', $social_share ) ): ?>
                    <li class="social fa fa-linkedin">
                    <a class="shear-icon-wrap" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>&amp;title=<?php echo urlencode( get_the_title() ); ?>&amp;summary=<?php echo urlencode( get_the_excerpt() ); ?>&amp;source=<?php echo urlencode( get_bloginfo( 'name' ) ); ?>" target="_blank">
                        <span class="text"><?php echo sprintf( esc_html__( 'Share "%s" on LinkedIn', 'piko-construct' ), get_the_title() ); ?></span>
                    </a>
                    </li>
                <?php endif; ?> 
            
    <?php endif; // End if ( !empty( $socials_shared ) )     
}

// Utilities
if( ! function_exists( 'pikoworks_single_product_print' ) ){
 /**
 * single product summary print button
 **/
    function pikoworks_single_product_print(){
        global $pikoworks;
        $enable_product_share = isset( $pikoworks['enable_product_single_post_share'] ) ? $pikoworks['enable_product_single_post_share'] == 1 : true;
        if ( $enable_product_share ):
        ?>
        <div class="print-button">
            <ul>
                <li>
                      <button class="fa fa-share-alt"></button>
                      <ul class="social-share">
                        <?php pikoworks_product_share(); ?>
                      </ul>
                </li>
            </ul>
        </div>
        <?php
        
    endif; //$enable_product_share
    }   
}
add_filter( 'woocommerce_single_product_summary', 'pikoworks_single_product_print', 51);


if( ! function_exists( 'pikoworks_related_products_args' ) ) {
/**
* Custom item related_products
**/
    function pikoworks_related_products_args( $args ) {
        $args['posts_per_page'] = 9; // 4 to 9 related product
        return $args;
    }
}
add_filter( 'woocommerce_output_related_products_args', 'pikoworks_related_products_args' );

if ( !function_exists( 'get' ) ){
	function get($var){
		return isset($_GET[$var]) ? $_GET[$var] : (isset($_REQUEST[$var]) ? $_REQUEST[$var] : '');
	}
}

if ( !function_exists( 'post' ) ){
	function post($var){
		return isset($_POST[$var]) ? $_POST[$var] : null;
	}
}

if ( !function_exists( 'cookie' ) ){
	function cookie($var){
		return isset($_COOKIE[$var]) ? $_COOKIE[$var] : null;
	}
}

if ( !function_exists( 'pikoworks_woocommerce_placeholder_img_src' ) ) {
	function pikoworks_woocommerce_placeholder_img_src($src){
		$src = get_template_directory_uri() . '/assets/images/placeholder.jpg';
		return esc_url($src);
	}
}


if ( !function_exists( 'pikoworks_woocommerce_add_filter_woocommerce_pagination_args' ) ) {
	function pikoworks_woocommerce_add_filter_woocommerce_pagination_args( $args )
	{
		$args[ 'prev_text' ] = esc_html__( 'Prev', 'piko-construct' );
		$args[ 'next_text' ] = esc_html__( 'Next', 'piko-construct' );
		return $args;
	}
}
if ( !function_exists( 'pikoworks_woocommerce_override_loop_shop_per_page' ) ) {
	function pikoworks_woocommerce_override_loop_shop_per_page( $cols )
	{
		$products_per_page = pikoworks_get_option_data( 'products_per_page', '9,15,30' );
		$mode_view = apply_filters( 'pikoworks_filter_products_mode_view', 'grid' );
		if ( $mode_view == 'list' ) {
			$products_per_page = pikoworks_get_option_data( 'products_per_page_list', '5,10,15' );
		}
		$array_per_page = explode( ',', $products_per_page );
		$array_per_page = array_map( 'trim', $array_per_page );
		$per_page = pikoworks_get_option_data( 'products_per_page_default', 9 );
		$per_page = apply_filters( 'pikoworks_filter_products_per_page', $per_page );
		if ( $per_page && in_array( $per_page, $array_per_page ) ) {
			return $per_page;
		}
		return $cols;
	}
}

if ( !function_exists( 'pikoworks_woocommerce_setcookie_default' ) ) {
	function pikoworks_woocommerce_setcookie_default()
	{
		$default_cookie_expire = time() + 3600 * 24 * 30;

		if ( !isset( $_COOKIE[ 'pikoworks_products_list_per_page' ] ) ) {
			setcookie(
				'pikoworks_products_list_per_page',
				pikoworks_get_option_data( 'products_per_page_list_default', 5 ),
				$default_cookie_expire,
				COOKIEPATH
			);
		}
		if ( !isset( $_COOKIE[ 'pikoworks_products_grid_per_page' ] ) ) {
			setcookie(
				'pikoworks_products_grid_per_page',
				pikoworks_get_option_data( 'products_per_page_default', 9 ),
				$default_cookie_expire,
				COOKIEPATH
			);
		}
		if ( !isset( $_COOKIE[ 'pikoworks_products_mode_view' ] ) ) {
			setcookie(
				'pikoworks_products_mode_view',
				'grid',
				$default_cookie_expire,
				COOKIEPATH
			);
		}

		// check mode_view
		if ( in_array( cookie( 'pikoworks_products_mode_view' ), array( 'list', 'grid' ) ) ) {
			add_filter(
				'pikoworks_filter_products_mode_view', function ( $per_row ) {
				return cookie( 'pikoworks_products_mode_view' );
			}, 99 );
		}
		if ( in_array( get( 'view' ), array( 'list', 'grid' ) ) ) {
			add_filter(
				'pikoworks_filter_products_mode_view', function ( $mode ) {
				return get( 'view' );
			}, 99 );
			setcookie(
				'pikoworks_products_mode_view',
				get( 'view' ),
				$default_cookie_expire,
				COOKIEPATH
			);
		}

		// Check per_row
		if ( absint( cookie( 'pikoworks_products_per_row' ) ) ) {
			add_filter(
				'pikoworks_filter_products_per_row', function ( $per_row ) {
				return absint( cookie( 'pikoworks_products_per_row' ) );
			}, 99 );
		}
		if ( absint( get( 'per_row' ) ) ) {
			add_filter(
				'pikoworks_filter_products_per_row', function ( $per_row ) {
				return absint( get( 'per_row' ) );
			}, 99 );
			setcookie(
				'pikoworks_products_per_row',
				absint( get( 'per_row' ) ),
				$default_cookie_expire,
				COOKIEPATH
			);
		}

		// check per_page
		$mode_view = in_array( cookie( 'pikoworks_products_mode_view' ), array( 'list', 'grid' ) ) ? cookie( 'pikoworks_products_mode_view' ) : 'grid';
		if ( in_array( get( 'view' ), array( 'list', 'grid' ) ) ) {
			$mode_view = get( 'view' );
		}

		if ( $mode_view == 'list' ) {
			if ( absint( cookie( 'pikoworks_products_list_per_page' ) ) ) {
				add_filter(
					'pikoworks_filter_products_per_page', function ( $per_row ) {
					return absint( cookie( 'pikoworks_products_list_per_page' ) );
				}, 99 );
			}
			if ( absint( get( 'per_page' ) ) ) {
				add_filter(
					'pikoworks_filter_products_per_page', function ( $per_page ) {
					return absint( get( 'per_page' ) );
				}, 99 );
				setcookie(
					'pikoworks_products_list_per_page',
					absint( get( 'per_page' ) ),
					$default_cookie_expire,
					COOKIEPATH
				);
			}
		} else {
			if ( absint( cookie( 'pikoworks_products_grid_per_page' ) ) ) {
				add_filter(
					'pikoworks_filter_products_per_page', function ( $per_row ) {
					return absint( cookie( 'pikoworks_products_grid_per_page' ) );
				}, 99 );
			}
			if ( absint( get( 'per_page' ) ) ) {
				add_filter(
					'pikoworks_filter_products_per_page', function ( $per_page ) {
					return absint( get( 'per_page' ) );
				}, 99 );
				setcookie(
					'pikoworks_products_grid_per_page',
					absint( get( 'per_page' ) ),
					$default_cookie_expire,
					COOKIEPATH
				);
			}
		}		
	}
}

if ( !function_exists( 'pikoworks_woocommerce_add_toolbar' ) ) {
	function pikoworks_woocommerce_add_toolbar()
	{
		wc_get_template( 'loop/toolbar.php' );
	}
}

if ( !function_exists( 'pikoworks_woocommerce_add_toolbar_per_page' ) ) {
	function pikoworks_woocommerce_add_toolbar_per_page()
	{
		$link = pikoworks_get_current_page_url();
		$mode_view = apply_filters( 'pikoworks_filter_products_mode_view', 'grid' );
		$products_per_page = pikoworks_get_option_data( 'products_per_page', '9,15,30' );
		$per_page = pikoworks_get_option_data( 'products_per_page_default', 9 );
		if ( $mode_view == 'list' ) {
			$products_per_page = pikoworks_get_option_data( 'products_per_page_list', '5,10,15' );
			$per_page = pikoworks_get_option_data( 'products_per_page_list_default', 5 );
		}
		$products_per_page = explode( ',', $products_per_page );
		$products_per_page = array_map( 'trim', $products_per_page );
		$per_page = apply_filters( 'pikoworks_filter_products_per_page', $per_page );
		if ( count( $products_per_page ) > 1 ) {
			?>
			<div class="sort-by-wrapper sort-by-per-page">
				<div class="sort-by-label"><?php esc_html_e( 'View', 'piko-construct' )?></div>
				<div class="sort-by-content">
					<ul>
						<?php foreach ( $products_per_page as $num ) : ?>
							<li<?php if ( $per_page == $num ) echo ' class="active"'; ?>>
								<a href="<?php echo esc_url(add_query_arg( 'per_page', $num, $link )) ?>"><?php echo esc_html( $num ); ?></a>
							</li>
						<?php endforeach;?>
					</ul>
				</div>
			</div>
		<?php
		}
	}
}

if ( !function_exists( 'pikoworks_woocommerce_add_toolbar_position' ) ) {
	function pikoworks_woocommerce_add_toolbar_position()
	{
		$link = pikoworks_get_current_page_url();
		?>
		<div class="sort-by-wrapper sort-by-order">
			<div class="sort-by-label"><?php esc_html_e( 'Position', 'piko-construct' )?></div>
			<div class="sort-by-content">
				<ul>
					<li<?php if ( strtolower( get( 'order' ) ) == 'asc' ) echo ' class="active"'; ?>>
						<a href="<?php echo esc_url(add_query_arg( 'order', 'asc', $link ))?>"><?php esc_html_e( 'Ascending', 'piko-construct' )?></a>
					</li>
					<li<?php if ( strtolower( get( 'order' ) ) == 'desc' ) echo ' class="active"'; ?>>
						<a href="<?php echo esc_url(add_query_arg( 'order', 'desc', $link ))?>"><?php esc_html_e( 'Descending', 'piko-construct' )?></a>
					</li>
				</ul>
			</div>
		</div>
	<?php
	}
}

if ( !function_exists( 'pikoworks_woocommerce_add_gridlist_toggle_button' ) ) {
	function pikoworks_woocommerce_add_gridlist_toggle_button()
	{
		global $wp;
		$mode_views = array(
			'grid' => array(
				esc_html__( 'Grid view', 'piko-construct' ),
				'<span><i class="fa fa-th-large"></i></span>'
			),
			'list' => array(
				esc_html__( 'List view', 'piko-construct' ),
				'<span><i class="fa fa-th-list"></i></span>'
			)
		);
		$active = apply_filters( 'pikoworks_filter_products_mode_view', 'grid' );
		$params = array();
		if ( isset( $_GET ) ) {
			foreach ( $_GET as $key => $val ) {
				$params[ $key ] = $val;
			}
		}
		if ( '' == get_option( 'permalink_structure' ) ) {
			$form_action = remove_query_arg( array( 'per_page', 'page', 'paged' ), add_query_arg( $wp->query_string, '', esc_url( home_url( trailingslashit( $wp->request ) ) )) );
		} else {
			$form_action = preg_replace( '%\/page/[0-9]+%', '', esc_url(home_url( trailingslashit( $wp->request ) )) );
		}
		?>
		<div class="gridlist-toggle-wrapper">
			<nav class="gridlist_toggle">
				<?php foreach ( $mode_views as $k => $v ) { ?>
                    <a id="<?php echo esc_attr( $k ); ?>" <?php
                        if ($active == $k) {
                            echo wp_kses('class="active" href="javascript:;"', array('a' => array('class' => array(), 'href' => array())));
                        } else {
                            echo wp_kses('href="' . esc_url(add_query_arg('view', $k, $form_action)) . '"', array('a' => array('href' => array())));
                        } ?>
					   title="<?php echo esc_attr( $v[ 0 ] ) ?>"><?php echo do_shortcode($v[ 1 ]) ?></a>
				<?php }?>
			</nav>
		</div>
	<?php
	}
}

if ( !function_exists( 'pikoworks_woocommerce_add_filter_attribute_on_toolbar' ) ) {
	function pikoworks_woocommerce_add_filter_attribute_on_toolbar()
	{
		ob_start();
		if ( is_active_sidebar( 'shop-filter' ) ) {
			dynamic_sidebar( 'shop-filter' );
		}
		$return = ob_get_clean();
		echo do_shortcode( $return );
	}
}


if ( !function_exists( 'pikoworks_woocommerce_add_filter_order_by_popularity_post_clauses' ) ) {
	function pikoworks_woocommerce_add_filter_order_by_popularity_post_clauses( $args )
	{
		global $wpdb;
		$order = in_array( strtoupper( get( 'order' ) ), array( 'DESC', 'ASC' ) ) ? strtoupper( get( 'order' ) ) : 'DESC';
		if(isset($args[ 'orderby' ])){
			if( strpos($args['orderby'], "$wpdb->postmeta.meta_value+0") !== FALSE ){
				$args[ 'orderby' ] = "$wpdb->postmeta.meta_value+0 $order, $wpdb->posts.post_date DESC";
			}
		}

		return $args;
	}
}

if ( !function_exists( 'pikoworks_woocommerce_add_filter_order_by_rating_post_clauses' ) ) {
	function pikoworks_woocommerce_add_filter_order_by_rating_post_clauses( $args )
	{
		global $wpdb;
		$order = in_array( strtoupper( get( 'order' ) ), array( 'DESC', 'ASC' ) ) ? strtoupper( get( 'order' ) ) : 'DESC';
		if(isset($args[ 'orderby' ])) {
			if( strpos($args['orderby'], "average_rating") !== FALSE ){
				$args['orderby'] = "average_rating $order, $wpdb->posts.post_date DESC";
			}
		}
		return $args;
	}
}


if ( !function_exists( 'pikoworks_woocommerce_custom_stock_html' ) ) {
	function pikoworks_woocommerce_custom_stock_html()
	{
		if ( is_product() ) {
			global $product;
			// Availability
			$availability = $product->get_availability();
			$availability_html = empty( $availability[ 'availability' ] ) ? '' : '<p class="stock ' . esc_attr( $availability[ 'class' ] ) . '"><span>' . esc_html__( 'Availability:', 'piko-construct' ) . '</span> ' . esc_html( $availability[ 'availability' ] ) . '</p>';
			echo do_shortcode( $availability_html);
		}
	}
}


if ( !function_exists( 'pikoworks_woocommerce_show_upsell_related_products' ) ) {
	function pikoworks_woocommerce_show_upsell_related_products()
	{
		if ( !pikoworks_get_option_data( 'related_products' ) ) {
			remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
		}
		if ( !pikoworks_get_option_data( 'upsell_products' ) ) {
			remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
		}
                
	}
}

if ( !function_exists( 'pikoworks_woocommerce_add_filter_product_tabs' ) ) {
	function pikoworks_woocommerce_add_filter_product_tabs( $tabs )
	{
		global $product, $post;
                
                $custom_html_enable =  get_post_meta(get_the_ID(), 'pikoworks_enable_custom_tab_html',true);
                if (!isset($custom_html_enable) || $custom_html_enable == '-1' || $custom_html_enable == '') {
                    $custom_html_enable = pikoworks_get_option_data( 'custom_tab' );
                }
                $custom_tab_heading =  get_post_meta(get_the_ID(), 'pikoworks_product_custom_tab_heading',true);
                if (!isset($custom_tab_heading) || $custom_tab_heading == '-1' || $custom_tab_heading == '') {
                    $custom_tab_heading = pikoworks_get_option_data( 'custom_tab_title', esc_html__('Custom Tab', 'piko-construct') );
                }
                
		if ( $custom_html_enable ) {
			$tabs[ 'custom_tab' ] = array(
				'title'    => $custom_tab_heading,
				'priority' => 50,
				'callback' => 'pikoworks_woocommerce_add_custom_product_tab_callback'
			);
		}
		if ( !pikoworks_get_option_data( 'review_tab' ) ) {
			if ( isset( $tabs[ 'reviews' ] ) ) {
				unset( $tabs[ 'reviews' ] );
			}
		}
		return $tabs;
	}
}

if ( !function_exists( 'pikoworks_woocommerce_add_custom_product_tab_callback' ) ) {
	function pikoworks_woocommerce_add_custom_product_tab_callback()
	{
            $custom_html_tab =  get_post_meta(get_the_ID(), 'pikoworks_product_custom_tab_content',true);
            if (!isset($custom_html_tab) || $custom_html_tab == '-1' || $custom_html_tab == '') {
                $custom_html_tab = pikoworks_get_option_data( 'custom_tab_content' );
            }
		echo pikoworks_add_formatting( $custom_html_tab );
	}
}
if ( !function_exists( 'pikoworks_woocommerce_init_sortable_taxonomies_brand' ) ) {
	add_filter('woocommerce_sortable_taxonomies','pikoworks_woocommerce_init_sortable_taxonomies_brand');
	function pikoworks_woocommerce_init_sortable_taxonomies_brand( $return )
	{
		global $current_screen;
		$return[ ] = 'product_brand';
		if ( is_object( $current_screen ) && in_array( $current_screen->id, array( 'edit-product_brand' ) ) ) {
			wp_enqueue_media();
		}
		return $return;
	}
}

function pikoworks_remove_woocommerce_category_products_count() {
  return;
}
add_filter( 'woocommerce_subcategory_count_html', 'pikoworks_remove_woocommerce_category_products_count' );