<?php
function pikoworks_toolbar_col_before(){
    echo '<div class="col-sm-6 col-md-5">';
}
function pikoworks_toolbar_col_after(){
    echo '</div>';
}
function pikoworks_toolbar_col2_before(){
    echo '<div class="col-sm-6 col-md-7 text-right">';
}
function pikoworks_toolbar_col2_after(){
    echo '</div>';
}

function pikoworks_toolbar_figure_before(){
    echo '<figure>';
}
function pikoworks_toolbar_figure_after(){
    echo '</figure>';
}

/* 
 * woocommerce hooks
 */
add_action( 'init', 'pikoworks_woocommerce_setcookie_default' );

add_filter( 'loop_shop_per_page', 'pikoworks_woocommerce_override_loop_shop_per_page', 20 );
add_filter( 'woocommerce_placeholder_img_src', 'pikoworks_woocommerce_placeholder_img_src' );

add_action( 'woocommerce_before_shop_loop_item_title', 'pikoworks_woocommerce_add_badge_new_in_list', 8 );

add_action( 'pikoworks_woocommerce_toolbar', 'pikoworks_toolbar_col_before',10);
add_action( 'pikoworks_woocommerce_toolbar', 'pikoworks_woocommerce_add_gridlist_toggle_button', 20 );
add_action( 'pikoworks_woocommerce_toolbar', 'pikoworks_woocommerce_add_toolbar_per_page', 30 );
add_action( 'pikoworks_woocommerce_toolbar', 'pikoworks_woocommerce_add_toolbar_position', 40 );

add_action( 'pikoworks_woocommerce_toolbar', 'pikoworks_toolbar_col_after',60);
add_action( 'pikoworks_woocommerce_toolbar', 'pikoworks_toolbar_col2_before',70);
add_action( 'pikoworks_woocommerce_toolbar', 'pikoworks_woocommerce_add_filter_attribute_on_toolbar', 80 );
add_action( 'pikoworks_woocommerce_toolbar', 'woocommerce_result_count', 90 );
add_action( 'pikoworks_woocommerce_toolbar', 'woocommerce_catalog_ordering', 99 );
add_action( 'pikoworks_woocommerce_toolbar', 'pikoworks_toolbar_col2_after',100);


add_action( 'woocommerce_before_shop_loop', 'pikoworks_woocommerce_add_toolbar' );

//archive product
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 ); 
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 ); 

add_action( 'woocommerce_before_shop_loop_item_title', 'pikoworks_toolbar_figure_before',10);
add_action( 'woocommerce_before_shop_loop_item_title', 'pikoworks_wc_template_loop_product_thumbnail', 12 );
add_action( 'woocommerce_before_shop_loop_item_title', 'pikoworks_wc_template_loop_product_action', 12,1 );
add_action( 'woocommerce_before_shop_loop_item_title', 'pikoworks_wc_template_loop_product_coundown', 12,2 );
add_action( 'woocommerce_before_shop_loop_item_title', 'pikoworks_toolbar_figure_after',14);

add_action('woocommerce_shop_loop_item_title', 'pikoworks_wc_shop_loop_item_title', 10);
add_action( 'woocommerce_before_shop_loop_item_title', 'pikoworks_woocommerce_add_badge_new_in_list', 8 );
add_action( 'woocommerce_before_shop_loop_item_title', 'pikoworks_woocommerce_add_badge_out_of_stock', 9 );


//single product
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );

add_action( 'woocommerce_before_single_product_summary', 'pikoworks_woocommerce_add_badge_new_in_list', 8 );
add_action( 'woocommerce_before_single_product_summary', 'pikoworks_woocommerce_add_badge_out_of_stock', 9 );
add_action( 'woocommerce_before_single_product_summary', 'pikoworks_wc_template_single_product_thumbnail', 20 );
add_action( 'woocommerce_single_product_summary', 'pikoworks_wc_template_loop_product_coundown', 39 );

remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );// loop unwanted link remove
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 ); //loop unwanted link remove

// Remove hooks of YITH plugins
if ( class_exists( 'YITH_WCQV_Frontend' ) ) {
    remove_action( 'woocommerce_after_shop_loop_item', array( YITH_WCQV_Frontend::get_instance(), 'yith_add_quick_view_button' ), 15 );   
}

add_filter( 'woocommerce_product_tabs', 'pikoworks_woocommerce_add_filter_product_tabs' );

