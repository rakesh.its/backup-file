<?php

if(!function_exists('pikoworks_widgets_init')){
    

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 */
    function pikoworks_widgets_init() {
            register_sidebar( array(
                    'name'          => esc_html__( 'Sidebar', 'piko-construct' ),
                    'id'            => 'sidebar-1',
                    'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'piko-construct' ),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
            ) );
            register_sidebar( array(
                    'name'          => esc_html__( 'Page Sidebar', 'piko-construct' ),
                    'id'            => 'sidebar-2',
                    'description'   => esc_html__( 'Add widgets here to appear in your page sidebar.', 'piko-construct' ),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
            ) );
            register_sidebar( array(
                    'name'          => esc_html__( 'Sub Footer Sidebar', 'piko-construct' ),
                    'id'            => 'sidebar-3',
                    'description'   => esc_html__( 'Add widgets here to appear in your sub footer.', 'piko-construct' ),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
            ) );
            register_sidebar( array(
                    'name'          => esc_html__( 'Sub Footer Sidebar two', 'piko-construct' ),
                    'id'            => 'sidebar-5',
                    'description'   => esc_html__( 'Add widgets here to appear in your sub footer Two.', 'piko-construct' ),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
            ) );
            register_sidebar( array(
                    'name'          => esc_html__( 'Service Sidebar', 'piko-construct' ),
                    'id'            => 'sidebar-6',
                    'description'   => esc_html__( 'Add widgets here to appear in your service page sidebar.', 'piko-construct' ),
                    'before_widget' => '<section id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</section>',
                    'before_title'  => '<h2 class="widget-title">',
                    'after_title'   => '</h2>',
            ) );
            if ( class_exists( 'WooCommerce' ) ):
                register_sidebar( array(
                        'name'          => esc_html__( 'Shop Sidebar', 'piko-construct' ),
                        'id'            => 'sidebar-4',
                        'description'   => esc_html__( 'Add widgets here to appear in your shop page sidebar.', 'piko-construct' ),
                        'before_widget' => '<section id="%1$s" class="widget %2$s">',
                        'after_widget'  => '</section>',
                        'before_title'  => '<h2 class="widget-title">',
                        'after_title'   => '</h2>',
                ) );
            endif;


            $theme_mods = get_theme_mods();
            if(is_array($theme_mods) && array_key_exists('redux-widget-areas', $theme_mods)){
                $sidebar = $theme_mods['redux-widget-areas'];
                foreach ($sidebar as $name){
                    register_sidebar( array(
                        'name'          => $name,
                        'id'            => str_replace(' ','',strtolower ($name)),
                        'description'   =>  $name,
                        'before_widget' => '<section id="%1$s" class="widget %2$s">',
                        'after_widget'  => '</section>',
                        'before_title'  => '<h2 class="widget-title">',
                        'after_title'   => '</h2>',
                    ) );
                }
            }

    }
    add_action( 'widgets_init', 'pikoworks_widgets_init' );
}

if (!function_exists('pikoworks_redux_custom_widget_area_filter')) {
    function pikoworks_redux_custom_widget_area_filter($arg) {
        return array(
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        );
    }
    add_filter('redux_custom_widget_args','pikoworks_redux_custom_widget_area_filter');
}