<?php
/**
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 */

//search form
if(!function_exists('pikoworks_search_form')){
    function pikoworks_search_form( $form ){        
        $key = get_search_query();        
        $form = '<form class="search-form" method="get"  action="' . esc_url( home_url( '/' ) ) .'">
                <input type="search" name="s" placeholder="' . esc_html__( 'Search here...', 'piko-construct' ) . '" value="' . esc_attr( $key ) . '" class="search-field">
                <button type="submit" class="search-submit"><i class="lnr lnr-arrow-right"  aria-hidden="true"></i></button>
            </form>';

        return $form;
    }
   add_filter('get_search_form', 'pikoworks_search_form');
}


if ( ! function_exists( 'pikoworks_entry_meta' ) ) :
/**
 * Prints HTML with meta information for the categories, tags.
 *
 * Create your own marn_entry_meta() function to override in a child theme.
 *
 */
function pikoworks_entry_meta() {	

	$format = get_post_format();
	if ( current_theme_supports( 'post-formats', $format ) ) {
		printf( '<span class="entry-format">%1$s<a href="%2$s">%3$s</a></span>',
			sprintf( '<span class="screen-reader-text">%s </span>', _x( 'Format', 'Used before post format.', 'piko-construct' ) ),
			esc_url( get_post_format_link( $format ) ),
			get_post_format_string( $format )
		);
	}
        if ( is_sticky() && is_home() && ! is_paged() ) {
    		printf( '<span class="sticky-post">%s</span>', esc_html__( 'Featured', 'piko-construct' ) );
    	}
	if ( 'post' === get_post_type() ) {
		pikoworks_entry_taxonomies();
	}

	if ( ! is_singular() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		comments_popup_link( sprintf( esc_html__( 'Leave a comment<span class="screen-reader-text"> on %s</span>', 'piko-construct' ), get_the_title() ) );
		echo '</span>';
	}
}
endif;


if( !function_exists('pikoworks_blog_meta')){
    /* *
     * blog meta
     * */
    function pikoworks_blog_meta(){
        $metaTag = pikoworks_get_option_data('optn_blog_post_metatag', array('data', 'author', 'category','comment', 'format_single'));
        if ( !empty( $metaTag ) ):
        ?>
    <ul class="title-blog-meta">
        <?php if ( in_array( 'data', $metaTag ) ): ?> 
        <li><?php if( !is_single()){echo get_the_date('Y'); } else{ echo pikoworks_entry_date(); }  ?></li>
        <?php endif; ?>
        <?php if ( in_array( 'author', $metaTag ) ): ?> 
        <li><?php esc_html_e('By: ', 'piko-construct' ); ?><a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php the_author(); ?></a></li>       
        <?php endif; ?>         
        <?php
        if ( in_array( 'category', $metaTag ) ):
            if ( 'post' === get_post_type() && !is_singular() ) {

                $categories_list = get_the_category_list( esc_html__( ', ', 'piko-construct' ) );
                if ( $categories_list && pikoworks_categorized_blog() ) {
                        printf( '<li>' . esc_html__( 'Posted in: %1$s', 'piko-construct' ) . '</li>', $categories_list);
                }           
            }
        endif; //category
        $format = get_post_format() ? get_post_format() : esc_html__('Standard', 'piko-construct');
        $format_link = get_post_format_link($format);
        if(is_singular() && in_array( 'format_single', $metaTag ) || ! is_singular() && in_array( 'format', $metaTag ) ){
            echo '<li>'. esc_html__('Post: ', 'piko-construct' ).'<a href="'. esc_url($format_link) .'">' .esc_attr($format) . '</a> </li>';
        }
        
        if ( in_array( 'comment', $metaTag ) ):
            if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
                echo '<li>';
                echo comments_popup_link( esc_html__( 'To Write First', 'piko-construct' ), esc_html__( '1 Comment', 'piko-construct' ), esc_html__( '% Comments', 'piko-construct' ) );
                echo '</li>';
            } 
        endif; ?>
    </ul>
<?php
    endif; // metatag
    }
}
//blog meta grid
if( !function_exists('pikoworks_grid_blog_meta')){
    function pikoworks_grid_blog_meta(){
         $metaTag = pikoworks_get_option_data('optn_blog_post_metatag', array('data', 'author', 'category','comment', 'format_single'));
          if ( !empty( $metaTag ) ):
        ?>
    <div class="blog-title">
        <ul class="title-blog-meta">
            <?php if ( is_sticky() && is_home() && ! is_paged() ) {
                    printf( '<li><i class="fa fa-star"></i>%s</li>', esc_html__( 'Featured', 'piko-construct' ) );
            } ?>
            <?php if ( in_array( 'author', $metaTag ) ): ?> 
            <li><?php esc_html_e('By', 'piko-construct'); ?> <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php the_author(); ?></a></li>
            <?php endif; ?>
            <?php if ( in_array( 'data', $metaTag ) ): ?> 
            <li><?php echo pikoworks_entry_date(); ?></li>
             <?php endif; ?>
             <?php if ( in_array( 'comment', $metaTag ) ): ?> 
            <li><i class="fa fa-comments-o"> </i> <?php echo comments_popup_link( esc_html__( '0', 'piko-construct' ), esc_html__( '1', 'piko-construct' ), esc_html__( '% Comments', 'piko-construct' ) ); ?></li>               
            <?php endif; ?>
        </ul>
    </div>
    <?php
 endif;
    }
}

if( !function_exists('pikoworks_blog_shortcode_meta')){
    function pikoworks_blog_shortcode_meta(){
        ?>
    <div class="blog-date">
        <p>
            <span><?php  echo get_the_date('M'); ?></span>
            <span><?php  echo get_the_date('y'); ?></span>
        </p>
    </div>
<?php
    }
}




if ( ! function_exists( 'pikoworks_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags.
 *
 * Create your own pikoworks_entry_meta() function to override in a child theme.
 *
 */
function pikoworks_entry_footer() {
        $metaTag = pikoworks_get_option_data('optn_blog_post_metatag', array('category_single','tags_single'));
        
	if ( 'post' === get_post_type() ) {
            
            $categories_list = get_the_category_list( esc_html__( ', ', 'piko-construct' ) );
            if ( $categories_list && in_array( 'category_single', $metaTag )) {
                    printf( '<div class="cat-links">' . esc_html__( 'Posted in:  %1$s', 'piko-construct' ) . '</div>', $categories_list);
            }
            $tags_list = get_the_tag_list( '', esc_html__( ' ', 'piko-construct' ) );
            if ( $tags_list && in_array( 'tags_single', $metaTag )) {
                    printf( ' <br /><div class="tags-links">' . esc_html__( '%1$s', 'piko-construct' ) .'</div>',	$tags_list);
            }
	}	
}
endif;



if ( ! function_exists( 'pikoworks_entry_date' ) ) :
/**
 * Prints HTML with date information for current post.
 *
 * Create your own pikoworks_entry_date() function to override in a child theme.
 *
 */
function pikoworks_entry_date() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" style="display:none" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		get_the_date(),
		esc_attr( get_the_modified_date( 'c' ) ),
		get_the_modified_date()
	);

	printf( '<span class="posted-on"><span class="screen-reader-text">%1$s </span><a href="%2$s" rel="bookmark">%3$s</a></span>',
		esc_html(_x( 'Posted on', 'Used before publish date.', 'piko-construct' )),
		esc_url( get_permalink() ),
		$time_string
	);
}
endif;

if ( ! function_exists( 'pikoworks_entry_taxonomies' ) ) :
/**
 * Prints HTML with category and tags for current post.
 *
 * Create your own pikoworks_entry_taxonomies() function to override in a child theme.
 *
 */
function pikoworks_entry_taxonomies() {
	$categories_list = get_the_category_list( esc_html__( ', ', 'piko-construct' ) );
	if ( $categories_list && pikoworks_categorized_blog() ) {
		printf( '<div class="cat-links">' . esc_html__( 'Posted in %1$s', 'piko-construct' ) . '</div>', $categories_list);
	}
	$tags_list = get_the_tag_list( '', esc_html__( ' ', 'piko-construct' ) );
	if ( $tags_list ) {
		printf( '<br /><div class="tags-links">' . esc_html__( '%1$s', 'piko-construct' ) .'</div>',	$tags_list);
	}
}
endif;

if(!function_exists('pikoworks_entry_header')):
 /**
 * entry header tile date meta.
 */
    function pikoworks_entry_header(){
    
    $sicky_post = $link_icon = '';
    if(has_post_format( 'link' )){
        $link_icon = '<i class="title-icon fa fa-link"><span>'. esc_html__('Link', 'piko-construct') .'</span></i>';
      }elseif(has_post_format( 'quote' )){
        $link_icon = '<i class="title-icon fa fa-quote-left"><span>'. esc_html__('Quote', 'piko-construct') .'</span></i>';
      }   
    if ( is_sticky() && is_home() && ! is_paged() ) {
        $sicky_post =  '<i>'. esc_html__( 'Featured', 'piko-construct' ) .'</i>';
    } ?> 

    <div class="blog-title-wrap">
        <div class="blog-title">
             <?php 
             if ( is_singular() ){
                    the_title( '<h2 class="entry-title">',  do_shortcode($sicky_post . $link_icon) . '</h2>' );
             }else{
                the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a> '. do_shortcode($sicky_post . $link_icon) .'</h2>' );  
             }             
             ?>
             <?php pikoworks_blog_meta(); ?>
        </div>
    </div>
    <div class="clearfix"></div>
   <?php     
    }
endif; //pikoworks_entry_header

if(!function_exists('pikoworks_entry_header_two')):
 /**
 * entry header tile date meta.
 */
    function pikoworks_entry_header_two(){
    $sicky_post = $link_icon = '';
    
    if(has_post_format( 'link' )){
        $link_icon = '<i class="title-icon fa fa-link"><span>'. esc_html__('Link', 'piko-construct') .'</span></i>';
      }elseif(has_post_format( 'quote' )){
        $link_icon = '<i class="title-icon fa fa-quote-left"><span>'. esc_html__('Quote', 'piko-construct') .'</span></i>';
      }
      
    if ( is_sticky() && is_home() && ! is_paged() ) {
        $sicky_post =  '<i>'. esc_html__( 'Featured', 'piko-construct' ) .'</i>';
    }
      
    ?>    
    <div class="blog-title-wrap two">        
        <div class="blog-title">
             <?php
             
             if ( is_singular() ){
                 the_title( '<h2 class="entry-title">',  do_shortcode($sicky_post . $link_icon) . '</h2>' );
             }else{
                the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a> '. do_shortcode($sicky_post . $link_icon) .'</h2>' );  
             } 
             ?>
             
             <?php 
             if ( 'post' == get_post_type() ) {                
                pikoworks_blog_meta();                
             }             
            ?>
        </div>
    </div>
    <div class="clearfix"></div>
   <?php     
    }
endif; //pikoworks_entry_header



if ( !function_exists( 'pikoworks_read_more_link' ) ) {
    function pikoworks_read_more_link() {
        global $pikoworks;


        if ( get_post_type( get_the_ID() ) == 'post' ) {
            $read_more_text = pikoworks_get_option_data('opt_blog_continue_reading', esc_html__( 'Read more', 'piko-construct' ));
            $btn_style = pikoworks_get_option_data('opt_blog_btn_style', 'advance');
            $btn_effect = pikoworks_get_option_data('opt_blog_btn_hover_effect', '4');
            $btn_size = pikoworks_get_option_data('opt_blog_btn_size', 'medium');
            $btn_color = isset( $pikoworks['opt_blog_btn_color'][0] ) ?  $pikoworks['opt_blog_btn_color'][0] : '';
            $btn_center = isset( $pikoworks['opt_blog_btn_color'][1] ) ?  $pikoworks['opt_blog_btn_color'][1] : '';
        }elseif( get_post_type( get_the_ID() ) == 'service' ) {
            $read_more_text = pikoworks_get_option_data('opt_service_continue_reading', esc_html__( 'Read more', 'piko-construct' ));
            $btn_style = pikoworks_get_option_data('opt_service_btn_style', 'advance');
            $btn_effect = pikoworks_get_option_data('opt_service_btn_hover_effect', '4');
            $btn_size = pikoworks_get_option_data('opt_service_btn_size', 'medium');
            $btn_color = isset( $pikoworks['opt_service_btn_color'][0] ) ?  $pikoworks['opt_service_btn_color'][0] : '';
            $btn_center = isset( $pikoworks['opt_service_btn_color'][1] ) ?  $pikoworks['opt_service_btn_color'][1] : '';
        }
        $btn10_after = $btn89_before = $btn89_after = '';
            if( $btn_effect == '8'|| $btn_effect == '9'){
                $btn89_before = '<i class="shuffle">';
                $btn89_after = '</i>';
            }    
            if($btn_effect == '10'){
                $btn10_after = '<div class="aware"></div>';
            }
        
        $read_more_html = '<a href="' . esc_url( get_permalink()) . '" title="' . get_the_title() . '" class="piko-btn-'. $btn_effect.'" ><span>' .$btn89_before . esc_attr( $read_more_text ) .$btn89_after . '</span>' . $btn10_after . '</a>';            

        
        if($btn_style == 'default'){
            return '<div class="btnwrap ' . esc_attr( $btn_size . ' ' . $btn_color . ' ' . $btn_center ) . '">' . $read_more_html . '</div>';
        }else{
           return '<a href="' . esc_url( get_permalink()) . '" class="btn_read_more">' . esc_attr($read_more_text) . '<span class="screen-reader-text">' . get_the_title() . '</span></a>'; 
        }
    }
    add_filter( 'the_content_more_link', 'pikoworks_read_more_link' );
}
if(!function_exists('pikoworks_trim_word')){
 /**
 * pikoworks trim word
 */
    function pikoworks_trim_word($charlength){
        $content = get_the_content(); // Get post content in $content and clening vc shortcode
        $content = preg_replace("/\[(.*?)\]/i", '', $content);
        $content = strip_tags($content);
        
        echo wp_trim_words($content, $charlength, ' ');
        
    }
}

if(!function_exists('pikoworks_remove_vc_from_excerpt'))  {
  function pikoworks_remove_vc_from_excerpt( $excerpt ) {
    $patterns = "/\[[\/]?vc_[^\]]*\]/";
    $replacements = "";
    return preg_replace($patterns, $replacements, $excerpt);
  }
}
if(!function_exists('pikoworks_trim_word_vc')) {
  function pikoworks_trim_word_vc($charlength) { 
    global $word_count, $post; 
    $word_count = isset($word_count) && $word_count != "" ? $word_count : $charlength; 
    $post_excerpt = $post->post_excerpt != "" ? $post->post_excerpt : strip_tags($post->post_content); $clean_excerpt = strpos($post_excerpt, '...') ? strstr($post_excerpt, '...', true) : $post_excerpt;
    $clean_excerpt = strip_shortcodes(pikoworks_remove_vc_from_excerpt($clean_excerpt));
    $excerpt_word_array = explode (' ',$clean_excerpt);
    $excerpt_word_array = array_slice ($excerpt_word_array, 0, $word_count); 
    $excerpt = implode (' ', $excerpt_word_array).'...'; echo ''.$excerpt.''; 
  } 
}



if ( ! function_exists( 'pikoworks_excerpt' ) ) :
	/**
	 * Displays the optional excerpt.
	 *
	 * Wraps the excerpt in a div element
	 *
	 * @param string $class Optional. Class string of the div element. Defaults to 'entry-summary'.
	 */
	function pikoworks_excerpt( $class = 'entry-summary' ) {
		$class =  $class;

		if ( has_excerpt() || is_search() ) : ?>
			<div class="<?php echo esc_attr( $class); ?>">
				<?php the_excerpt(); ?>
			</div>
		<?php endif;
	}
endif;
if ( ! function_exists( 'pikoworks_excerpt_more' ) && ! is_admin() ) :
/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * Create your own pikoworks_excerpt_more() function to override in a child theme.
 *
 *
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function pikoworks_excerpt_more() {
        global $pikoworks;
        $read_more_text = isset( $pikoworks['opt_blog_continue_reading'] ) ? sanitize_text_field( $pikoworks['opt_blog_continue_reading'] ) : esc_html__( 'Read more', 'piko-construct' );        
	$link = sprintf( '<a href="%1$s" class="more-link">%2$s</a>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf(esc_attr($read_more_text), '' )
	);
	return $link;
}
add_filter( 'excerpt_more', 'pikoworks_excerpt_more' );
endif;



if ( !function_exists( 'pikoworks_excerpt_max_charlength' ) ) {
    
    function pikoworks_excerpt_max_charlength( $charlength ) {
    	$excerpt = get_the_excerpt();
    	$charlength++;
        
        if ( mb_strlen( $excerpt ) <= $charlength ) {
            $excerpt = strip_tags( get_the_content() );
        }
    
    	if ( mb_strlen( $excerpt ) > $charlength ) {
    		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
    		$exwords = explode( ' ', $subex );
    		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
    		if ( $excut < 0 ) {
    			$subex = mb_substr( $subex, 0, $excut );
    		} 
    		$subex .= '...';
            $excerpt = $subex;
    	} 
        
        return $excerpt;
    }   
    
}


/**
 * Determines whether blog/site has more than one category.
 *
 * Create your own pikoworks_categorized_blog() function to override in a child theme.
 *
 * @return bool True if there is more than one category, false otherwise.
 */
function pikoworks_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'pikoworks_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'pikoworks_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so pikoworks_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so pikoworks_categorized_blog should return false.
		return false;
	}
}

if ( !function_exists( 'pikoworks_before_loop_posts_wrap' ) ) {
    
    function pikoworks_before_loop_posts_wrap() {
        global $pikoworks;
        
        $archive_layout_style = isset( $pikoworks['optn_archive_display_type'] ) ? trim( $pikoworks['optn_archive_display_type'] ) : 'grid';
        $masonry = '';
        if($archive_layout_style == 'masonry'){
            $masonry = "data-masonry='{ \"itemSelector\": \".grid-item\", \"columnWidth\": 200 }'";
        }
        
        echo '<div class="posts-wrap posts-' . esc_attr( $archive_layout_style ) . '">'; 
        
        
    }
    add_action( 'pikoworks_before_loop_posts', 'pikoworks_before_loop_posts_wrap', 10 );
}

if ( !function_exists( 'pikoworks_after_loop_posts_wrap' ) ) {
    
    function pikoworks_after_loop_posts_wrap() {
        global $pikoworks;
        
        $archive_layout_style = isset( $pikoworks['optn_archive_display_type'] ) ? trim( $pikoworks['optn_archive_display_type'] ) : '';
        
        echo '</div><!-- /.posts-wrap .posts-' . esc_attr( $archive_layout_style ) . ' -->';
        
    }
    add_action( 'pikoworks_after_loop_posts', 'pikoworks_after_loop_posts_wrap', 10 );
}



/**
 * Flushes out the transients used in pikoworks_categorized_blog().
 *
 * @since pikoworks
 */
function pikoworks_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'pikoworks_categories' );
}
add_action( 'edit_category', 'pikoworks_category_transient_flusher' );
add_action( 'save_post',     'pikoworks_category_transient_flusher' );
