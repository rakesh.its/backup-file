<?php 
/* 
 * preloader html
 */

if(!function_exists('pikoworks_enable_loader')){
    function pikoworks_enable_loader(){ 
        $enable_loader = pikoworks_get_option_data('optn_enable_loader', false);        
        if($enable_loader == 1){
            pikoworks_get_template('site-loading');
        }       
    }
}