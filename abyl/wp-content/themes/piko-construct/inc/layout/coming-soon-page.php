<?php
/*
 *@Author themepiko
 *@Coming soon redirect
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

//coming soon mode
if ( !function_exists( 'pikoworks_coming_soon_redirect' ) ) {
    function pikoworks_coming_soon_redirect() {
        global $pikoworks;
        
        $is_coming_soon_mode = pikoworks_get_option_data('enable_coming_soon_mode', false);   
        $disable_if_date_smaller_than_current = pikoworks_get_option_data('disable_coming_soon_when_date_small', false);
        $coming_date = pikoworks_get_option_data('coming_soon_date', ''); 
        
        $today = date( 'm/d/Y' );
        
        if ( trim( $coming_date ) == '' || strtotime( $coming_date ) <= strtotime( $today ) ) {
            if ( $disable_if_date_smaller_than_current ) {
                $is_coming_soon_mode = false;
            }
        }
        
        // Dont't show coming soon page if not coming soon mode on or  is user logged in.
        if ( is_user_logged_in() || !$is_coming_soon_mode ) {
            return;
        }
        
        pikoworks_coming_soon_template(); // Locate in theme/lib/coming-soon-page.php
        
        exit();
    }
    add_action( 'template_redirect', 'pikoworks_coming_soon_redirect' );
}

if ( !function_exists( 'pikoworks_coming_soon_mode_admin_toolbar' ) ) {
    // Add Toolbar Menus
    function pikoworks_coming_soon_mode_admin_toolbar() {
    	global $wp_admin_bar, $pikoworks;
        $is_coming_soon_mode = pikoworks_get_option_data('enable_coming_soon_mode', false);     
        $disable_if_date_smaller_than_current = pikoworks_get_option_data('disable_coming_soon_when_date_small', false);     
        $coming_date = pikoworks_get_option_data('coming_soon_date', ''); 
        
        $today = date( 'm/d/Y' ); 
        
        if ( trim( $coming_date ) == '' || strtotime( $coming_date ) <= strtotime( $today ) ) {
            if ( $disable_if_date_smaller_than_current && $is_coming_soon_mode ) {
                $is_coming_soon_mode = false;
                $menu_item_class = 'piko_coming_soon_expired';
                if ( current_user_can( 'administrator' ) ) { // Coming soon date expired
                    
                    $date = isset( $pikoworks['coming_soon_date'] ) ? $pikoworks['coming_soon_date'] : date();
                    
                    $args = array(
                		'id'     => 'piko_coming_soon',
                		'parent' => 'top-secondary',
                		'title'  => esc_html__( 'Coming Soon Mode Expired', 'piko-construct' ),
                		'href'   => esc_url( admin_url( 'themes.php?page=theme_options' ) ),
                		'meta'   => array(
                			'class'          => 'piko_coming_soon_expired',
                			'title'          => esc_html__( 'Coming soon mode is actived but expired', 'piko-construct' )
                		),
                	);
                	$wp_admin_bar->add_menu( $args );   
                }
            }
        }        
        
        if ( current_user_can( 'administrator' ) && $is_coming_soon_mode ) {
            
            $date = isset( $pikoworks['coming_soon_date'] ) ? $pikoworks['coming_soon_date'] : date();
            
            $args = array(
        		'id'     => 'piko_coming_soon',
        		'parent' => 'top-secondary',
        		'title'  => esc_html__( 'Coming Soon Mode', 'piko-construct' ),
        		'href'   => esc_url( admin_url( 'themes.php?page=theme_options' ) ),
        		'meta'   => array(
        			'class'          => 'coming_soon piko-countdown-wrap countdown-admin-menu piko-cms-date_' . esc_attr( $date ),
        			'title'          => esc_html( 'Showing date '.esc_attr( $date ).' Coming soon ended')
        		),
        	);
        	$wp_admin_bar->add_menu( $args );   
        }
    
    }
    add_action( 'wp_before_admin_bar_render', 'pikoworks_coming_soon_mode_admin_toolbar', 999 );
}



if ( !function_exists( 'pikoworks_coming_soon_template' ) ) {
    
    function pikoworks_coming_soon_template() {
        global $pikoworks;
        
        $title = isset( $pikoworks['coming_soon_site_title'] ) ? $pikoworks['coming_soon_site_title'] : '';
        $date = pikoworks_get_option_data('coming_soon_date', date());         
        $text = isset( $pikoworks['coming_soon_text'] ) ? wpautop( $pikoworks['coming_soon_text'] ) : '';
        $img = isset( $pikoworks['coming_soon_logo'] ) ? $pikoworks['coming_soon_logo'] : array( 'url' => get_template_directory_uri() . '/assets/images/logo/logo@2x.png' );
        $show_subscribe_form = isset( $pikoworks['enable_coming_soon_newsletter'] ) ? $pikoworks['enable_coming_soon_newsletter'] != 0 : false;
        $social_icon = isset( $pikoworks['enable_coming_soon_social'] ) ? $pikoworks['enable_coming_soon_social'] : 0;
        $coming_page_layout = isset( $pikoworks['coming_page_layout'] ) ? $pikoworks['coming_page_layout'] : 0;
        
        //mailchimp Subscribe form information
        $mailchimp_api_key = isset( $pikoworks['mailchimp_api_key'] ) ? $pikoworks['mailchimp_api_key'] : '';
        $mailchimp_list_id = isset( $pikoworks['mailchimp_list_id'] ) ? $pikoworks['mailchimp_list_id'] : '';
        $subscribe_form_title = isset( $pikoworks['subscribe_form_title'] ) ? $pikoworks['subscribe_form_title'] : '';
        $subscribe_form_input_placeholder = isset( $pikoworks['subscribe_form_input_placeholder'] ) ? $pikoworks['subscribe_form_input_placeholder'] : '';
        $subscribe_submit_text = isset( $pikoworks['subscribe_form_submit_text'] ) ? $pikoworks['subscribe_form_submit_text'] : '';
        $subscribe_success_message = isset( $pikoworks['subscribe_success_message'] ) ? $pikoworks['subscribe_success_message'] : '';
        
        get_header( 'coming' );
        
        $html = '';
        $title_html = '';
        $logo_img_html = '';
        $subscribe_form_html = '';
        $count_down_html = '';
        $social_icon_html = '';
        
        if ( trim( $img['url'] ) != '' ) {
            $logo_img_html = '<div class="logo"><img src="' . esc_url( $img['url'] ) . '" alt="'.esc_attr__('logo', 'piko-construct').'"></div>';
        }
        
        if($title){
            $title_html = trim( $title ) != '' ? '<h1>' . sanitize_text_field( $title ) . '</h1>' : '';
        }
        $enable_news = '';
        if ( $show_subscribe_form ) {
            $enable_news = 'enable-news';
            $subscribe_form_title_html = trim( $subscribe_form_title ) != '' ? '<p class="subs">' . sanitize_text_field( $subscribe_form_title ) . '</p>' : '';
            $subscribe_form_html = '<div class="newsletter-form-wrap">
                                        ' . $subscribe_form_title_html . '
                    					<form action="" name="news_letter" class="form-newsletter">
                                            <input type="hidden" name="api_key" value="' . esc_html( $mailchimp_api_key ) . '" />
                                                <input type="hidden" name="list_id" value="' . esc_html( $mailchimp_list_id ) . '" />
                                                <input type="hidden" name="success_message" value="' . sanitize_text_field( $subscribe_success_message ) . '" />
                    						<input type="text" name="email" placeholder="' . sanitize_text_field( $subscribe_form_input_placeholder ) . '">
                    						<span><button type="submit" name="submit_button" class="button_newletter">' . sanitize_text_field( $subscribe_submit_text ) . '</button></span>
                    					</form>
                    				</div> <!-- end newsletter-form-wrap -->';
        }
        
         
        if($social_icon == 1 ){
            $value= '';
            foreach ($GLOBALS['pikoworks']['coming_soon_social'] as $key => $val){
               if(! empty($GLOBALS['pikoworks'][$key]) && $val == 1 ){
                   $value .=  "<a target='_blank' href='" . esc_url($GLOBALS['pikoworks'][$key]). "'><i class='social-icon fa fa-" .esc_attr($key). "'></i></a>". " ";
                   }
               } 
           $social_icon_html =  '<div class="social-page">' . do_shortcode($value) . '</div>';
            
        }        
        
        
        if(isset($date) && $date != ''){            
            $date = (explode("/",$date));           
            $count_down_html = '<div class="countdown-lastest countdown-show4 clearfix coming-countdown" data-y="' .esc_attr( $date[2] ).'" data-m="'.esc_attr($date[0] ).'" data-d="'. esc_attr( $date[1] ).'" data-h="00" data-i="00" data-s="00" ></div>';
        }
        $center_class = '';
        $col_offset = 'col-sm-offset-6';
        if ($coming_page_layout == 1){
            $col_offset = 'col-sm-offset-3';
            $center_class = 'layout-center';
        }
        
        
        $html .= '<div class="container-fluid">
                		<div class="row maintenance '.esc_attr($enable_news . ' ' . $center_class) .'">
                                    <div class="col-sm-6 '.$col_offset.'">
                			' . $logo_img_html . '                			
                                        ' . $title_html . '  
                                        <div class="coming-content">
                                        ' . $count_down_html . '
                                        ' . $text . '
                                        ' . $subscribe_form_html . '
                                        ' . $social_icon_html . '                			
                                        </div>
                                    </div>
                		</div>
                	</div>';
        
        echo do_shortcode( $html );
        get_footer( 'coming' );
        
    }    
    
}