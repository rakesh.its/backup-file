<?php
/**
 * theme functions
 **/
if (!function_exists('pikoworks_get_template')) {
    /*
     * get template parts
     */
	function pikoworks_get_template($template, $name = null){
		get_template_part( 'template-parts/' . $template, $name);
	}
}

if ( !function_exists( 'pikoworks_get_option_data' ) ){
	function pikoworks_get_option_data($id, $fallback = false, $param = false){
		global $pikoworks;
		$pikoworks = apply_filters('pikoworks_filter_option_data',$pikoworks);
		if ( $fallback == false ){
			$fallback = '';
		}
		if(isset($pikoworks[$id]) && $pikoworks[$id] !== ''){
			$output = $pikoworks[$id];
		}
		else{
			$output = $fallback;
		}
		if ( !empty( $pikoworks[$id] ) && $param ) {
			if(isset($pikoworks[$id][$param])){
				$output = $pikoworks[$id][$param];
			}
			else{
				$output = $fallback;
			}
		}
		return $output;
	}
}

if (!function_exists('pikoworks_meta_tags')) {
    /**
     * favicon, Meta tags 
     **/
    function pikoworks_meta_tags() {
        // add favicon
       global $pikoworks;
       
       if (isset($pikoworks['custom_ios_title']) && !empty($pikoworks['custom_ios_title'])) {
            echo '<meta name="apple-mobile-web-app-title" content="' . esc_attr($pikoworks['custom_ios_title']) . '">';
        }
       
       if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
            if ( isset($pikoworks['optn_favicon']['url']) ) {
                    echo '<link rel="shortcut icon" type="image/x-icon" href="' . esc_url( $pikoworks['optn_favicon']['url']) . '" />' . "\n";
            } else {
                    echo '<link rel="shortcut icon" type="image/x-icon" href="' . get_template_directory_uri() . '/assets/images/logo/favicon.ico" />' . "\n";
            }
       }
       
       //retina favicon
       if (isset($pikoworks['custom_ios_icon144']['url']) && !empty($pikoworks['custom_ios_icon144']['url'])) {
             echo '<link rel="apple-touch-icon" sizes="144x144" href=" '. esc_url($pikoworks['custom_ios_icon144']['url']). '">';
        }
       if (isset($pikoworks['custom_ios_icon114']['url']) && !empty($pikoworks['custom_ios_icon114']['url'])){
            echo '<link rel="apple-touch-icon" sizes="114x114" href="' . esc_url($pikoworks['custom_ios_icon114']['url']).'">';
       }
       if (isset($pikoworks['custom_ios_icon72']['url']) && !empty($pikoworks['custom_ios_icon72']['url'])) {
            echo '<link rel="apple-touch-icon" sizes="72x72" href="' .  esc_url($pikoworks['custom_ios_icon72']['url']) . '">';
        }
        if (isset($pikoworks['custom_ios_icon57']['url']) && !empty($pikoworks['custom_ios_icon57']['url'])) {
            echo '<link rel="apple-touch-icon" sizes="57x57" href="' . esc_url($pikoworks['custom_ios_icon57']['url']) . '">';
        }
        
        echo '<meta name="robots" content="NOODP">';
        
        if ( is_front_page() && is_home() ) {
            // Default home page
            echo '<meta name="description" content="' . esc_attr(get_bloginfo( 'description' )) . '" />';
        } elseif ( is_front_page() ) {
            // static home page
            echo '<meta name="description" content="' . esc_attr(get_bloginfo( 'description' )) . '" />';
        } elseif ( is_home() ) {
            //blog page
            echo '<meta name="description" content="' . esc_attr(get_bloginfo( 'description' )) . '" />';
        } else {
            //  Is a singular
            if ( is_singular() ) {
                echo '<meta name="description" content="' . single_post_title( '', false ) . '" />';
            }
            else{ 
                // Is archive or taxonomy
                if ( is_archive() ) {
                    // Checking for shop archive
                    if ( function_exists( 'is_shop' ) ) { // products category, archive, search page
                        if ( is_shop() ) {
                            $post_id = get_option( 'woocommerce_shop_page_id' );                            
                            echo '<meta name="description" content="' . woocommerce_page_title( false ) . '" />';                           
                        }   
                    } 
                    else{
                        echo '<meta name="description" content="' . get_the_archive_description() . '" />';
                    }
                }
                else{
                    if ( is_404() ) {
                        $error_text =  esc_html__( 'Oops, page not found !', 'piko-construct' );
                        echo '<meta name="description" content="' . sanitize_text_field( $error_text ) . '" />';
                    }
                    else{ 
                        if ( is_search() ) {
                            echo '<meta name="description" content="' . sprintf( esc_html__( 'Search results for: %s', 'piko-construct' ), get_search_query() ) . '" />';
                        }
                        else{
                            // is category, is tags, is taxonomize
                            echo '<meta name="description" content="' . single_cat_title( '', false ) . '" />';
                        } 
                    }
                }                
                // Is WooCommerce page title
                if ( function_exists( 'is_woocommerce' ) ) {
                    if ( is_woocommerce() && !is_shop() ) {
                        if ( apply_filters( 'woocommerce_show_page_title', true ) ) {
                            echo '<meta name="description" content="' . woocommerce_page_title( false ) . '" />';
                        }
                    }
                }                
            }
        }
        
    }
    add_action( 'wp_head', 'pikoworks_meta_tags' );
}

if ( !function_exists( 'pikoworks_get_the_content_with_formatting' ) ){
	function pikoworks_get_the_content_with_formatting() {
		$content = get_the_content();
		$content = apply_filters('the_content', $content);
		$content = do_shortcode($content);
		return $content;
	}
}

if ( !function_exists( 'pikoworks_add_formatting' ) ) {
	function pikoworks_add_formatting($content){
		$content = do_shortcode($content);
		return $content;
	}
}


if ( !function_exists( 'pikoworks_get_current_page_url' ) ){
	function pikoworks_get_current_page_url() {
		$current_url = add_query_arg(null,null);
		return esc_url($current_url);
	}
}

//for tab vc product 6/10
if( ! function_exists('pikoworks_get_all_attributes') ){
    function pikoworks_get_all_attributes( $tag, $text ) {
        preg_match_all( '/' . get_shortcode_regex() . '/s', $text, $matches );
        $out = array();
        if( isset( $matches[2] ) )
        {
            foreach( (array) $matches[2] as $key => $value )
            {
                if( $tag === $value )
                    $out[] = shortcode_parse_atts( $matches[3][$key] );  
            }
        }
        return $out;
    }
}

if (!function_exists('pikoworks_unregister_post_type')) {
    /**
    * UNREGISTER CUSTOM POST TYPES
    **/
	function pikoworks_unregister_post_type( $post_type, $slug = '' ) {
            global $pikoworks, $wp_post_types, $cpt_disable ;
            if ( isset( $pikoworks['optn_cpt_disable'] ) ) {
                $cpt_disable = $pikoworks['optn_cpt_disable'];
                if ( ! empty( $cpt_disable ) ) {
                    foreach ( $cpt_disable as $post_type => $cpt ) {
                        if ( $cpt == 1 && isset( $wp_post_types[ $post_type ] ) ) {
                            unset( $wp_post_types[ $post_type ] );
                        }
                    }
                }
            }
	}
    add_action( 'init', 'pikoworks_unregister_post_type', 20 );
}