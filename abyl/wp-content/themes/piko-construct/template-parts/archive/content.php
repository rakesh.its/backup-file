<?php
/**
 * The template part for displaying content
 */

$size = 'full';
if (isset($GLOBALS['pikoworks_archive_loop']['image-size'])) {
    $size = $GLOBALS['pikoworks_archive_loop']['image-size'];
}
$archive_title_position = isset( $GLOBALS['pikoworks']['optn_archive_title_position'] ) ? trim( $GLOBALS['pikoworks']['optn_archive_title_position'] ) : 'image-bottom';
$except_word = isset( $GLOBALS['pikoworks']['optn_archive_except_word'] ) ? trim( $GLOBALS['pikoworks']['optn_archive_except_word'] ) : '55';

?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    
    <?php if($archive_title_position == 'image-top'): ?>
	<header class="entry-header">
            <?php pikoworks_entry_header(); ?>
	</header><!-- .entry-header -->
    <?php endif; ?>    
    <?php
    $thumbnail = pikoworks_post_format($size);
    if (!empty($thumbnail)) : ?>
        <figure class="entry-thumbnail-wrap">
            <?php echo wp_kses_post($thumbnail); ?>
        </figure>
    <?php endif; ?>
        
    <?php if($archive_title_position == 'image-bottom'): ?>
	<header class="entry-header">
           <?php pikoworks_entry_header_two(); ?>
	</header><!-- .entry-header -->
    <?php endif; ?> 
        
    <div class="entry-content">
        <?php
                /* translators: %s: Name of current post */
                if ( ! has_excerpt() ) {
                echo '<p>'. wp_trim_words( get_the_content(), esc_attr($except_word), '...  ' ) . ' ' . pikoworks_read_more_link() . '</p>';
                } else { 
                      echo '<p>'. wp_trim_words( the_excerpt(), esc_attr($except_word), '... ' ) . ' ' . pikoworks_read_more_link() . '</p>';
                }

                wp_link_pages( array(
                        'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'piko-construct' ) . '</span>',
                        'after'       => '</div>',
                        'link_before' => '<span>',
                        'link_after'  => '</span>',
                        'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'piko-construct' ) . ' </span>%',
                        'separator'   => '<span class="screen-reader-text">, </span>',
                ) );
        ?>
    </div><!-- .entry-content -->
</article><!-- #post-## -->