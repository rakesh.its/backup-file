<?php
/**
 * The template part for displaying content
 */
$size = 'full';
if (isset($GLOBALS['pikoworks_archive_loop']['image-size'])) {
    $size = $GLOBALS['pikoworks_archive_loop']['image-size'];
}


$archive_title_position = isset( $GLOBALS['pikoworks']['optn_archive_title_position'] ) ? trim( $GLOBALS['pikoworks']['optn_archive_title_position'] ) : 'image-bottom';

$prefix = 'pikoworks_';
$url = get_post_meta(get_the_ID(), $prefix.'post_format_link_url', true);
$text = get_post_meta(get_the_ID(), $prefix.'post_format_link_text', true);
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>   
    <header class="entry-header">        
        <?php pikoworks_entry_header_two(); ?>
    </header><!-- .entry-header -->      
    <div class="entry-content-link">
        <?php if (empty($url) || empty($text)) : ?>
            <?php the_content(); ?>
        <?php else : ?>
            <a href="<?php echo esc_url($url); ?>" rel="bookmark">
                <?php echo esc_html($text); ?>
            </a>
        <?php endif; ?>
    </div>
</article>