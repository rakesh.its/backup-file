<?php
/**
 * Post search
 * @author themepiko
 */
$args = array(
  'show_option_none' => esc_html__( 'All Categories', 'piko-construct' ),
  'taxonomy'    => 'category',
  'class'      => 'select-category',
  'hide_empty'  => 1,
  'orderby'     => 'name',
  'order'       => "asc",
  'tab_index'   => true,
  'hierarchical' => true
);
?>
<form class="form-inline" method="get" action="<?php echo esc_url( home_url( '/' ) ) ?>">
  <div class="form-group">    
    <div class="input-group">
      <div class="input-group-addon"><?php wp_dropdown_categories( $args ); ?></div>
      <input type="hidden" name="post_type" value="post" />
      <input value="<?php echo esc_attr( get_search_query() );?>" class="form-control" type="text" name="s"  placeholder="<?php esc_attr_e( 'Keyword here...', 'piko-construct' ); ?>" />
      
      <div class="input-group-addon btn-search"><button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button></div>
        <div class="input-group">      
        <div class="input-group-addon login">
          <a href="javascript:void(0)" class="button-togole togole-loginform login-btn hidden-xs" data-togole="piko-show-account">                                
                <?php if(is_user_logged_in()): ?>
                        <span class="icon fa fa-lock" aria-hidden="true"></span>
                        <?php else: ?>
                        <span class="icon fa fa-lock" aria-hidden="true"></span>
                <?php endif;?>
                <?php if(is_user_logged_in()): ?>
                        <span class="icon fa fa-unlock-alt" aria-hidden="true"> </span>
                        <?php else: ?>
                        <span class="icon fa fa-unlock-alt" aria-hidden="true"></span>
                <?php endif;?>                                
            </a>
        </div>
        </div>
    </div>
  </div>
</form>
