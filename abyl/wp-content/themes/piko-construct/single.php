<?php
/**
 * The template for displaying all single posts and attachments
 *
 */

get_header(); 

$prefix = 'pikoworks_';
$sidebar_position =  get_post_meta(get_the_ID(), $prefix . 'page_sidebar',true);

if (($sidebar_position === '') || ($sidebar_position == '-1')) {
    $sidebar_position = isset( $GLOBALS['pikoworks']['optn_blog_single_sidebar_pos'] ) ? $GLOBALS['pikoworks']['optn_blog_single_sidebar_pos'] : 'fullwidth';
} 

$primary_class = pikoworks_primary_blog_single_sidebar_class();
$secondary_class = pikoworks_secondary_blog_single_sidebar_class();

$left_sidebar =  get_post_meta(get_the_ID(), $prefix . 'page_right_sidebar',true);

if (($left_sidebar === '') || ($left_sidebar == '-1')) {
    $left_sidebar = isset( $GLOBALS['pikoworks']['optn_blog_single_sidebar'] ) ? $GLOBALS['pikoworks']['optn_blog_single_sidebar'] : 'sidebar-1';

}
$right_sidebar =  get_post_meta(get_the_ID(), $prefix . 'page_left_sidebar',true);

if (($right_sidebar === '') || ($right_sidebar == '-1')) {    
    $right_sidebar = isset( $GLOBALS['pikoworks']['optn_blog_single_sidebar_left'] ) ? $GLOBALS['pikoworks']['optn_blog_single_sidebar_left'] : '';
}


?>
<?php if ( $sidebar_position == 'both' ): ?>
	<aside id="secondary" class="widget-area <?php echo esc_attr( $secondary_class ); ?>" role="complementary">
		<?php dynamic_sidebar( $right_sidebar ); ?>
	</aside><!-- .sidebar .widget-area -->
<?php endif; ?>

<div id="primary" class="content-area <?php echo esc_attr( $primary_class ); ?>">
	<main id="main" class="site-main" role="main">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the single post content template.
			get_template_part( 'template-parts/content', 'single' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// Previous/next post navigation.
                        the_post_navigation( array(
                                'next_text' => '<span class="meta-nav" aria-hidden="true">' . esc_html__( 'Next', 'piko-construct' ) . '</span> ' .
                                        '<span class="screen-reader-text">' . esc_html__( 'Next post:', 'piko-construct' ) . '</span> ' .
                                        '<span class="post-title">%title</span>',
                                'prev_text' => '<span class="meta-nav" aria-hidden="true">' . esc_html__( 'Previous', 'piko-construct' ) . '</span> ' .
                                        '<span class="screen-reader-text">' . esc_html__( 'Previous post:', 'piko-construct' ) . '</span> ' .
                                        '<span class="post-title">%title</span>',
                        ) );

			// End of the loop.
		endwhile;
		?>

	</main><!-- .site-main -->

</div><!-- .content-area -->

<?php if ( $sidebar_position != 'fullwidth' || $sidebar_position == 'both' ): ?>
	<aside id="secondary" class="widget-area <?php echo esc_attr( $secondary_class ); ?>" role="complementary">
		<?php dynamic_sidebar( $left_sidebar ); ?>
	</aside><!-- .sidebar .widget-area -->
<?php endif; ?>
<?php get_footer(); ?>