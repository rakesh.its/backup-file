<?php
/**
 * @author themepiko
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

?>
<article class="piko-categories">
        <div  class="piko-categories-wrap">
            <div class="product-header">
                <a href="<?php echo get_term_link( $category->slug, 'product_cat' ); ?>" class="icon icon-arrows-slim-right"></a>
            </div>
            <figure>
                <?php
                    /**
                     * woocommerce_before_subcategory_title hook
                     *
                     * @hooked woocommerce_subcategory_thumbnail - 10
                     */
                    do_action( 'woocommerce_before_subcategory_title', $category );
                ?>
            </figure>
        </div>
            <footer class="product-footer">
                <h5 class="category"><a href="<?php echo get_term_link( $category->slug, 'product_cat' ); ?>"><?php echo esc_attr( $category->name); ?></a></h5>
                <div class="product-count">
                   <?php if ( $category->count > 0 ) echo apply_filters( 'woocommerce_subcategory_count_html', ' ' . $category->count . ' '. esc_html__('Products','woocommerce').'', $category); ?> 
                </div>
                <?php
                    /**
                     * woocommerce_after_subcategory_title hook
                     */
                    do_action( 'woocommerce_after_subcategory_title', $category );
                ?>            
                <div class="clearfix"></div>
            </footer>
</article>