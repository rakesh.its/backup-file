<?php
/*
 * custom template
 * 
 *  Quick view overide.
 * @author Themepiko
 *
*/
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
while ( have_posts() ) : the_post(); ?>
 <div class="product quick-view-custom row">
    <div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="row">
            <div class="col-md-6">
                <?php                
                    do_action('woocommerce_before_single_product_summary');
                ?>               
            </div>
            <div class="col-md-6">
                <div class="summary entry-summary quick-view">
                    <?php                   
                        do_action('woocommerce_single_product_summary');
                    ?>
                </div>                
            </div>
        </div>
    </div>
</div>
<?php endwhile; // end of the loop. ?>
<script type="text/javascript">!function(e){"use strict";e(document).ready(function(){new Swiper(".product-gallery-carousel",{pagination:"",nextButton:".swiper-button-next",prevButton:".swiper-button-prev",slidesPerView:4,spaceBetween:22,breakpoints:{1200:{spaceBetween:14},992:{slidesPerView:3,spaceBetween:14},580:{slidesPerView:3,spaceBetween:14},320:{slidesPerView:2,spaceBetween:14}}});e.fn.elevateZoom&&(e("#product-zoom").elevateZoom({responsive:!0,zoomType:"inner",cursor:"crosshair",borderSize:1,borderColour:"#e7e7e7",lensSize:180,lensBorder:4,lensOpacity:1,lensColour:"rgba(255, 255, 255, 0.18)"}),e(".product-gallery-carousel").find("a").on("click",function(t){var o=e("#product-zoom").data("elevateZoom"),r=e(this).data("image"),n=e(this).data("zoom-image");e("#product-zoom").removeAttr("srcset"),o.swaptheimage(r,n),t.preventDefault()})),e("#yith-quick-view-close").click(function(){e(".zoomContainer").remove()}),e(".print-button button").on("click",function(){e(this).parent().toggleClass("start-animation")})})}(jQuery);</script>