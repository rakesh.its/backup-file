<?php

$directory_url_1=get_option('_iv_directory_url_1');					
if($directory_url_1==""){$directory_url_1='hospital';}	

$directory_url_2=get_option('_iv_directory_url_2');					
if($directory_url_2==""){$directory_url_2='doctor';}



$title=(isset($atts['cpt1_category_title'])?$atts['cpt1_category_title']:'Hospital Categories');
$banner_subtitle=(isset($atts['cpt1_category_sub_title'])?$atts['cpt1_category_sub_title']:'With over 5000 doctors and experts in the healthcare field medical directory provides a listing of all doctors across a wide variety if medical fields');



?>
<h2 class="home-title" style="text-align: center;"><strong><?php echo $title;?></strong></h2>
<div class="home-subtitle"><?php echo $banner_subtitle;?></div>
<div style="text-align: center;"><?php echo do_shortcode('[hospital_categories]')?></div>
