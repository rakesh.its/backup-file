<?php
// VC elements
add_action('vc_before_init', 'medicaldirectory_top_banner');
function medicaldirectory_top_banner(){
					vc_map( array(
						  "name" => esc_html__( "Home Top Banner", "medical-directory" ),
						  "base" => "medicaldirectory_top",
						  'icon' =>  medicaldirectory_IMAGE.'vc-icon.png',
						  "class" => "",
						  "category" => esc_html__( "Medical Directory", "medical-directory"),
						  "params" => array(
						   array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Banner Top Icon", "medical-directory" ),
								"param_name" => "banner_top_icon",
								"value" => esc_html__( "fa-user-md", "medical-directory" ),
								"description" => __( "You can more Icon code here : https://fontawesome.com/v4.7.0/icons/ ", "medical-directory" )
								
							 ),
						  array(
								"type" => "attach_image",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( " Top Banner Image", "medical-directory" ),
								"param_name" => "top_banner",								
								),							 
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Top Title", "medical-directory" ),
								"param_name" => "top_title",
								"value" => esc_html__( "Welcome to Medical Directory", "medical-directory" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Sub Title", "medical-directory" ),
								"param_name" => "top_sub_title",
								"value" => esc_html__( "SEARCH FOR HOSPITALS AND DOCTORS ON WORLD WIDE BASIS", "medical-directory" ),								
							 ),
							
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Hospital Button Text", "medical-directory" ),
								"param_name" => "hospital_button_text",
								"value" => esc_html__( "FIND A HOSPITAL", "medical-directory" ),								
							 ),
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Hospital Button Link", "medical-directory" ),
								"param_name" => "hospital_button_link",
								"value" => esc_html__( "", "medical-directory" ),								
							 ),	
							 					 
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Doctor Button Text", "medical-directory" ),
								"param_name" => "doctor_button_text",
								"value" => esc_html__( "FIND A DOCTOR", "medical-directory" ),								
							 ),	
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Doctor Button Link", "medical-directory" ),
								"param_name" => "doctor_button_link",
								"value" => esc_html__( "", "medical-directory" ),								
							 ),	
							  array(
								"type" => "checkbox",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Top Banner Search Bar", "medical-directory" ),
								"param_name" => "top_search_bar",
								"value" => esc_html__( "1", "medical-directory" ),								
							 ),
						  )
					   ) );
				
				}
add_shortcode('medicaldirectory_top', 'medicaldirectory_top_func');	

function medicaldirectory_top_func($atts, $content = null ){	
									
	include('vc/top_banner.php');				
}	
//********* 3 home page blocks
add_action('vc_before_init', 'medicaldirectory_top_blocks');
function medicaldirectory_top_blocks(){
					vc_map( array(
						  "name" => esc_html__( "Home Top 3 Blocks", "medical-directory" ),
						  "base" => "medicaldirectory_top_3blocks",
						  'icon' =>  medicaldirectory_IMAGE.'vc-icon.png',
						  "class" => "",
						  "category" => esc_html__( "Medical Directory", "medical-directory"),
						  "params" => array(
						  array(
								"type" => "colorpicker",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 1 Color", "medical-directory" ),
								"param_name" => "block1_color",
								"value" => esc_html__( "#f5f5f5", "medical-directory" ),
								
								
							 ),
						   array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 1 Top Icon", "medical-directory" ),
								"param_name" => "block1_top_icon",
								"value" => esc_html__( "fa-hospital-o", "medical-directory" ),
								"description" => __( "You can more Icon code here : https://fontawesome.com/v4.7.0/icons/ ", "medical-directory" )
								
							 ),
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 1 Top Title", "medical-directory" ),
								"param_name" => "b1top_title",
								"value" => esc_html__( "Hospital", "medical-directory" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 1 Sub Title", "medical-directory" ),
								"param_name" => "b1top_sub_title",
								"value" => esc_html__( "With Over 300 hospitals across 20 countries medical directory is the right place to find your closest healthcare center", "medical-directory" ),								
							 ),
							 
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 1 Button Text", "medical-directory" ),
								"param_name" => "b1button_title",
								"value" => esc_html__( "SEARCH NOW", "medical-directory" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 1 Button Link", "medical-directory" ),
								"param_name" => "b1top_button_link",
								"value" => esc_html__( "", "medical-directory" ),								
							 ),							 
							
							 array(
								"type" => "colorpicker",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 2 Color", "medical-directory" ),
								"param_name" => "block2_color",
								"value" => esc_html__( "#f5f5f5", "medical-directory" ),
								
								
							 ),
							   array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 2 Top Icon", "medical-directory" ),
								"param_name" => "block2_top_icon",
								"value" => esc_html__( "fa-user-md", "medical-directory" ),
								"description" => __( "You can more Icon code here : https://fontawesome.com/v4.7.0/icons/ ", "medical-directory" )
								
							 ),
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 2 Top Title", "medical-directory" ),
								"param_name" => "b2top_title",
								"value" => esc_html__( "Doctor", "medical-directory" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 2 Sub Title", "medical-directory" ),
								"param_name" => "b2top_sub_title",
								"value" => esc_html__( "Find the right doctor within the closest hospital across a wide range of medical fields including neurosurgery", "medical-directory" ),								
							 ),
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 2 Button Text", "medical-directory" ),
								"param_name" => "b2button_title",
								"value" => esc_html__( "SEARCH NOW", "medical-directory" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 2 Button Link", "medical-directory" ),
								"param_name" => "b2top_button_link",
								"value" => esc_html__( "", "medical-directory" ),								
							 ),	
							  array(
								"type" => "colorpicker",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 3 Color", "medical-directory" ),
								"param_name" => "block3_color",
								"value" => esc_html__( "#f5f5f5", "medical-directory" ),
								
								
							 ),	
							   array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 3 Top Icon", "medical-directory" ),
								"param_name" => "block3_top_icon",
								"value" => esc_html__( "fa-file-text-o", "medical-directory" ),
								"description" => __( "You can more Icon code here : https://fontawesome.com/v4.7.0/icons/ ", "medical-directory" )
								
							 ),
							array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 3 Top Title", "medical-directory" ),
								"param_name" => "b3top_title",
								"value" => esc_html__( "Register Now", "medical-directory" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 3 Sub Title", "medical-directory" ),
								"param_name" => "b3top_sub_title",
								"value" => esc_html__( "You're a medical center with hospitals and doctors worldwide, medical directory is the right place to list your hospitals and doctors, join us now", "medical-directory" ),								
							 ), 
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 3 Button Text", "medical-directory" ),
								"param_name" => "b3button_title",
								"value" => esc_html__( "Register", "medical-directory" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Block 3 Button Link", "medical-directory" ),
								"param_name" => "b3top_button_link",
								"value" => esc_html__( "", "medical-directory" ),								
							 ),		
						  
							 
							 
						  )
					   ) );
				
				}
add_shortcode('medicaldirectory_top_3blocks', 'medicaldirectory_top_3blocks_func');	

function medicaldirectory_top_3blocks_func($atts, $content = null ){	
									
	include('vc/top_3blocks.php');				
}	
// CPT 1 category
$directory_url_1=get_option('_iv_directory_url_1');					
if($directory_url_1==""){$directory_url_1='hospital';}	

add_action('vc_before_init', 'medicaldirectory_cpt1_category');
function medicaldirectory_cpt1_category(){
					vc_map( array(
						  "name" => esc_html__( "Hospital Categories", "medical-directory" ),
						  "base" => "medicaldirectory_cpt1_category",
						  'icon' =>  medicaldirectory_IMAGE.'vc-icon.png',
						  "class" => "",
						  "category" => esc_html__( "Medical Directory", "medical-directory"),
						  "params" => array(
						  						 
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Top Title", "medical-directory" ),
								"param_name" => "cpt1_category_title",
								"value" => esc_html__( "Hospital Categories", "medical-directory" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Sub Title", "medical-directory" ),
								"param_name" => "cpt1_category_sub_title",
								"value" => esc_html__( "With over 5000 doctors and experts in the healthcare field medical directory provides a listing of all doctors
across a wide variety if medical fields", "medical-directory" ),								
							 ),							
							
						  )
					   ) );
				
				}
add_shortcode('medicaldirectory_cpt1_category', 'medicaldirectory_cpt1_category_func');	

function medicaldirectory_cpt1_category_func($atts, $content = null ){	
									
	include('vc/ctp1_category.php');				
}

// CPT 2 category


add_action('vc_before_init', 'medicaldirectory_cpt2_category');
function medicaldirectory_cpt2_category(){
					vc_map( array(
						  "name" => esc_html__( "Doctor Categories", "medical-directory" ),
						  "base" => "medicaldirectory_cpt2_category",
						  'icon' =>  medicaldirectory_IMAGE.'vc-icon.png',
						  "class" => "",
						  "category" => esc_html__( "Medical Directory", "medical-directory"),
						  "params" => array(
						    array(
								"type" => "attach_image",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( " Background Image", "medical-directory" ),
								"param_name" => "cpt2_category_image",								
								),		
						  						 
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Top Title", "medical-directory" ),
								"param_name" => "cpt2_category_title",
								"value" => esc_html__( "Doctor Categories", "medical-directory" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Sub Title", "medical-directory" ),
								"param_name" => "cpt2_category_sub_title",
								"value" => esc_html__( "With over 5000 doctors and experts in the healthcare field medical directory provides a listing of all doctors
across a wide variety if medical fields", "medical-directory" ),								
							 ),	
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Categores Only", "medical-directory" ),
								"param_name" => "cpt2_category_only_slug",
								"description" => __( "You can add category slugs", "medical-directory" )								
							 ),							
							
						  )
					   ) );
				
				}
add_shortcode('medicaldirectory_cpt2_category', 'medicaldirectory_cpt2_category_func');	

function medicaldirectory_cpt2_category_func($atts, $content = null ){	
									
	include('vc/ctp2_category.php');				
}
/// Feature Hospital

add_action('vc_before_init', 'medicaldirectory_ctp1_featured');
function medicaldirectory_ctp1_featured(){
					vc_map( array(
						  "name" => esc_html__( "Hospital Featured", "medical-directory" ),
						  "base" => "medicaldirectory_ctp1_featured",
						  'icon' =>  medicaldirectory_IMAGE.'vc-icon.png',
						  "class" => "",
						  "category" => esc_html__( "Medical Directory", "medical-directory"),
						  "params" => array(
						    array(
								"type" => "attach_image",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( " Background Image", "medical-directory" ),
								"param_name" => "cpt1_featured_image",								
								),		
						  						 
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Top Title", "medical-directory" ),
								"param_name" => "cpt1_featured_title",
								"value" => esc_html__( "Featured Hospital", "medical-directory" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Sub Title", "medical-directory" ),
								"param_name" => "cpt1_featured_sub_title",
								"value" => esc_html__( "With over 5000 doctors and experts in the healthcare field medical directory provides a listing of all doctors
across a wide variety if medical fields", "medical-directory" ),								
							 ),	
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Featured Hospital IDs", "medical-directory" ),
								"param_name" => "cpt1_featured_ids",
								"description" => __( "10,20,36", "medical-directory" )								
							 ),							
							
						  )
					   ) );
				
				}
add_shortcode('medicaldirectory_ctp1_featured', 'medicaldirectory_ctp1_featured_func');	

function medicaldirectory_ctp1_featured_func($atts, $content = null ){	
									
	include('vc/ctp1_featured.php');				
}


// CPT 2 featured


add_action('vc_before_init', 'medicaldirectory_ctp2_featured');
function medicaldirectory_ctp2_featured(){
					vc_map( array(
						  "name" => esc_html__( "Doctor Featured", "medical-directory" ),
						  "base" => "medicaldirectory_ctp2_featured",
						  'icon' =>  medicaldirectory_IMAGE.'vc-icon.png',
						  "class" => "",
						  "category" => esc_html__( "Medical Directory", "medical-directory"),
						  "params" => array(
						    array(
								"type" => "attach_image",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( " Background Image", "medical-directory" ),
								"param_name" => "cpt2_featured_image",								
								),		
						  						 
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Top Title", "medical-directory" ),
								"param_name" => "cpt2_featured_title",
								"value" => esc_html__( "Featured Doctor", "medical-directory" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Sub Title", "medical-directory" ),
								"param_name" => "cpt2_featured_sub_title",
								"value" => esc_html__( "With over 5000 doctors and experts in the healthcare field medical directory provides a listing of all doctors
across a wide variety if medical fields", "medical-directory" ),								
							 ),	
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Featured Doctor IDs", "medical-directory" ),
								"param_name" => "cpt2_featured_ids",
								"description" => __( "10,20,36", "medical-directory" )								
							 ),							
							
						  )
					   ) );
				
				}
add_shortcode('medicaldirectory_ctp2_featured', 'medicaldirectory_ctp2_featured_func');	

function medicaldirectory_ctp2_featured_func($atts, $content = null ){	 
									
	include('vc/ctp2_featured.php');				
}

// Latest Post
add_action('vc_before_init', 'medicaldirectory_latest_post');
function medicaldirectory_latest_post(){
					vc_map( array(
						  "name" => esc_html__( "Latest Post", "medical-directory" ),
						  "base" => "medicaldirectory_latest_post",
						  'icon' =>  medicaldirectory_IMAGE.'vc-icon.png',
						  "class" => "",
						  "category" => esc_html__( "Medical Directory", "medical-directory"),
						  "params" => array(
						   			 
							 array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Top Title", "medical-directory" ),
								"param_name" => "latest_post_title",
								"value" => esc_html__( "Latest Post", "medical-directory" ),
								
							 ),
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Sub Title", "medical-directory" ),
								"param_name" => "latest_post_sub_title",
								"value" => esc_html__( "With over 5000 doctors and experts in the healthcare field medical directory provides a listing of all doctors
across a wide variety if medical fields", "medical-directory" ),								
							 ),	
							  array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => esc_html__( "Post IDs", "medical-directory" ),
								"param_name" => "latest_post_ids",
								"description" => __( "10,20,36", "medical-directory" )								
							 ),							
							
						  )
					   ) );
				
				}
add_shortcode('medicaldirectory_latest_post', 'medicaldirectory_latest_post_func');	

function medicaldirectory_latest_post_func($atts, $content = null ){	 
									
	include('vc/latest_post.php');				
}

