<?php
global $wpdb, $wp_hasher;
		
	$email_body = get_option( 'iv_directories_forget_email');
	$forget_email_subject = get_option( 'iv_directories_forget_email_subject');			
					
		$admin_mail = get_option('admin_email');	
		if( get_option( 'admin_email_iv_directories' )==FALSE ) {
			$admin_mail = get_option('admin_email');						 
		}else{
			$admin_mail = get_option('admin_email_iv_directories');								
		}						
		$wp_title = get_bloginfo();
	
	parse_str($_POST['form_data'], $data_a);
	
	$user_info = get_user_by( 'email',$data_a['forget_email'] );
	if(isset($user_info->ID) ){

		
		
		$user_login = $user_info->user_login;
		
		do_action('lostpassword_post');
		
		// redefining user_login ensures we return the right case in the email
		$user_login = $user_info->user_login;
		$user_email = $user_info->user_email;

		do_action('retreive_password', $user_login);  // Misspelled and deprecated
		do_action('retrieve_password', $user_login);

		$allow = apply_filters('allow_password_reset', true, $user_info->ID);

		if ( ! $allow )
			return false;
		else if ( is_wp_error($allow) )
			return false;

		$key = wp_generate_password( 20, false );
		do_action( 'retrieve_password_key', $user_login, $key );

		if ( empty( $wp_hasher ) ) {
			require_once ABSPATH . 'wp-includes/class-phpass.php';
			$wp_hasher = new PasswordHash( 8, true );
		}
		//$hashed = $wp_hasher->HashPassword( $key );
		$hashed = time() . ':' . $wp_hasher->HashPassword( $key );
		
		$wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user_login ) );

		$rp_link= site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') ;

		$rp_link_new='<a href="'.$rp_link.'">'.$rp_link.'</a>';
		 // New pass link end *************
		


		$email_body = str_replace("[user_name]", $user_info->display_name, $email_body);
		$email_body = str_replace("[iv_member_user_name]", $user_info->user_login, $email_body);	
		$email_body = str_replace("[iv_member_password]", $rp_link_new, $email_body); 
				
		$cilent_email_address =$user_info->user_email; //trim(get_post_meta($post_id, 'iv_form_modal_client_email', true));
						
		
		
				
		$auto_subject=  $forget_email_subject; 
								
		$headers = array("From: " . $wp_title . " <" . $admin_mail . ">", "Content-Type: text/html");
		$h = implode("\r\n", $headers) . "\r\n";
		wp_mail($cilent_email_address, $auto_subject, $email_body, $h);
			
	}
					
			
	

