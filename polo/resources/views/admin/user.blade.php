@include('admin.include.header')
@include('admin.include.sidebar')
<div class="ibox" style="margin-top:10px">
        <div class="ibox-head" >
            <b>Update  Admin Details</b>
        </div>
       
        <div class="ibox-body">
            <form action="{{url('admin/user/update')}}" method="post" enctype="multipart/form-data" id="product_frm">
                @csrf()
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Name:<span style="color:red">*</span></label>
                            <input type="text" required class="form-control" name="user_name" id="user_name" value="{{Auth::user()->name}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Email:<span style="color:red">*</span></label>
                            <input type="email" required class="form-control" name="user_email" id="user_email" value="{{Auth::user()->email ?? '' }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">User Name:<span style="color:red">*</span></label>
                            <input type="text" required class="form-control" name="user_username" id="user_username" value="{{Auth::user()->username ?? ''}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Contact No:<span style="color:red">*</span></label>
                            <input type="text"  class="form-control" name="user_contact_no" id="user_contact_no" value="{{Auth::user()->contact_no ?? ''}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Profile Pic:<span style="color:red">*</span></label>
                            <input type="file" class="form-control" name="profile_pic" id="profile_pic">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="submit" class="btn btn-outline-primary" value="Update" style="margin-top:32px;">
                        </div>
                    </div>
                    
                </div>
            </form>
        </div>
    </div>
@include('admin.include.footer') 