@include('admin.include.header')
<link href="{{url('AdminAssest/vendors/summernote/dist/summernote.css')}}" rel="stylesheet" />
@include('admin.include.sidebar')

<!-- START PAGE CONTENT-->

    <div class="ibox" style="margin-top:10px">
        <div class="ibox-head">
            <div class="ibox-title">List  Category</div>
            <button class="btn btn-outline-primary pull-right" onclick="addCategory()">Add Category<i class="fa fa-plus"></i> </button>
        </div>
       
        <div class="ibox-body">
            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Date Created</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
               
                <tbody>
                    @foreach($category as $key_category => $value_category)
                    <tr>
                        <td>{{$value_category->heading}}</td>
                        
                        <td>{{$value_category->description}}</td>
                        <td class="text-right">{{date('d M Y',strtotime($value_category->created_at))}}</td>
                        @if($value_category->status == 1)
                        <td class="text-center">
                            <p class="mb-0">
                                <span class="badge badge-success">Active</span>
                            </p>
                        </td>
                        @else
                        <td class="text-center">
                            <p class="mb-0">
                                <span class="badge badge-danger">Inactive</span>
                            </p>
                        </td>
                        @endif
                        <td style="width: 15%;" class="action">
                                <a href="javascript:void()" class="on-default view-row" onclick="editCategory({{$value_category->id}})" data-placement="top" title="Active "><i class="fa fa-edit" aria-hidden="true"></i></a>
                                <a href="{{url('admin/category/statusUpdate/')}}/{{$value_category->id}}" class="on-default view-row" data-placement="top" title="Active "><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    @endforeach
                   
                    
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="category_action" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{url('admin/category/save')}}" method="post" enctype="multipart/form-data">
                @csrf()
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Category :<span id="title_view">Add</span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Category Heading:</label>
                            <input type="text" required class="form-control" name="heading" id="heading">
                            <input type="hidden" required class="form-control" name="category_id" id="category_id">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Category Description:</label>
                            <textarea name="description" id="description" class="form-control"></textarea>
                        </div>
                        
                        <div class="form-group">
                            <p class="control-label"><b>Is Active</b>
                                <font color="red">*</font>
                            </p>
                            <div class="radio radio-info form-check-inline">
                                <input type="radio" value="1" name="status" checked="">
                                <label for="inlineRadio1"> Active </label>
                            </div>
                            <div class="radio radio-info form-check-inline">
                                <input type="radio" value="0" name="status">
                                <label for="inlineRadio1"> Inactive </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <center><button type="submit" class="btn btn-primary">Submit</button></center>
                    </div>
                </div>
            </form>
        </div>
    </div>           
@include('admin.include.footer') 
   
<script src="{{url('AdminAssest/vendors/summernote/dist/summernote.min.js')}}" type="text/javascript"></script>
<script>
     function addCategory() {
        $('#page').val('');
        $("#title_view").html("Add");

        $('#category_action').modal('show');
    }

    function editCategory(id) {
        $("#title_view").html("Edit");
       
        $("#category_id").val(id);
        $.ajax({
            url: "{{url('admin/category/fetch')}}" + "/" + id,
            method: "GET",
            contentType: 'application/json',
            dataType: "json",
            success: function (data) {

                console.log(data);
                $("#category_title").val(data.category_title);
                $("#heading").val(data.heading);
                $("#description	").val(data.description	);
                $("#category_position").val(data.category_position);
             
                var val = data.status;
                if (val == 1) {
                    $('input[name=status][value=' + val + ']').prop('checked', true);
                } else {
                    $('input[name=status][value=' + val + ']').prop('checked', true);
                }
            }
        });
        $('#category_action').modal('show');

    }
</script>
