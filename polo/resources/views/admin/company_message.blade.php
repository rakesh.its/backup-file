@include('admin.include.header')
<link href="{{url('AdminAssest/vendors/summernote/dist/summernote.css')}}" rel="stylesheet" />
@include('admin.include.sidebar')

<!-- START PAGE CONTENT-->

    <div class="ibox" style="margin-top:10px">
        <div class="ibox-head">
            <div class="ibox-title">List  Pages</div>
            <button class="btn btn-outline-primary pull-right" onclick="addPages()">Add Pages<i class="fa fa-plus"></i> </button>
        </div>
       
        <div class="ibox-body">
            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Title</th>
                        <th>Position</th>
                        <th>Date Created</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
               
                <tbody>
                    @foreach($pages as $key_pages => $value_pages)
                    <tr>
                        <td>{{$value_pages->page_name}}</td>
                        <td>{{$value_pages->page_title}}</td>
                        <td>{{$value_pages->page_position}}</td>
                        <td>{{date('d/M/Y',strtotime($value_pages->created_at))}}</td>
                        @if($value_pages->status == 1)
                        <td>
                            <p class="mb-0">
                                <span class="badge badge-success">Active</span>
                            </p>
                        </td>
                        @else
                        <td>
                            <p class="mb-0">
                                <span class="badge badge-danger">Inactive</span>
                            </p>
                        </td>
                        @endif
                        <td style="width: 15%;" class="action">
                                <a href="javascript:void()" class="on-default view-row" onclick="editPages({{$value_pages->id}})" data-placement="top" title="Active "><i class="fa fa-edit" aria-hidden="true"></i></a>
                                <a href="{{url('admin/pages/statusUpdate/')}}/{{$value_pages->id}}" class="on-default view-row" data-placement="top" title="Active "><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    @endforeach
                   
                    
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="pages_action" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{url('admin/pages/save')}}" method="post" enctype="multipart/form-data">
                @csrf()
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Pages :<span id="title_view">Add</span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Name:</label>
                            <input type="text" required class="form-control" name="page_name" id="page_name">
                            <input type="hidden" required class="form-control" name="pages_id" id="pages_id">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Title:</label>
                            <input type="text" required class="form-control" name="page_title" id="page_title">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Pages Position:</label>
                            <select class="form-control" name="page_position" id="page_position">
                                <option>Header</option>
                                <option>Footer</option>
                            </select>
                            
                        </div>
                        
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Page Contain:</label>
                            <textarea name="page_contain" data-provide="markdown" data-iconlibrary="fa" rows="10">h1 header ============ Paragraphs are separated by a blank line. 2nd paragraph. *Italic*, **bold**, and `monospace`. Itemized lists look like: * this one * that one * the other one</textarea>
                        </div>
                        
                        <div class="form-group">
                            <p class="control-label"><b>Is Active</b>
                                <font color="red">*</font>
                            </p>
                            <div class="radio radio-info form-check-inline">
                                <input type="radio" value="1" name="status" checked="">
                                <label for="inlineRadio1"> Active </label>
                            </div>
                            <div class="radio radio-info form-check-inline">
                                <input type="radio" value="0" name="status">
                                <label for="inlineRadio1"> Inactive </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <center><button type="submit" class="btn btn-primary">Submit</button></center>
                    </div>
                </div>
            </form>
        </div>
    </div>           
@include('admin.include.footer') 
   
<script src="{{url('AdminAssest/vendors/summernote/dist/summernote.min.js')}}" type="text/javascript"></script>
<script>
     function addPages() {
        $('#page').val('');
        $("#title_view").html("Add");

        $('#pages_action').modal('show');
    }

    function editPages(id) {
        $("#title_view").html("Edit");
       
        $("#pages_id").val(id);
        $.ajax({
            url: "{{url('admin/pages/fetch')}}" + "/" + id,
            method: "GET",
            contentType: 'application/json',
            dataType: "json",
            success: function (data) {

                console.log(data);
                $("#page_title").val(data.page_title);
                $("#page_name").val(data.page_name);
                $("#page_contain").val(data.page_contain);
                $("#page_position").val(data.page_position);
             
                var val = data.status;
                if (val == 1) {
                    $('input[name=status][value=' + val + ']').prop('checked', true);
                } else {
                    $('input[name=status][value=' + val + ']').prop('checked', true);
                }
            }
        });
        $('#pages_action').modal('show');

    }
</script>
