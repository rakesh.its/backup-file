@include('admin.include.header')
<link href="{{url('AdminAssest/vendors/summernote/dist/summernote.css')}}" rel="stylesheet" />
@include('admin.include.sidebar')

<!-- START PAGE CONTENT-->

    <div class="ibox" style="margin-top:10px">
        <div class="ibox-head">
            <div class="ibox-title">List  testimonial</div>
            <button class="btn btn-outline-primary pull-right" onclick="addtestimonial()">Add testimonial<i class="fa fa-plus"></i> </button>
        </div>
       
        <div class="ibox-body">
            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Message</th>
                        <th>Photo</th>
                        <th>Date Created</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
               
                <tbody>
                    @foreach($testimonial as $key_testimonial => $value_testimonial)
                    <tr>
                        <td>{{$value_testimonial->customer_name}}</td>
                        <td>{{$value_testimonial->customer_message}}</td>
                        <td><img src="{{url('upload/testimonial')}}/{{$value_testimonial->customer_photo}}" width="50px" height="50px"></td>

                        <td class="text-right">{{date('d/M/Y',strtotime($value_testimonial->created_at))}}</td>
                        @if($value_testimonial->status == 1)
                        <td class="text-center">
                            <p class="mb-0">
                                <span class="badge badge-success">Active</span>
                            </p>
                        </td>
                        @else
                        <td class="text-center">
                            <p class="mb-0">
                                <span class="badge badge-danger">Inactive</span>
                            </p>
                        </td>
                        @endif
                        <td style="width: 15%;" class="action">
                                <a href="javascript:void()" class="on-default view-row" onclick="edittestimonial({{$value_testimonial->id}})" data-placement="top" title="Active "><i class="fa fa-edit" aria-hidden="true"></i></a>
                                <a href="{{url('admin/testimonial/statusUpdate/')}}/{{$value_testimonial->id}}" class="on-default view-row" data-placement="top" title="Active "><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    @endforeach
                   
                    
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="testimonial_action" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="{{url('admin/testimonial/save')}}" method="post" enctype="multipart/form-data">
                @csrf()
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">testimonial :<span id="title_view">Add</span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Customer Name:</label>
                            <input type="text" required class="form-control" name="customer_name" id="customer_name">
                            <input type="hidden" required class="form-control" name="testimonial_id" id="testimonial_id">
                        </div>
                      
                        
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label"> Customer Messages:</label>
                            <textarea name="customer_message" class="form-control" id="customer_message" ></textarea>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Customer Photo:</label>
                            <input type="file"  class="form-control" name="customer_photo" id="customer_photo">
                            <div id="edit_photo_section"></div>
                        </div>
                        <div class="form-group">
                            <p class="control-label"><b>Is Active</b>
                                <font color="red">*</font>
                            </p>
                            <div class="radio radio-info form-check-inline">
                                <input type="radio" value="1" name="status" checked="">
                                <label for="inlineRadio1"> Active </label>
                            </div>
                            <div class="radio radio-info form-check-inline">
                                <input type="radio" value="0" name="status">
                                <label for="inlineRadio1"> Inactive </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <center><button type="submit" class="btn btn-primary">Submit</button></center>
                    </div>
                </div>
            </form>
        </div>
    </div>           
@include('admin.include.footer') 
   
<script src="{{url('AdminAssest/vendors/summernote/dist/summernote.min.js')}}" type="text/javascript"></script>
<script>
     function addtestimonial() {
        $('#page').val('');
        $("#title_view").html("Add");

        $('#testimonial_action').modal('show');
    }

    function edittestimonial(id) {
        $("#title_view").html("Edit");
       var to_append="";
        $("#testimonial_id").val(id);
        $.ajax({
            url: "{{url('admin/testimonial/fetch')}}" + "/" + id,
            method: "GET",
            contentType: 'application/json',
            dataType: "json",
            success: function (data) {

                console.log(data);
                $("#customer_name").val(data.customer_name);
                $("#testimonial_name").val(data.testimonial_name);
                $("#customer_message").val(data.customer_message);
                $("#address").val(data.address);
             
                var val = data.status;
                to_append += `<div class="images-delete-block" style="margin-right:5px; display:inline-block; position:relative; padding:3px;border:1px solid #c4c4c4;border-radious:3px;">
                                <img src="{{url('upload/testimonial/`+ data.customer_photo+ `')}}" style="height:90px; min-height:90px; min-width:80px;">
                            </div>`;
                if (val == 1) {
                    $('input[name=status][value=' + val + ']').prop('checked', true);
                } else {
                    $('input[name=status][value=' + val + ']').prop('checked', true);
                }
                $('#edit_photo_section').css('display', 'block');
                $('#edit_photo_section').append(to_append);
            }
        });
        $('#testimonial_action').modal('show');

    }
</script>
