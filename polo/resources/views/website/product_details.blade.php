@include('website.header')
<style>
.top_nav{
	background-color:blue;
}
</style>
<!-- Service Area Start -->
<section class="service">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-7 col-md-10">
				<div class="section-heading">
					<h2 class="title" style="padding-top:15px;">
						 Products Details
					</h2>
				</div>
			</div>
		</div>
		<div class="row">
            <div class="col-lg-6 col-md-6" >
                <div class="product-details">
					<div class="icon">
						<img src="{{url('public/upload/product_image')}}/{{$toReturn['ProductDetails']->product_photo}}" alt="">
					</div>
				</div>
            </div>
            <div class="col-lg-6 col-md-6"  >
                <div class="product-details" >
                    <h4 class="title">Product Name: <span>{{$toReturn['ProductDetails']->product_name ?? ""}}</span></h4>
                    <p>
                        <span style="color: rgb(5, 14, 51); text-align: center;">Product Specs : <span>{{$toReturn['ProductDetails']->product_specs ?? ""}}</span></span><br>
                    </p>
                    <p>
                        <span style="color: rgb(5, 14, 51); text-align: center;">Product Price: <span>{{$toReturn['ProductDetails']->product_price ?? ""}}</span></span><br>
                    </p>
                    <p>
                        <span style="color: rgb(5, 14, 51); text-align: center;">Product Description: </span><br><p>{{$toReturn['ProductDetails']->product_description ?? ""}}</p>
                    </p>
                    
                </div>
            </div>
           
          
		

		</div>
	</div>
</section>
<!-- Service Area End -->
<!-- contact  -->
<div class="container">
    <!-- <div class="col-2"></div> -->
    <div class="col-12" style="background-color: #f9f9f9;">
    <h3 style="text-align:center" class="py-4"><b>Contact Us</b> </h3>
            <div class="col-12">
				
				<form action="{{url('contactsave')}}" method="post">
					@csrf()
					<div class="row">
					<div class="col-6">
					<div class="form-group">
                        <label for="exampleInputEmail1">Name <span style="color: red;">*</span></label>
                        <input type="text" class="form-control" name="name" required placeholder="Name" />
                        <input type="hidden" class="form-control" name="Product_id" value="{{$toReturn['ProductDetails']->id}}"/>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email </label>
                        <input type="email" class="form-control" name="email" placeholder="Enter email" />
                    </div>
					</div>
					<div class="col-6">
					<div class="form-group">
                        <label for="exampleInputEmail1">Phone No. <span style="color: red;">*</span></label>
                        <input type="number" class="form-control" min="100" max="10000000000" name="phone_no" required placeholder="Phone No." />
                    </div>
                    <button type="submit" class="btn btn-primary" style="width:100%;margin-top:30px;">Submit</button>
					</div>
                                      
				</div>
                </form>
            </div>
        
    </div>
    <!-- <div class="col-2"></div> -->

</div>

<!-- contact end  -->
@include('website.footer')
