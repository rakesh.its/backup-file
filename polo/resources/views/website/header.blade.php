<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords"
content="">
<meta name="author" content="">
<title>NABU SUPPORT AND TRADING INC</title>
<!-- favicon -->
<link rel="icon" type="image/x-icon" href="{{url('images/1561451980service-icon-1.png')}}" />
<!-- bootstrap -->
<link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
<!-- Plugin css -->
<link rel="stylesheet" href="{{url('css/plugin.css')}}">
<link rel="stylesheet" href="{{url('css/pignose.calender.css')}}">
<!-- stylesheet -->
<link rel="stylesheet" href="{{url('css/style.css')}}">
<link rel="stylesheet" href="{{url('css/custom.css')}}">
<link rel="stylesheet" href="{{url('css/megamenu.css')}}">
<!-- responsive -->
<link rel="stylesheet" href="{{url('css/responsive.css')}}">
<!--Updated CSS-->
<link rel="stylesheet" href="{{url('css/stylesf00a.html?color=4a1ec7')}}">
<style>
:root {--blue:#1242C9;}
/* ==== Site Banner ==== */
.hero-area {height:none;padding:0;}
#site-banner {position:relative;left:0;top:0;width:100%;}
#site-banner .carousel-item {width:100%;}
#site-banner .carousel-item img {width:100%;height:100vh;object-fit:cover;}
.banner-carousel-caption {bottom:43%;}
.banner-carousel-caption h1 {font-weight:900;color:#fff;}
.banner-slider-arrows {width:15%!important;}
/* ==== Section First ==== */
.section-first{display:block;padding:50px 0 0;}
.slider-heading-left {margin:;font-size:15px;font-weight:600;text-transform:uppercase;color:var(--blue);}
.slider-arrow-box-right{width:61px;background:#e2e5ef;position:relative;top:0px;height:31px;}
.slider-arrow-box-right .carousel-control-prev,
.slider-arrow-box-right .carousel-control-next {width:30px;height:30px;line-height:30px;text-align:center;background-color:var(--blue);color:#fff;opacity:0.9}
.sec-1-img {width:100%;height:350px;object-fit:cover;}
.slider-cover {position:absolute;left:0;bottom:-120px;right:0;width:100%;padding:100px 15px 15px;background:linear-gradient(#0000 10%,#051b57);color:#fff;text-align:center;transition:all 0.5s ease-in-out;}
.slider-cover h6 {font-size:16px;font-weight:600;text-transform:uppercase;color:#fff;}
.slider-cover p {font-size:13px;color:#fff;margin-bottom:2px;}
.slider-cover a {font-size:13px;color:#fff;}
.section-first .carousel-item {overflow:hidden;}
.section-first .carousel-item:hover .slider-cover {bottom:0;opacity:1}

.site-footer {font-size:13px;background-color:#F6F6F6;}
.site-footer h5 {font-size:14px;font-weight:600;color:var(--blue);}
.site-footer .site-f-social {display:flex;}
.site-footer .site-f-social > a {width:35px;height:35px;line-height:32px;text-align:center;background-color:var(--blue);color:#fff;margin-right:0.3rem;border-radius:50%;border:2px solid royalblue;}
.site-footer .site-f-social > a > i {color:#fff;}
.n_f_newletter {display:block;width:100%;background:#dcdcdc;padding:10px 0px 10px 15px;}
.n_f_newletter h6 {position:relative;font-size:13px;color:var(--blue);margin-bottom:0.3em;}
.n_f_newletter .form-control {height:38px;line-height:38px;border-radius:0;box-shadow:none;background-color:#ccc;border:1px solid var(--blue);}
.n_f_newletter .form-control::placeholder {font-size:13px;}
.n_f_newletter button{background:var(--blue);color:#fff;border-radius:0;display:inline-block;width:100px;border:1px solid var(--blue);height:38px;line-height:38px;}
.n_f_newletter button:hover {color:#fff;}
.copy {background-color:var(--blue);color:#fff;padding:30px 0;}


@media (max-width:900px) {
 .banner-carousel-caption {bottom:18%;}
}
</style>
</head>
<body>
<div class="preloader" id="preloader" style="background: url({{url('images/1558779995loader.gif')}}) no-repeat scroll center center #FFF;"></div>
<header class="d-block">
   <div class="top_nav">
      <div class="top">
         <div class="container">
            <div class="links">
            	<div class="links_left">
            		<a href="javascript:void(0)" style="cursor:default;">Follow Us:</a>
            		<a href=""><i class="fab fa-facebook-f"></i></a>
            		<a href=""><i class="fab fa-google-plus"></i></a>
            		<a href=""><i class="fab fa-twitter"></i></a>
            		<a href=""><i class="fab fa-linkedin"></i></a>
            	</div>
            	<div class="links_right">
            		<div class="language-selector">
            			<i class="fas fa-globe"></i>
            			<select name="language" class="language selectors">
            				<option value="#" selected="">English</option>
            			</select>
            		</div>
            		<div class="currency-selector">
            			<select name="currency" class="currency selectors">
            				<option value="#" selected="">USD</option>
            				<option value="#">BDT</option>
            				<option value="#">EUR</option>
            			</select>
            		</div>
            	</div>
            </div>
         </div>
      </div>
      <nav class="navbar navbar-expand-lg navbar-light mainNav">
         <div class="container">
         	<a class="navbar-brand" href="{{url('/')}}" style="pointer-events:none;">
         		<img src="{{url('images/nabu-white-logo.png')}}" alt="" class="img-fluid" style="pointer-events:all;">
         	</a>
         	<div class="collapse navbar-collapse" id="mainMenu">
         		<ul class="navbar-nav ml-auto">
				 	@php
						@$page=DB::table('pages')->where('page_position','Header')->where('status',1)->orderBy('Page_SI_no','ASC')->select('page_title','page_name')->get();
					@endphp
					@if(count($page)>0)
						@foreach($page as $key_p=> $value_p)
						<li class="nav-item">
							<a class="nav-link" href="{{url('pages/details/'.$value_p->page_name)}}">{{$value_p->page_title}}</a>
						</li>
						@endforeach
					@endif
         			
         			<li class="nav-item">
         				<a class="nav-link" href="{{url('/Products')}}">Product</a>
         				<div class="mega-menu">
         					<div class="n-b-container">
         						<div class="n-b-img">
         							<ul class="list-unstyled n-b-box" >
									 	 <?php $count = 1; ?>
										 @foreach($toReturn['tag'] as $key => $value)
         								<li>
         									<a href="#!">
         										<span>{{$value['category_titile']}}</span>
         										<img src="{{url('upload/product_category/'.$value['category_image'])}}" alt="" width="80" height="50">
         									</a>
											 @if($key == 0)
         										<ul class="list-unstyled d-block">
											 @else
         										<ul class="list-unstyled">
											 @endif
											@foreach($value['products'] as $item)
         										<li>
         											<a href="#!" class="b-{{$count}}" onmouseover="spamHover({{$count++}})" >
         												<span>{{$item['product_name']}}</span>
         											</a>
         										</li>
											@endforeach
         									</ul>
         								</li>
         								
										@endforeach
         							</ul>
         						</div>
         						<div class="n-b-img w-50">
								<?php $countPerProduct = 1; ?>
									@foreach($toReturn['tag'] as $value)
										@foreach($value['products'] as $Invalues)
											<div class="n-b-img-inner s-pearl _{{$countPerProduct++}}" style="background-image:url({{url('upload/product_image/'.@$Invalues['product_photo'])}});">
												<div class="n-b-caption w-100">
													<div class="row mb-3 w-100">
														<div class="col-md-6">
															<h4>Product Name</h4>
															<p>{{@$Invalues['product_name']}}</p> <br>
															<h4>Product Description</h4>
															<p>{{@$Invalues['product_description']}}</p> <br>
															<h4>Product Specs</h4>
															<p>{{@$Invalues['product_specs']}}</p> <br>
															<h4>Product Price</h4>
															<p>Start Price -: {{@$Invalues['product_start_price']}}, Product Price -: {{@$Invalues['product_price']}}</p> <br>

														</div>
													</div>
													<!-- <div class="row mb-3">
														<div class="col-md-12">
															<i class="fa fa-angle-right"></i> Explore </a>
														</div>
													</div>		 -->
													
													<!-- <div class="row w-100">
														<div class="col-md-12 w-100">
															<p class="small"></p>
														</div>
													</div> -->
												</div>
											</div>
										@endforeach
									@endforeach
         						</div>
         					</div>
         				</div>
         			</li>
         			<li class="nav-item">
         				<a class="nav-link" href="{{url('/website/comming_soon')}}">Blog</a>
         			</li>
         			<li class="nav-item">
         				<a class="nav-link" href="{{url('/contact')}}">Contact</a>
         			</li>
         		</ul>
         	</div>
         </div>
      </nav>
   </div>
   <div class="mobile_menu">
   	<div class="mobile_menu_btn" onclick="$('.mobile_menu_slide').toggleClass('active');">Menu</div>
   	<div class="mobile_menu_slide">
   		<ul>
   			<li class="dropdown-divider mt-0"></li>
   			<li><a href="#!" onclick="$('.mobile_menu_slide').removeClass('active');">Close</a></li>
   			<li class="dropdown-divider"></li>
   			<li><a href="">Home</a></li>
   			<li><a href="">About</a></li>
   			<li><a href="">Product</a></li>
   			<li><a href="">Blog</a></li>
   			<li><a href="">Contact</a></li>
   		</ul>
   	</div>
   </div>
</header>