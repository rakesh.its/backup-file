@include('website.header')
@if(Count( $toReturn['slider_Data'])>0)
<section class="hero-area" style="background: url({{url('images/banner001.jpg')}});">
	<div id="site-banner" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
		@foreach($toReturn['slider_Data'] as $keys => $value_s)
		@if($keys==0)
			<div class="carousel-item active">
			@else
			<div class="carousel-item">
			@endif
				<img src="{{url('upload/slider/')}}/{{$value_s['slider_photo']}}" alt="...">
				<div class="carousel-caption banner-carousel-caption">
					<h1>{{$value_s['silder_message']}}</h1>
				</div>
			</div>
		@endforeach
			<!-- <div class="carousel-item">
				<img src="{{url('images/banner2.jpg')}}" alt="...">
				<div class="carousel-caption banner-carousel-caption">
					<h1>Anti Covid-19 Medical SuppliesSupplgjhsjhjshhsd</h1>
				</div>
			</div> -->
		</div>
		<a class="carousel-control-prev banner-slider-arrows" href="#site-banner" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next banner-slider-arrows" href="#site-banner" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</section>
@else
<section class="hero-area" style="background: url({{url('images/banner001.jpg')}});">
	<div id="site-banner" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img src="{{url('images/banner1.jpg')}}" alt="...">
				<div class="carousel-caption banner-carousel-caption">
					<h1>Anti Covid-19 Medical Supplies</h1>
				</div>
			</div>
			<div class="carousel-item">
				<img src="{{url('images/banner2.jpg')}}" alt="...">
				<div class="carousel-caption banner-carousel-caption">
					<h1>Anti Covid-19 Medical Supplies</h1>
				</div>
			</div>
		</div>
		<a class="carousel-control-prev banner-slider-arrows" href="#site-banner" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next banner-slider-arrows" href="#site-banner" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</section>
@endif
<!-- Hero Area End -->

<!-- Section First -->
@if(count($toReturn['GetProductDetailsCategoryBy'])>0)
<section class="section-first">
	<div class="container-fluid">
		<div class="row">
		@foreach($toReturn['GetProductDetailsCategoryBy'] as $key_D=> $value_D)
			<div class="col-lg-4 mb-2">
				<div class="d-flex justify-content-between align-items-end">
					<h5 class="slider-heading-left">OUR Products</h5>
					<div class="slider-arrow-box-right">
						<a class="carousel-control-prev" href="#sli-1" role="button" data-slide="prev">
							<i class="fa fa-angle-left"></i> 
						</a>
						<a class="carousel-control-next" href="#sli-1" role="button" data-slide="next">
							<i class="fa fa-angle-right"></i> 
						</a>
					</div>
				</div>
				<div id="sli-1" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
					@if(count($value_D)>0)
						@foreach($value_D as $key_PC=> $value_PC)
						@if($key_PC==0)
						<div class="carousel-item active">
						@else
						<div class="carousel-item">
						@endif
							<img src="{{url('upload/product_image/'.@$value_PC['product_photo'])}}" class="sec-1-img" alt="...">
							<span class="slider-cover">
								<h6>{{$value_PC['product_name']}}</h6>
								<p>
								{{$value_PC['product_description']}}
								</p>
								<a href="{{url('/Products/details')}}/{{Crypt::encrypt($value_PC->id)}}" class="text-center">Read more</a>
							</span>
						</div>
						@endforeach
					@else
						<div class="carousel-item active">
							<img src="{{url('images/banner2.jpg')}}" class="sec-1-img" alt="...">
							<span class="slider-cover">
								<h6></h6>
								<p>
								</p>
								<a href="" class="text-center">Read more</a>
							</span>  
						</div>
					@endif
					</div>
				</div>
			</div>
		@endforeach
		
			
		</div>
	</div>
</section>
@endif
<!-- Section First Close -->


<!-- Service Area Start -->
<section class="service">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-7 col-md-10">
				<div class="section-heading">
					<h5 class="sub-title">
						Recommended For
					</h5>
					<h2 class="title">
						We bring the best things
					</h2>
					<p class="text">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
						et dolore magna aliqua.
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			@if(count($toReturn['Category_Data'])!="")
				@foreach($toReturn['Category_Data'] as $key_C => $value_C)
				<div class="col-lg-4 col-md-6">
					<div class="single-service">
						<div class="icon">
							<img src="{{url('images/features/1561533001service-icon-1.png')}}" alt="">
						</div>
						<div class="content">
							<h4 class="title">
								{{$value_C->heading?? ""}}
							</h4>
							@if($value_C->description!="")
							<p>
								<span style="color: rgb(5, 14, 51); text-align: center;">{{$value_C->description}}.</span><br>
							</p>
							@endif
						</div>
					</div>
				</div>
				@endforeach
			@endif

		</div>
	</div>
</section>
<!-- Service Area End -->
<!-- Portfolio Area Start -->
<section class="portfolio">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-7 col-md-10">
				<div class="section-heading color-white">
					<h5 class="sub-title">
						HOT COVID-19 PRODUCTS - JOIN THE FIGHT
					</h5>
					<h2 class="title">
						Our Products
					</h2>
					<p class="text">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
						et dolore magna aliqua.
					</p>
				</div>
			</div>
		</div>
		<div class="row">
		@if(count($toReturn['Products_Data'])!="")
				@foreach($toReturn['Products_Data'] as $key_P => $value_P)
			<div class="col-lg-4 col-md-6">
				<div class="single-portfolio">
					<img src="{{url('upload/product_image')}}/{{$value_P->product_photo}}" alt="">
					<div class="content-wrapper">
						<div class="content">
							<h4 class="title">
								{{$value_P->product_name}}
							</h4>
							<div class="links">
								<a href="{{url('upload/product_image')}}/{{$value_P->product_photo}}" class="link img-popup">
									<i class="far fa-eye"></i>
								</a>
								<a href="{{url('/Products/details')}}/{{Crypt::encrypt($value_P->id)}}" class="link">
									<i class="fas fa-link"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		@endif
			
		</div>
	</div>
</section>
<!-- Portfolio Area End -->
<section class="pricing">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-7 col-md-10">
				<div class="section-heading">
					<h5 class="sub-title">
						Top Product Categories
					</h5>
					<h2 class="title">
						Check Our Wide Range Of Quality Products
					</h2>
					<p class="text">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
						et dolore magna aliqua.
					</p>
				</div>
			</div>
		</div>
		<div class="row ">
		</div>
	</div>
</section>
<!-- Statistics Area Start -->
<section class="statistics">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-7 col-md-10">
				<div class="section-heading color-white">
					<h5 class="sub-title">
						YOU SHOULD KNOW MORE ABOUT US
					</h5>
					<h2 class="title">
						Necessary Steps
					</h2>
					<p class="text">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
						et dolore magna aliqua.
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-md-6">
				<div class="single-statistics">
					<div class="icon">
						<img src="{{url('images/vpresentation/1561542255statistics1.png')}}" alt="">
					</div>
					<p class="title">
						Sanitization tunnel for Employees
					</p>
				</div>
			</div>
			<div class="col-lg-3 col-md-6">
				<div class="single-statistics">
					<div class="icon">
						<img src="{{url('images/vpresentation/1561542327statistics1.png')}}" alt="">
					</div>
					<p class="title">
						Masks are handed over at the reception
					</p>
				</div>
			</div>
			<div class="col-lg-3 col-md-6">
				<div class="single-statistics">
					<div class="icon">
						<img src="{{url('images/vpresentation/1561542384statistics1.png')}}" alt="">
					</div>
					<p class="title">
						Temperature screening with Zero Touch thermometers
					</p>
				</div>
			</div>
			<div class="col-lg-3 col-md-6">
				<div class="single-statistics">
					<div class="icon">
						<img src="{{url('images/vpresentation/1561542391statistics1.png')}}" alt="">
					</div>
					<p class="title">
						Sanitizers available at every desk
					</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Statistics Area End -->
<!-- Team Area Start -->
<section class="team">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-7 col-md-10">
				<div class="section-heading">
					<h5 class="sub-title">
						ANTI COVID-19 MEDICAL SUPPLIES
					</h5>
					<h2 class="title">
						Do you have any question ?
					</h2>
					<p class="text">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
						et dolore magna aliqua.
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-md-6">
			</div>
			<div class="col-lg-3 col-md-6">
			</div>
			<div class="col-lg-3 col-md-6">
			</div>
			<div class="col-lg-3 col-md-6">
			</div>
			<div class="col-lg-3 col-md-6">
			</div>
			<div class="col-lg-3 col-md-6">
			</div>
			<div class="col-lg-3 col-md-6">
			</div>
		</div>
	</div>
</section>
<!-- Team Area End -->
<!-- Testimonial Area Start -->
<section class="testimonial">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-7 col-md-10">
				<div class="section-heading color-white">
					<h5 class="sub-title">
						Testimonial
					</h5>
					<h2 class="title">
						Customer Reviews
					</h2>
					<p class="text">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
						et dolore magna aliqua.
					</p>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-lg-8 col-md-10">
				<div class="testimonial-slider">
					<div class="single-testimonial">
						<div class="review-text">
							<p>That conviction is where the process of change really begins—with the realization that just
								because a certain abuse has taken place in the past doesn’t mean that we have to tole. That
								conviction is where the process of change really begins
							</p>
						</div>
						<div class="people">
							<div class="img">
								<img src="{{url('images/reviews/1561447511people.png')}}" alt="">
							</div>
							<h4 class="title">M. Badr</h4>
							<p class="designation">CEO &amp; Founder</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@include('website.footer')
