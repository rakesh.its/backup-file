@include('website.header')
<section class="hero-area" style="background: url({{url('images/banner001.jpg')}});">
	<div id="site-banner" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img src="{{url('images/banner1.jpg')}}" alt="...">
				<div class="carousel-caption banner-carousel-caption">
					<h1>Anti Covid-19 Medical Supplies</h1>
				</div>
			</div>
			<div class="carousel-item">
				<img src="{{url('images/banner2.jpg')}}" alt="...">
				<div class="carousel-caption banner-carousel-caption">
					<h1>Anti Covid-19 Medical Supplies</h1>
				</div>
			</div>
		</div>
		<a class="carousel-control-prev banner-slider-arrows" href="#site-banner" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next banner-slider-arrows" href="#site-banner" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</section>

<div class="col-12">
		<div class="container">
			<!-- tittle heading -->
			<h3 style="text-align:center" class="py-4"><b>Contact Us</b> </h3>
			<!-- //tittle heading -->
			<!-- contact -->
			<div class="col-12" style="background-color:#f9f9f9;">
				<div class="row">
					<div class="col-6">
                    <form action="{{url('contactsave')}}" method="post">
                        @csrf()
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name <span style="color:red">*</span></label>
                            <input type="text" class="form-control" name="name" required placeholder="Name">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email </label>
                            <input type="email" class="form-control" name="email"  placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Subject <span style="color:red">*</span></label>
                            <input type="text" class="form-control" name="subject" required placeholder="Subject">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Phone No. <span style="color:red">*</span></label>
                            <input type="number" class="form-control" name="phone_no" required placeholder="Phone No.">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Address</label>
                            <input type="text" class="form-control" name="address"  placeholder="Address">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Comment</label>
                            <input type="text" class="form-control" name="comments"  placeholder="Comment">
                        </div>
                       
                        
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
						
					</div>




					<!-- <div class="col-6">
						
							<h4>GET IN TOUCH :</h4>
							<p>
								<i class="fa fa-map-marker"></i> 123 Sebastian, NY 10002, USA.</p>
							<p>
								<i class="fa fa-phone"></i> Telephone : 333 222 3333</p>
							<p>
								<i class="fa fa-fax"></i> FAX : +1 888 888 4444</p>
							<p>
								<i class="fa fa-envelope-o"></i> Email :
								<a href="mailto:example@mail.com">mail@example.com</a>
							</p>
						
						<div class="clearfix"> </div>
					</div> -->
					<div class="col-6 py-5">
							<img src="{{url('images/contact2.jpg')}}" alt="">
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
			<!-- //contact -->
		</div>
	</div>


@include('website.footer')