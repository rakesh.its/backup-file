<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('clear-cache', function () {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('config:cache');
    Session::flash('success', 'All Clear');
    echo "DONE";
});
Route::get('/', 'WebSiteController@GetHomePageDate');
Route::get('/contact', 'WebSiteController@contact');
Route::post('/contactsave', 'WebSiteController@contactsave');
Route::get('/website/comming_soon', 'WebSiteController@website_comming_soon');
Route::get('/Products', 'WebSiteController@ProductAll'); //List All Product
Route::get('/pages/details/{page_name}', 'WebSiteController@viewPages'); //List All Product
Route::get('/Products/details/{ProductId}', 'WebSiteController@ProductDetails'); //List All Product

Route::get('/admin', function () {
    return redirect("/login");
});

Auth::routes();
Route::middleware('auth')->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    });
   

    Route::get('/pages', function () {
        return view('admin.pages');
    });
    Route::get('/admin/comming_soon', function () {
        return view('admin.comming_soon');
    });
    // Admin Routes
    Route::get('/admin/pages', 'MasterController@list_pages'); //pages List
    Route::post('/admin/pages/save', 'MasterController@save_pages'); // pages Save
    Route::get('/admin/pages/fetch/{sub_id}', 'MasterController@fetchpages'); //pages Fetch For Edit
    Route::get('admin/pages/statusUpdate/{id}', 'MasterController@updateStatuspages'); // pages guardians Status

    Route::get('/admin/products', 'MasterController@list_products'); //products List
    Route::post('/admin/products/save', 'MasterController@save_products'); // products Save
    Route::get('/admin/products/fetch/{sub_id}', 'MasterController@fetchproducts'); //products Fetch For Edit
    Route::get('admin/products/statusUpdate/{id}', 'MasterController@updateStatusproducts'); // products guardians Status
    // Product Category
    Route::get('/admin/product_category', 'MasterController@list_product_category'); //product_category List
    Route::post('/admin/product_category/save', 'MasterController@save_product_category'); // product_category Save
    Route::get('/admin/product_category/fetch/{sub_id}', 'MasterController@fetchproduct_category'); //product_category Fetch For Edit
    Route::get('admin/product_category/statusUpdate/{id}', 'MasterController@updateStatusproduct_category'); // product_category guardians Status
    // Product Sub Category
    Route::get('/admin/product_subcategory', 'MasterController@list_product_subcategory'); //product_subcategory List
    Route::post('/admin/product_subcategory/save', 'MasterController@save_product_subcategory'); // product_subcategory Save
    Route::get('/admin/product_subcategory/fetch/{sub_id}', 'MasterController@fetchproduct_subcategory'); //product_subcategory Fetch For Edit
    Route::get('admin/product_subcategory/statusUpdate/{id}', 'MasterController@updateStatusproduct_subcategory'); // product_subcategory guardians Status

    // Slider 
    Route::get('/admin/slider', 'MasterController@list_slider'); //admin/slider listing
    Route::post('/admin/slider/save', 'MasterController@save_slider'); // product_subcategory Save
    Route::get('/admin/slider/fetch/{sub_id}', 'MasterController@fetch_slider'); //product_subcategory Fetch For Edit
    Route::get('admin/slider/statusUpdate/{id}', 'MasterController@updateStatusSlider'); // product_subcategory guardians Status

    Route::get('/admin/destinations', 'MasterController@list_destinations'); //destinations List
    Route::post('/admin/destinations/save', 'MasterController@save_destinations'); // destinations Save
    Route::get('/admin/destinations/fetch/{sub_id}', 'MasterController@fetchdestinations'); //destinations Fetch For Edit
    Route::get('admin/destinations/statusUpdate/{id}', 'MasterController@updateStatusdestinations'); // destinations guardians Status

    Route::get('/admin/category', 'MasterController@list_category'); //category List
    Route::post('/admin/category/save', 'MasterController@save_category'); // category Save
    Route::get('/admin/category/fetch/{sub_id}', 'MasterController@fetchcategory'); //category Fetch For Edit
    Route::get('admin/category/statusUpdate/{id}', 'MasterController@updateStatuscategory'); // category guardians Status

    Route::get('/admin/testimonial', 'MasterController@list_testimonial'); //testimonial List
    Route::post('/admin/testimonial/save', 'MasterController@save_testimonial'); // testimonial Save
    Route::get('/admin/testimonial/fetch/{sub_id}', 'MasterController@fetchtestimonial'); //testimonial Fetch For Edit
    Route::get('admin/testimonial/statusUpdate/{id}', 'MasterController@updateStatustestimonial'); // testimonial guardians Status

    Route::get('/admin/contact', 'MasterController@list_contact'); //contact List
    Route::get('/search_for_subcategory/{id}', 'MasterController@search_for_subcategory'); //search_for_subcategory according to category
    // Admin Details
    Route::get('/admin/user', function () {
        return view('admin.user');
    });
    Route::post('/admin/user/update', 'MasterController@UpdateUserDetails');
});