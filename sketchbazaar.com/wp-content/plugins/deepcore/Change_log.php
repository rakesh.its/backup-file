<?php
return $change_log = '
v 1.0.18 – 19 January 2021
- Added: New demo "Wedding"
- Added: New demo "Insurance"
- Added: New demo "Yoga"
- Added: New demo "Mechanic"
- Added: New demo "Portfolio"
- Added: New demo "Dietitian"
- Added: New demo "Software"
- Added: New demo "Beauty"
- Added: New demo "Consulting"
- Added: New demo "Crypto"

v 1.0.17 – 15 January 2021
- Added: New demo "Startup"
- Added: New demo "Dentist"
- Added: New demo "Lawyer"
- Added: New demo "Business"
- Added: New demo "Language School"
- Added: New demo "Freelancer"
- Added: New demo "Real Estate"
- Added: New demo "Church"
- Added: New demo "Events"
- Added: New demo "Corporate"
- Added: New demo "Corporate2"
- Added: New demo "SPA"

v 1.0.16 – 12 January 2021
- Fixed: Some minor issues

v 1.0.15 – 5 January 2021
- Added: New demo "Conference"
- Fixed: Some minor issues

v 1.0.14 – 26 December 2020
- Added: New demo "Modern Business"
- Fixed: Some minor issues

v 1.0.5 – 29 September 2020
- Fixed: Demo importer progress bar
- Fixed: Some JS errors

v 1.0.4 – 2 September 2020
- Fixed: Demo importer issue
- Fixed: Mobile menu dropdown issue

v 1.0.3 – 12 August 2020
- Compatibility: WordPress 5.5

v 1.0.2 – 28 July 2020
- Added: Option to disable the theme customized checkout form
- Added: More styling options for Elementor widgets 
- Fixed: Woocommerce variation images display issue
- Fixed: Header builder date widget string issues
- Fixed: Testimonial slider display icons issue
- Fixed: Food menu widget styling issues
- Fixed: Menu display issue in mobile
- Fixed: Mega menu width style issue
- Fixed: Mobile menu closing issue
- Fixed: List widget ordering view

v 1.0.1 – 18 April 2020
- Added: More styling options for button elementor widget
- Fixed: Blog container size effect on footer and header
- Fixed: Header builder search widget rendering issue
- Fixed: Woocommerce product image custom crop size
- Fixed: Woocommerce product sold individually
- Fixed: Custom 404 page rendering issue
- Fixed: Child theme auto-update
- Fixed: Social widget issue

v 1.0.0 –  21 October 2019
- Initial Release
';